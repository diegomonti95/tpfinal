/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     08/11/2020 08:33:05 p.m.                     */
/*==============================================================*/


drop index AREAS_NOMBRE;

drop table AREAS;

drop table AUDITORIAS;

drop index CLASIFICACIONES_NOMBRE;

drop table CLASIFICACIONES;

drop index CLASIFICADOR_NOMBRE;

drop table CLASIFICADOR_GASTOS;

drop index CLIENTES_DOCUMENTO;

drop table CLIENTES;

drop table COMPROBANTES_DETALLES;

drop table GASTOS;

drop index MONEDAS_SIGLA;

drop table MONEDAS;

drop table PRESUPUESTOS_DETALLES;

drop table PRESUPUESTOS_VENTAS;

drop index PRODUCTOS_CODIGO;

drop table PRODUCTOS;

drop index PROVEEDORES_DOCUMENTO;

drop table PROVEEDORES;

drop index SUCURSALES_NOMBRE;

drop table SUCURSALES;

drop index UNIDADES_SIGLA;

drop table UNIDADES;

drop table USUARIOS;

drop domain D_CHAR1;

drop domain D_DATE;

drop domain D_INT2;

drop domain D_INT4;

drop domain D_NUMERIC10_2;

drop domain D_NUMERIC12;

drop domain D_NUMERIC12_2;

drop domain D_TIMESTAMP;

drop domain D_VARCHAR15;

drop domain D_VARCHAR20;

drop domain D_VARCHAR200;

drop domain D_VARCHAR50;

drop sequence SEQUENCE_1;

create sequence SEQUENCE_1;

/*==============================================================*/
/* Domain: D_CHAR1                                              */
/*==============================================================*/
create domain D_CHAR1 as CHAR(1);

/*==============================================================*/
/* Domain: D_DATE                                               */
/*==============================================================*/
create domain D_DATE as DATE;

/*==============================================================*/
/* Domain: D_INT2                                               */
/*==============================================================*/
create domain D_INT2 as INT2;

/*==============================================================*/
/* Domain: D_INT4                                               */
/*==============================================================*/
create domain D_INT4 as INT4;

/*==============================================================*/
/* Domain: D_NUMERIC10_2                                        */
/*==============================================================*/
create domain D_NUMERIC10_2 as NUMERIC(10,2);

/*==============================================================*/
/* Domain: D_NUMERIC12                                          */
/*==============================================================*/
create domain D_NUMERIC12 as NUMERIC(12);

/*==============================================================*/
/* Domain: D_NUMERIC12_2                                        */
/*==============================================================*/
create domain D_NUMERIC12_2 as NUMERIC(12,2);

/*==============================================================*/
/* Domain: D_TIMESTAMP                                          */
/*==============================================================*/
create domain D_TIMESTAMP as TIMESTAMP;

/*==============================================================*/
/* Domain: D_VARCHAR15                                          */
/*==============================================================*/
create domain D_VARCHAR15 as VARCHAR(15);

/*==============================================================*/
/* Domain: D_VARCHAR20                                          */
/*==============================================================*/
create domain D_VARCHAR20 as VARCHAR(20);

/*==============================================================*/
/* Domain: D_VARCHAR200                                         */
/*==============================================================*/
create domain D_VARCHAR200 as VARCHAR(200);

/*==============================================================*/
/* Domain: D_VARCHAR50                                          */
/*==============================================================*/
create domain D_VARCHAR50 as VARCHAR(50);

/*==============================================================*/
/* Table: AREAS                                                 */
/*==============================================================*/
create table AREAS (
   AREA                 SERIAL               not null,
   SUCURSAL             D_INT2               not null,
   NOMBRE               D_VARCHAR50          not null,
   constraint PK_AREAS primary key (AREA)
);

comment on table AREAS is
'Logisica
Cimiento
Techo
Andamios
Parte Electrica
Parte de Agua';

/*==============================================================*/
/* Index: AREAS_NOMBRE                                          */
/*==============================================================*/
create unique index AREAS_NOMBRE on AREAS (
SUCURSAL,
NOMBRE
);

/*==============================================================*/
/* Table: AUDITORIAS                                            */
/*==============================================================*/
create table AUDITORIAS (
   AUDITORIA            SERIAL               not null,
   USUARIO              D_VARCHAR20          not null,
   TABLA                D_VARCHAR50          not null,
   FECHA                D_TIMESTAMP          not null,
   OBSERVACION          D_VARCHAR200         not null,
   ACCION               D_CHAR1              not null,
   constraint PK_AUDITORIAS primary key (AUDITORIA)
);

/*==============================================================*/
/* Table: CLASIFICACIONES                                       */
/*==============================================================*/
create table CLASIFICACIONES (
   CLASIFICACION        SERIAL               not null,
   NOMBRE               D_VARCHAR50          not null,
   constraint PK_CLASIFICACIONES primary key (CLASIFICACION)
);

comment on table CLASIFICACIONES is
'Insumo
Telas
Mesas
Sillas
Sillones
Sof�
';

/*==============================================================*/
/* Index: CLASIFICACIONES_NOMBRE                                */
/*==============================================================*/
create unique index CLASIFICACIONES_NOMBRE on CLASIFICACIONES (
NOMBRE
);

/*==============================================================*/
/* Table: CLASIFICADOR_GASTOS                                   */
/*==============================================================*/
create table CLASIFICADOR_GASTOS (
   CLASIFICADOR         SERIAL               not null,
   NOMBRE               D_VARCHAR50          not null,
   constraint PK_CLASIFICADOR_GASTOS primary key (CLASIFICADOR)
);

/*==============================================================*/
/* Index: CLASIFICADOR_NOMBRE                                   */
/*==============================================================*/
create unique index CLASIFICADOR_NOMBRE on CLASIFICADOR_GASTOS (
NOMBRE
);

/*==============================================================*/
/* Table: CLIENTES                                              */
/*==============================================================*/
create table CLIENTES (
   CLIENTE              D_INT4               not null,
   RAZON_SOCIAL         D_VARCHAR50          not null,
   DOCUMENTO            D_VARCHAR20          not null,
   DIRECCION            D_VARCHAR50          not null,
   CIUDAD               D_VARCHAR50          not null,
   BARRIO               D_VARCHAR50          not null,
   TELEFONO_PRINCIPAL   D_VARCHAR20          not null,
   TELEFONO_SECUNDARIO  D_VARCHAR20          null,
   EMAIL                D_VARCHAR200         not null,
   PERSONERIA           D_CHAR1              not null,
   constraint PK_CLIENTES primary key (CLIENTE)
);

/*==============================================================*/
/* Index: CLIENTES_DOCUMENTO                                    */
/*==============================================================*/
create unique index CLIENTES_DOCUMENTO on CLIENTES (
DOCUMENTO
);

/*==============================================================*/
/* Table: COMPROBANTES_DETALLES                                 */
/*==============================================================*/
create table COMPROBANTES_DETALLES (
   COMPRA               D_INT4               not null,
   PRODUCTO             D_INT4               not null,
   IMPORTE              D_NUMERIC12_2        not null,
   CANTIDAD             D_NUMERIC10_2        not null,
   IMPUESTO             D_CHAR1              not null,
   SUBTOTAL             D_NUMERIC12_2        not null,
   CAMBIO               D_NUMERIC12_2        not null,
   constraint PK_COMPROBANTES_DETALLES primary key (COMPRA, PRODUCTO)
);

/*==============================================================*/
/* Table: GASTOS                                                */
/*==============================================================*/
create table GASTOS (
   GASTO                D_INT4               not null,
   SUCURSAL             D_INT2               not null,
   PROVEEDOR            D_INT4               not null,
   PRESUPUESTO_VTA      D_INT4               null,
   MONEDA               D_INT2               not null,
   CLASIFICADOR         D_INT2               not null,
   NRO_COMPROBANTE      D_VARCHAR20          not null,
   FECHA                D_TIMESTAMP          not null,
   CONDICION            D_CHAR1              not null,
   TOTAL_COMPRA         D_NUMERIC12_2        not null,
   TOTAL_IMPUESTO       D_NUMERIC12_2        not null,
   ESTADO               D_CHAR1              not null,
   constraint PK_GASTOS primary key (GASTO)
);

/*==============================================================*/
/* Table: MONEDAS                                               */
/*==============================================================*/
create table MONEDAS (
   MONEDA               SERIAL               not null,
   NOMBRE               D_VARCHAR50          not null,
   SIGLA                D_VARCHAR15          not null,
   constraint PK_MONEDAS primary key (MONEDA)
);

/*==============================================================*/
/* Index: MONEDAS_SIGLA                                         */
/*==============================================================*/
create unique index MONEDAS_SIGLA on MONEDAS (
SIGLA
);

/*==============================================================*/
/* Table: PRESUPUESTOS_DETALLES                                 */
/*==============================================================*/
create table PRESUPUESTOS_DETALLES (
   PRESUPUESTO          D_INT4               not null,
   PRODUCTO             D_INT4               not null,
   CANTIDAD             D_NUMERIC12_2        not null,
   PRECIO               D_NUMERIC12_2        not null,
   SUBTOTAL             D_NUMERIC12_2        not null,
   constraint PK_PRESUPUESTOS_DETALLES primary key (PRESUPUESTO, PRODUCTO)
);

/*==============================================================*/
/* Table: PRESUPUESTOS_VENTAS                                   */
/*==============================================================*/
create table PRESUPUESTOS_VENTAS (
   PRESUPUESTO          D_INT4               not null,
   SUCURSAL             D_INT2               not null,
   CLIENTE              D_INT4               not null,
   NOMBRE               D_VARCHAR50          not null,
   FECHA_PRESUPUESTO    D_TIMESTAMP          not null,
   MONTO_PRESUPUESTO    D_NUMERIC12_2        not null,
   ENCARGADO            D_VARCHAR50          not null,
   CONDICION            D_CHAR1              not null,
   ESTADO               D_CHAR1              not null,
   OBSERVACION          D_VARCHAR200         null,
   constraint PK_PRESUPUESTOS_VENTAS primary key (PRESUPUESTO)
);

/*==============================================================*/
/* Table: PRODUCTOS                                             */
/*==============================================================*/
create table PRODUCTOS (
   PRODUCTO             D_INT4               not null,
   CODIGO               D_VARCHAR15          not null,
   NOMBRE               D_VARCHAR50          not null,
   DESCRIPCION          D_VARCHAR200         not null,
   CLASIFICACION        D_INT2               null,
   UNIDAD               D_INT2               not null,
   IMPUESTO             D_CHAR1              not null,
   HABILITADO           D_CHAR1              not null,
   constraint PK_PRODUCTOS primary key (PRODUCTO)
);

/*==============================================================*/
/* Index: PRODUCTOS_CODIGO                                      */
/*==============================================================*/
create unique index PRODUCTOS_CODIGO on PRODUCTOS (
CODIGO
);

/*==============================================================*/
/* Table: PROVEEDORES                                           */
/*==============================================================*/
create table PROVEEDORES (
   PROVEEDOR            SERIAL               not null,
   RAZON_SOCIAL         D_VARCHAR50          not null,
   DOCUMENTO            D_VARCHAR20          not null,
   DIRECCION            D_VARCHAR50          not null,
   PAIS                 D_VARCHAR50          not null,
   CIUDAD               D_VARCHAR50          not null,
   LOCALIDAD            D_VARCHAR50          not null,
   TELEFONO             D_VARCHAR20          not null,
   EMAIL                D_VARCHAR200         not null,
   PERSONERIA           D_CHAR1              not null,
   constraint PK_PROVEEDORES primary key (PROVEEDOR)
);

/*==============================================================*/
/* Index: PROVEEDORES_DOCUMENTO                                 */
/*==============================================================*/
create unique index PROVEEDORES_DOCUMENTO on PROVEEDORES (
DOCUMENTO
);

/*==============================================================*/
/* Table: SUCURSALES                                            */
/*==============================================================*/
create table SUCURSALES (
   SUCURSAL             SERIAL               not null,
   NOMBRE               D_VARCHAR50          not null,
   RUC                  D_VARCHAR20          not null,
   DIRECCION            D_VARCHAR50          not null,
   CIUDAD               D_VARCHAR50          not null,
   BARRIO               D_VARCHAR50          not null,
   TELEFONO             D_VARCHAR20          not null,
   EMAIL                D_VARCHAR50          not null,
   PAGINA_WEB           D_VARCHAR200         null,
   constraint PK_SUCURSALES primary key (SUCURSAL)
);

/*==============================================================*/
/* Index: SUCURSALES_NOMBRE                                     */
/*==============================================================*/
create unique index SUCURSALES_NOMBRE on SUCURSALES (
NOMBRE
);

/*==============================================================*/
/* Table: UNIDADES                                              */
/*==============================================================*/
create table UNIDADES (
   UNIDAD               D_INT2               not null,
   NOMBRE               D_VARCHAR50          not null,
   SIGLA                D_VARCHAR15          not null,
   constraint PK_UNIDADES primary key (UNIDAD)
);

/*==============================================================*/
/* Index: UNIDADES_SIGLA                                        */
/*==============================================================*/
create unique index UNIDADES_SIGLA on UNIDADES (
SIGLA
);

/*==============================================================*/
/* Table: USUARIOS                                              */
/*==============================================================*/
create table USUARIOS (
   USUARIO              D_VARCHAR20          not null,
   CLAVE                D_VARCHAR50          not null,
   NOMBRES              D_VARCHAR50          not null,
   FECHA_REGISTRO       TIMESTAMP            not null,
   PERFIL               VARCHAR(2)           not null,
   ESTADO               VARCHAR(2)           not null,
   constraint PK_USUARIOS primary key (USUARIO)
);

alter table AREAS
   add constraint FK_AREAS_REFERENCE_SUCURSAL foreign key (SUCURSAL)
      references SUCURSALES (SUCURSAL)
      on delete restrict on update restrict;

alter table AUDITORIAS
   add constraint FK_AUDITORI_REFERENCE_USUARIOS foreign key (USUARIO)
      references USUARIOS (USUARIO)
      on delete restrict on update restrict;

alter table COMPROBANTES_DETALLES
   add constraint FK_COMPROBA_REFERENCE_GASTOS foreign key (COMPRA)
      references GASTOS (GASTO)
      on delete restrict on update restrict;

alter table COMPROBANTES_DETALLES
   add constraint FK_COMPROBA_REFERENCE_PRODUCTO foreign key (PRODUCTO)
      references PRODUCTOS (PRODUCTO)
      on delete restrict on update restrict;

alter table GASTOS
   add constraint FK_GASTOS_REFERENCE_PRESUPUE foreign key (PRESUPUESTO_VTA)
      references PRESUPUESTOS_VENTAS (PRESUPUESTO)
      on delete restrict on update restrict;

alter table GASTOS
   add constraint FK_GASTOS_REFERENCE_MONEDAS foreign key (MONEDA)
      references MONEDAS (MONEDA)
      on delete restrict on update restrict;

alter table GASTOS
   add constraint FK_GASTOS_REFERENCE_CLASIFIC foreign key (CLASIFICADOR)
      references CLASIFICADOR_GASTOS (CLASIFICADOR)
      on delete restrict on update restrict;

alter table GASTOS
   add constraint FK_GASTOS_REFERENCE_SUCURSAL foreign key (SUCURSAL)
      references SUCURSALES (SUCURSAL)
      on delete restrict on update restrict;

alter table GASTOS
   add constraint FK_GASTOS_REFERENCE_PROVEEDO foreign key (PROVEEDOR)
      references PROVEEDORES (PROVEEDOR)
      on delete restrict on update restrict;

alter table PRESUPUESTOS_DETALLES
   add constraint FK_PRESUPUE_REFERENCE_PRESUPUE foreign key (PRESUPUESTO)
      references PRESUPUESTOS_VENTAS (PRESUPUESTO)
      on delete cascade on update restrict;

alter table PRESUPUESTOS_DETALLES
   add constraint FK_PRESUPUE_REFERENCE_PRODUCTO foreign key (PRODUCTO)
      references PRODUCTOS (PRODUCTO)
      on delete restrict on update restrict;

alter table PRESUPUESTOS_VENTAS
   add constraint FK_PRESUPUE_REFERENCE_SUCURSAL foreign key (SUCURSAL)
      references SUCURSALES (SUCURSAL)
      on delete restrict on update restrict;

alter table PRESUPUESTOS_VENTAS
   add constraint FK_PRESUPUE_REFERENCE_CLIENTES foreign key (CLIENTE)
      references CLIENTES (CLIENTE)
      on delete restrict on update restrict;

alter table PRODUCTOS
   add constraint FK_PRODUCTO_REFERENCE_CLASIFIC foreign key (CLASIFICACION)
      references CLASIFICACIONES (CLASIFICACION)
      on delete restrict on update restrict;

alter table PRODUCTOS
   add constraint FK_PRODUCTO_REFERENCE_UNIDADES foreign key (UNIDAD)
      references UNIDADES (UNIDAD)
      on delete restrict on update restrict;

