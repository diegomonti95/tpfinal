PGDMP                     
    x           importadora    10.5    12.3 s    �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    24633    importadora    DATABASE     �   CREATE DATABASE importadora WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Spanish_Paraguay.1252' LC_CTYPE = 'Spanish_Paraguay.1252';
    DROP DATABASE importadora;
                postgres    false                       1247    24634    d_char1    DOMAIN     .   CREATE DOMAIN public.d_char1 AS character(1);
    DROP DOMAIN public.d_char1;
       public          postgres    false                       1247    24635    d_date    DOMAIN     %   CREATE DOMAIN public.d_date AS date;
    DROP DOMAIN public.d_date;
       public          postgres    false                       1247    24636    d_int2    DOMAIN     )   CREATE DOMAIN public.d_int2 AS smallint;
    DROP DOMAIN public.d_int2;
       public          postgres    false                       1247    24637    d_int4    DOMAIN     (   CREATE DOMAIN public.d_int4 AS integer;
    DROP DOMAIN public.d_int4;
       public          postgres    false            a           1247    24638    d_numeric10_2    DOMAIN     5   CREATE DOMAIN public.d_numeric10_2 AS numeric(10,2);
 "   DROP DOMAIN public.d_numeric10_2;
       public          postgres    false            b           1247    24639    d_numeric12    DOMAIN     3   CREATE DOMAIN public.d_numeric12 AS numeric(12,0);
     DROP DOMAIN public.d_numeric12;
       public          postgres    false            c           1247    24640    d_numeric12_2    DOMAIN     5   CREATE DOMAIN public.d_numeric12_2 AS numeric(12,2);
 "   DROP DOMAIN public.d_numeric12_2;
       public          postgres    false            d           1247    24641    d_timestamp    DOMAIN     A   CREATE DOMAIN public.d_timestamp AS timestamp without time zone;
     DROP DOMAIN public.d_timestamp;
       public          postgres    false            e           1247    24642    d_varchar15    DOMAIN     ;   CREATE DOMAIN public.d_varchar15 AS character varying(15);
     DROP DOMAIN public.d_varchar15;
       public          postgres    false            f           1247    24643    d_varchar20    DOMAIN     ;   CREATE DOMAIN public.d_varchar20 AS character varying(20);
     DROP DOMAIN public.d_varchar20;
       public          postgres    false            g           1247    24644    d_varchar200    DOMAIN     =   CREATE DOMAIN public.d_varchar200 AS character varying(200);
 !   DROP DOMAIN public.d_varchar200;
       public          postgres    false            h           1247    24645    d_varchar50    DOMAIN     ;   CREATE DOMAIN public.d_varchar50 AS character varying(50);
     DROP DOMAIN public.d_varchar50;
       public          postgres    false            �            1259    24646    areas    TABLE     �   CREATE TABLE public.areas (
    area integer NOT NULL,
    sucursal public.d_int2 NOT NULL,
    nombre public.d_varchar50 NOT NULL
);
    DROP TABLE public.areas;
       public            postgres    false    616    528            �           0    0    TABLE areas    COMMENT     c   COMMENT ON TABLE public.areas IS 'Logisica
Cimiento
Techo
Andamios
Parte Electrica
Parte de Agua';
          public          postgres    false    196            �            1259    24652    areas_area_seq    SEQUENCE     �   CREATE SEQUENCE public.areas_area_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public.areas_area_seq;
       public          postgres    false    196            �           0    0    areas_area_seq    SEQUENCE OWNED BY     A   ALTER SEQUENCE public.areas_area_seq OWNED BY public.areas.area;
          public          postgres    false    197            �            1259    24654 
   auditorias    TABLE       CREATE TABLE public.auditorias (
    auditoria integer NOT NULL,
    usuario public.d_varchar20 NOT NULL,
    tabla public.d_varchar50 NOT NULL,
    fecha public.d_timestamp NOT NULL,
    observacion public.d_varchar200 NOT NULL,
    accion public.d_char1 NOT NULL
);
    DROP TABLE public.auditorias;
       public            postgres    false    615    616    612    614    526            �            1259    24660    auditorias_auditoria_seq    SEQUENCE     �   CREATE SEQUENCE public.auditorias_auditoria_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.auditorias_auditoria_seq;
       public          postgres    false    198            �           0    0    auditorias_auditoria_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.auditorias_auditoria_seq OWNED BY public.auditorias.auditoria;
          public          postgres    false    199            �            1259    24662    clasificaciones    TABLE     t   CREATE TABLE public.clasificaciones (
    clasificacion integer NOT NULL,
    nombre public.d_varchar50 NOT NULL
);
 #   DROP TABLE public.clasificaciones;
       public            postgres    false    616            �           0    0    TABLE clasificaciones    COMMENT     X   COMMENT ON TABLE public.clasificaciones IS 'Insumo
Telas
Mesas
Sillas
Sillones
Sofá
';
          public          postgres    false    200            �            1259    24668 !   clasificaciones_clasificacion_seq    SEQUENCE     �   CREATE SEQUENCE public.clasificaciones_clasificacion_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 8   DROP SEQUENCE public.clasificaciones_clasificacion_seq;
       public          postgres    false    200            �           0    0 !   clasificaciones_clasificacion_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public.clasificaciones_clasificacion_seq OWNED BY public.clasificaciones.clasificacion;
          public          postgres    false    201            �            1259    24670    clasificador_gastos    TABLE     w   CREATE TABLE public.clasificador_gastos (
    clasificador integer NOT NULL,
    nombre public.d_varchar50 NOT NULL
);
 '   DROP TABLE public.clasificador_gastos;
       public            postgres    false    616            �            1259    24676 $   clasificador_gastos_clasificador_seq    SEQUENCE     �   CREATE SEQUENCE public.clasificador_gastos_clasificador_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public.clasificador_gastos_clasificador_seq;
       public          postgres    false    202            �           0    0 $   clasificador_gastos_clasificador_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public.clasificador_gastos_clasificador_seq OWNED BY public.clasificador_gastos.clasificador;
          public          postgres    false    203            �            1259    24678    clientes    TABLE     �  CREATE TABLE public.clientes (
    cliente public.d_int4 NOT NULL,
    razon_social public.d_varchar50 NOT NULL,
    documento public.d_varchar20 NOT NULL,
    direccion public.d_varchar50 NOT NULL,
    ciudad public.d_varchar50 NOT NULL,
    barrio public.d_varchar50 NOT NULL,
    telefono_principal public.d_varchar20 NOT NULL,
    telefono_secundario public.d_varchar20,
    email public.d_varchar200 NOT NULL,
    personeria public.d_char1 NOT NULL
);
    DROP TABLE public.clientes;
       public            postgres    false    614    529    616    616    616    616    614    614    615    526            �            1259    24684    comprobantes_detalles    TABLE     I  CREATE TABLE public.comprobantes_detalles (
    compra public.d_int4 NOT NULL,
    producto public.d_int4 NOT NULL,
    importe public.d_numeric12_2 NOT NULL,
    cantidad public.d_numeric10_2 NOT NULL,
    impuesto public.d_char1 NOT NULL,
    subtotal public.d_numeric12_2 NOT NULL,
    cambio public.d_numeric12_2 NOT NULL
);
 )   DROP TABLE public.comprobantes_detalles;
       public            postgres    false    611    609    611    611    526    529    529            �            1259    24690    gastos    TABLE        CREATE TABLE public.gastos (
    gasto public.d_int4 NOT NULL,
    sucursal public.d_int2 NOT NULL,
    proveedor public.d_int4 NOT NULL,
    presupuesto_vta public.d_int4,
    moneda public.d_int2 NOT NULL,
    clasificador public.d_int2 NOT NULL,
    nro_comprobante public.d_varchar20 NOT NULL,
    fecha public.d_timestamp NOT NULL,
    condicion public.d_char1 NOT NULL,
    total_compra public.d_numeric12_2 NOT NULL,
    total_impuesto public.d_numeric12_2 NOT NULL,
    estado public.d_char1 NOT NULL
);
    DROP TABLE public.gastos;
       public            postgres    false    528    529    528    529    529    528    614    612    526    611    611    526            �            1259    24696    monedas    TABLE     �   CREATE TABLE public.monedas (
    moneda integer NOT NULL,
    nombre public.d_varchar50 NOT NULL,
    sigla public.d_varchar15 NOT NULL
);
    DROP TABLE public.monedas;
       public            postgres    false    613    616            �            1259    24702    monedas_moneda_seq    SEQUENCE     �   CREATE SEQUENCE public.monedas_moneda_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.monedas_moneda_seq;
       public          postgres    false    207            �           0    0    monedas_moneda_seq    SEQUENCE OWNED BY     I   ALTER SEQUENCE public.monedas_moneda_seq OWNED BY public.monedas.moneda;
          public          postgres    false    208            �            1259    24704    presupuestos_detalles    TABLE     �   CREATE TABLE public.presupuestos_detalles (
    presupuesto public.d_int4 NOT NULL,
    producto public.d_int4 NOT NULL,
    cantidad public.d_numeric12_2 NOT NULL,
    precio public.d_numeric12_2 NOT NULL,
    subtotal public.d_numeric12_2 NOT NULL
);
 )   DROP TABLE public.presupuestos_detalles;
       public            postgres    false    611    611    529    529    611            �            1259    24710    presupuestos_ventas    TABLE     �  CREATE TABLE public.presupuestos_ventas (
    presupuesto public.d_int4 NOT NULL,
    sucursal public.d_int2 NOT NULL,
    cliente public.d_int4 NOT NULL,
    nombre public.d_varchar50 NOT NULL,
    fecha_presupuesto public.d_timestamp NOT NULL,
    monto_presupuesto public.d_numeric12_2 NOT NULL,
    encargado public.d_varchar50 NOT NULL,
    condicion public.d_char1 NOT NULL,
    estado public.d_char1 NOT NULL,
    observacion public.d_varchar200
);
 '   DROP TABLE public.presupuestos_ventas;
       public            postgres    false    615    526    529    528    529    616    612    611    616    526            �            1259    24716 	   productos    TABLE     W  CREATE TABLE public.productos (
    producto public.d_int4 NOT NULL,
    codigo public.d_varchar15 NOT NULL,
    nombre public.d_varchar50 NOT NULL,
    descripcion public.d_varchar200 NOT NULL,
    clasificacion public.d_int2,
    unidad public.d_int2 NOT NULL,
    impuesto public.d_char1 NOT NULL,
    habilitado public.d_char1 NOT NULL
);
    DROP TABLE public.productos;
       public            postgres    false    616    615    528    526    526    528    529    613            �            1259    24722    proveedores    TABLE     �  CREATE TABLE public.proveedores (
    proveedor integer NOT NULL,
    razon_social public.d_varchar50 NOT NULL,
    documento public.d_varchar20 NOT NULL,
    direccion public.d_varchar50 NOT NULL,
    pais public.d_varchar50 NOT NULL,
    ciudad public.d_varchar50 NOT NULL,
    localidad public.d_varchar50 NOT NULL,
    telefono public.d_varchar20 NOT NULL,
    email public.d_varchar200 NOT NULL,
    personeria public.d_char1 NOT NULL
);
    DROP TABLE public.proveedores;
       public            postgres    false    616    616    614    616    616    616    614    615    526            �            1259    24728    proveedores_proveedor_seq    SEQUENCE     �   CREATE SEQUENCE public.proveedores_proveedor_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public.proveedores_proveedor_seq;
       public          postgres    false    212            �           0    0    proveedores_proveedor_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public.proveedores_proveedor_seq OWNED BY public.proveedores.proveedor;
          public          postgres    false    213            �            1259    24730 
   sucursales    TABLE       CREATE TABLE public.sucursales (
    sucursal integer NOT NULL,
    nombre public.d_varchar50 NOT NULL,
    ruc public.d_varchar20 NOT NULL,
    direccion public.d_varchar50 NOT NULL,
    ciudad public.d_varchar50 NOT NULL,
    barrio public.d_varchar50 NOT NULL,
    telefono public.d_varchar20 NOT NULL,
    email public.d_varchar50 NOT NULL,
    pagina_web public.d_varchar200
);
    DROP TABLE public.sucursales;
       public            postgres    false    614    616    615    616    614    616    616    616            �            1259    24736    sucursales_sucursal_seq    SEQUENCE     �   CREATE SEQUENCE public.sucursales_sucursal_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.sucursales_sucursal_seq;
       public          postgres    false    214            �           0    0    sucursales_sucursal_seq    SEQUENCE OWNED BY     S   ALTER SEQUENCE public.sucursales_sucursal_seq OWNED BY public.sucursales.sucursal;
          public          postgres    false    215            �            1259    24738    unidades    TABLE     �   CREATE TABLE public.unidades (
    unidad public.d_int2 NOT NULL,
    nombre public.d_varchar50 NOT NULL,
    sigla public.d_varchar15 NOT NULL
);
    DROP TABLE public.unidades;
       public            postgres    false    528    616    613            �            1259    24744    usuarios    TABLE     '  CREATE TABLE public.usuarios (
    usuario public.d_varchar20 NOT NULL,
    clave public.d_varchar50 NOT NULL,
    nombres public.d_varchar50 NOT NULL,
    fecha_registro timestamp without time zone NOT NULL,
    perfil character varying(2) NOT NULL,
    estado character varying(2) NOT NULL
);
    DROP TABLE public.usuarios;
       public            postgres    false    614    616    616            �
           2604    24750 
   areas area    DEFAULT     h   ALTER TABLE ONLY public.areas ALTER COLUMN area SET DEFAULT nextval('public.areas_area_seq'::regclass);
 9   ALTER TABLE public.areas ALTER COLUMN area DROP DEFAULT;
       public          postgres    false    197    196            �
           2604    24751    auditorias auditoria    DEFAULT     |   ALTER TABLE ONLY public.auditorias ALTER COLUMN auditoria SET DEFAULT nextval('public.auditorias_auditoria_seq'::regclass);
 C   ALTER TABLE public.auditorias ALTER COLUMN auditoria DROP DEFAULT;
       public          postgres    false    199    198            �
           2604    24752    clasificaciones clasificacion    DEFAULT     �   ALTER TABLE ONLY public.clasificaciones ALTER COLUMN clasificacion SET DEFAULT nextval('public.clasificaciones_clasificacion_seq'::regclass);
 L   ALTER TABLE public.clasificaciones ALTER COLUMN clasificacion DROP DEFAULT;
       public          postgres    false    201    200            �
           2604    24753     clasificador_gastos clasificador    DEFAULT     �   ALTER TABLE ONLY public.clasificador_gastos ALTER COLUMN clasificador SET DEFAULT nextval('public.clasificador_gastos_clasificador_seq'::regclass);
 O   ALTER TABLE public.clasificador_gastos ALTER COLUMN clasificador DROP DEFAULT;
       public          postgres    false    203    202            �
           2604    24754    monedas moneda    DEFAULT     p   ALTER TABLE ONLY public.monedas ALTER COLUMN moneda SET DEFAULT nextval('public.monedas_moneda_seq'::regclass);
 =   ALTER TABLE public.monedas ALTER COLUMN moneda DROP DEFAULT;
       public          postgres    false    208    207            �
           2604    24755    proveedores proveedor    DEFAULT     ~   ALTER TABLE ONLY public.proveedores ALTER COLUMN proveedor SET DEFAULT nextval('public.proveedores_proveedor_seq'::regclass);
 D   ALTER TABLE public.proveedores ALTER COLUMN proveedor DROP DEFAULT;
       public          postgres    false    213    212            �
           2604    24756    sucursales sucursal    DEFAULT     z   ALTER TABLE ONLY public.sucursales ALTER COLUMN sucursal SET DEFAULT nextval('public.sucursales_sucursal_seq'::regclass);
 B   ALTER TABLE public.sucursales ALTER COLUMN sucursal DROP DEFAULT;
       public          postgres    false    215    214            �          0    24646    areas 
   TABLE DATA           7   COPY public.areas (area, sucursal, nombre) FROM stdin;
    public          postgres    false    196   �       �          0    24654 
   auditorias 
   TABLE DATA           [   COPY public.auditorias (auditoria, usuario, tabla, fecha, observacion, accion) FROM stdin;
    public          postgres    false    198   �       �          0    24662    clasificaciones 
   TABLE DATA           @   COPY public.clasificaciones (clasificacion, nombre) FROM stdin;
    public          postgres    false    200   "�       �          0    24670    clasificador_gastos 
   TABLE DATA           C   COPY public.clasificador_gastos (clasificador, nombre) FROM stdin;
    public          postgres    false    202   ?�       �          0    24678    clientes 
   TABLE DATA           �   COPY public.clientes (cliente, razon_social, documento, direccion, ciudad, barrio, telefono_principal, telefono_secundario, email, personeria) FROM stdin;
    public          postgres    false    204   \�       �          0    24684    comprobantes_detalles 
   TABLE DATA           p   COPY public.comprobantes_detalles (compra, producto, importe, cantidad, impuesto, subtotal, cambio) FROM stdin;
    public          postgres    false    205   y�       �          0    24690    gastos 
   TABLE DATA           �   COPY public.gastos (gasto, sucursal, proveedor, presupuesto_vta, moneda, clasificador, nro_comprobante, fecha, condicion, total_compra, total_impuesto, estado) FROM stdin;
    public          postgres    false    206   ��       �          0    24696    monedas 
   TABLE DATA           8   COPY public.monedas (moneda, nombre, sigla) FROM stdin;
    public          postgres    false    207   ��       �          0    24704    presupuestos_detalles 
   TABLE DATA           b   COPY public.presupuestos_detalles (presupuesto, producto, cantidad, precio, subtotal) FROM stdin;
    public          postgres    false    209   Д       �          0    24710    presupuestos_ventas 
   TABLE DATA           �   COPY public.presupuestos_ventas (presupuesto, sucursal, cliente, nombre, fecha_presupuesto, monto_presupuesto, encargado, condicion, estado, observacion) FROM stdin;
    public          postgres    false    210   �       �          0    24716 	   productos 
   TABLE DATA           w   COPY public.productos (producto, codigo, nombre, descripcion, clasificacion, unidad, impuesto, habilitado) FROM stdin;
    public          postgres    false    211   
�       �          0    24722    proveedores 
   TABLE DATA           �   COPY public.proveedores (proveedor, razon_social, documento, direccion, pais, ciudad, localidad, telefono, email, personeria) FROM stdin;
    public          postgres    false    212   '�       �          0    24730 
   sucursales 
   TABLE DATA           s   COPY public.sucursales (sucursal, nombre, ruc, direccion, ciudad, barrio, telefono, email, pagina_web) FROM stdin;
    public          postgres    false    214   D�       �          0    24738    unidades 
   TABLE DATA           9   COPY public.unidades (unidad, nombre, sigla) FROM stdin;
    public          postgres    false    216   a�       �          0    24744    usuarios 
   TABLE DATA           [   COPY public.usuarios (usuario, clave, nombres, fecha_registro, perfil, estado) FROM stdin;
    public          postgres    false    217   ~�       �           0    0    areas_area_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.areas_area_seq', 1, false);
          public          postgres    false    197            �           0    0    auditorias_auditoria_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public.auditorias_auditoria_seq', 1, false);
          public          postgres    false    199            �           0    0 !   clasificaciones_clasificacion_seq    SEQUENCE SET     P   SELECT pg_catalog.setval('public.clasificaciones_clasificacion_seq', 1, false);
          public          postgres    false    201            �           0    0 $   clasificador_gastos_clasificador_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public.clasificador_gastos_clasificador_seq', 1, false);
          public          postgres    false    203            �           0    0    monedas_moneda_seq    SEQUENCE SET     A   SELECT pg_catalog.setval('public.monedas_moneda_seq', 1, false);
          public          postgres    false    208            �           0    0    proveedores_proveedor_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public.proveedores_proveedor_seq', 1, false);
          public          postgres    false    213            �           0    0    sucursales_sucursal_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.sucursales_sucursal_seq', 1, false);
          public          postgres    false    215            �
           2606    24758    areas pk_areas 
   CONSTRAINT     N   ALTER TABLE ONLY public.areas
    ADD CONSTRAINT pk_areas PRIMARY KEY (area);
 8   ALTER TABLE ONLY public.areas DROP CONSTRAINT pk_areas;
       public            postgres    false    196            �
           2606    24760    auditorias pk_auditorias 
   CONSTRAINT     ]   ALTER TABLE ONLY public.auditorias
    ADD CONSTRAINT pk_auditorias PRIMARY KEY (auditoria);
 B   ALTER TABLE ONLY public.auditorias DROP CONSTRAINT pk_auditorias;
       public            postgres    false    198            �
           2606    24762 "   clasificaciones pk_clasificaciones 
   CONSTRAINT     k   ALTER TABLE ONLY public.clasificaciones
    ADD CONSTRAINT pk_clasificaciones PRIMARY KEY (clasificacion);
 L   ALTER TABLE ONLY public.clasificaciones DROP CONSTRAINT pk_clasificaciones;
       public            postgres    false    200            �
           2606    24764 *   clasificador_gastos pk_clasificador_gastos 
   CONSTRAINT     r   ALTER TABLE ONLY public.clasificador_gastos
    ADD CONSTRAINT pk_clasificador_gastos PRIMARY KEY (clasificador);
 T   ALTER TABLE ONLY public.clasificador_gastos DROP CONSTRAINT pk_clasificador_gastos;
       public            postgres    false    202            �
           2606    24766    clientes pk_clientes 
   CONSTRAINT     W   ALTER TABLE ONLY public.clientes
    ADD CONSTRAINT pk_clientes PRIMARY KEY (cliente);
 >   ALTER TABLE ONLY public.clientes DROP CONSTRAINT pk_clientes;
       public            postgres    false    204            �
           2606    24768 .   comprobantes_detalles pk_comprobantes_detalles 
   CONSTRAINT     z   ALTER TABLE ONLY public.comprobantes_detalles
    ADD CONSTRAINT pk_comprobantes_detalles PRIMARY KEY (compra, producto);
 X   ALTER TABLE ONLY public.comprobantes_detalles DROP CONSTRAINT pk_comprobantes_detalles;
       public            postgres    false    205    205            �
           2606    24770    gastos pk_gastos 
   CONSTRAINT     Q   ALTER TABLE ONLY public.gastos
    ADD CONSTRAINT pk_gastos PRIMARY KEY (gasto);
 :   ALTER TABLE ONLY public.gastos DROP CONSTRAINT pk_gastos;
       public            postgres    false    206            �
           2606    24772    monedas pk_monedas 
   CONSTRAINT     T   ALTER TABLE ONLY public.monedas
    ADD CONSTRAINT pk_monedas PRIMARY KEY (moneda);
 <   ALTER TABLE ONLY public.monedas DROP CONSTRAINT pk_monedas;
       public            postgres    false    207            �
           2606    24774 .   presupuestos_detalles pk_presupuestos_detalles 
   CONSTRAINT        ALTER TABLE ONLY public.presupuestos_detalles
    ADD CONSTRAINT pk_presupuestos_detalles PRIMARY KEY (presupuesto, producto);
 X   ALTER TABLE ONLY public.presupuestos_detalles DROP CONSTRAINT pk_presupuestos_detalles;
       public            postgres    false    209    209            �
           2606    24776 *   presupuestos_ventas pk_presupuestos_ventas 
   CONSTRAINT     q   ALTER TABLE ONLY public.presupuestos_ventas
    ADD CONSTRAINT pk_presupuestos_ventas PRIMARY KEY (presupuesto);
 T   ALTER TABLE ONLY public.presupuestos_ventas DROP CONSTRAINT pk_presupuestos_ventas;
       public            postgres    false    210            �
           2606    24778    productos pk_productos 
   CONSTRAINT     Z   ALTER TABLE ONLY public.productos
    ADD CONSTRAINT pk_productos PRIMARY KEY (producto);
 @   ALTER TABLE ONLY public.productos DROP CONSTRAINT pk_productos;
       public            postgres    false    211            �
           2606    24780    proveedores pk_proveedores 
   CONSTRAINT     _   ALTER TABLE ONLY public.proveedores
    ADD CONSTRAINT pk_proveedores PRIMARY KEY (proveedor);
 D   ALTER TABLE ONLY public.proveedores DROP CONSTRAINT pk_proveedores;
       public            postgres    false    212            �
           2606    24782    sucursales pk_sucursales 
   CONSTRAINT     \   ALTER TABLE ONLY public.sucursales
    ADD CONSTRAINT pk_sucursales PRIMARY KEY (sucursal);
 B   ALTER TABLE ONLY public.sucursales DROP CONSTRAINT pk_sucursales;
       public            postgres    false    214            �
           2606    24784    unidades pk_unidades 
   CONSTRAINT     V   ALTER TABLE ONLY public.unidades
    ADD CONSTRAINT pk_unidades PRIMARY KEY (unidad);
 >   ALTER TABLE ONLY public.unidades DROP CONSTRAINT pk_unidades;
       public            postgres    false    216            �
           2606    24786    usuarios pk_usuarios 
   CONSTRAINT     W   ALTER TABLE ONLY public.usuarios
    ADD CONSTRAINT pk_usuarios PRIMARY KEY (usuario);
 >   ALTER TABLE ONLY public.usuarios DROP CONSTRAINT pk_usuarios;
       public            postgres    false    217            �
           1259    24787    areas_nombre    INDEX     Q   CREATE UNIQUE INDEX areas_nombre ON public.areas USING btree (sucursal, nombre);
     DROP INDEX public.areas_nombre;
       public            postgres    false    196    196            �
           1259    24788    clasificaciones_nombre    INDEX     [   CREATE UNIQUE INDEX clasificaciones_nombre ON public.clasificaciones USING btree (nombre);
 *   DROP INDEX public.clasificaciones_nombre;
       public            postgres    false    200            �
           1259    24789    clasificador_nombre    INDEX     \   CREATE UNIQUE INDEX clasificador_nombre ON public.clasificador_gastos USING btree (nombre);
 '   DROP INDEX public.clasificador_nombre;
       public            postgres    false    202            �
           1259    24790    clientes_documento    INDEX     S   CREATE UNIQUE INDEX clientes_documento ON public.clientes USING btree (documento);
 &   DROP INDEX public.clientes_documento;
       public            postgres    false    204            �
           1259    24791    monedas_sigla    INDEX     I   CREATE UNIQUE INDEX monedas_sigla ON public.monedas USING btree (sigla);
 !   DROP INDEX public.monedas_sigla;
       public            postgres    false    207            �
           1259    24792    productos_codigo    INDEX     O   CREATE UNIQUE INDEX productos_codigo ON public.productos USING btree (codigo);
 $   DROP INDEX public.productos_codigo;
       public            postgres    false    211            �
           1259    24793    proveedores_documento    INDEX     Y   CREATE UNIQUE INDEX proveedores_documento ON public.proveedores USING btree (documento);
 )   DROP INDEX public.proveedores_documento;
       public            postgres    false    212            �
           1259    24794    sucursales_nombre    INDEX     Q   CREATE UNIQUE INDEX sucursales_nombre ON public.sucursales USING btree (nombre);
 %   DROP INDEX public.sucursales_nombre;
       public            postgres    false    214            �
           1259    24795    unidades_sigla    INDEX     K   CREATE UNIQUE INDEX unidades_sigla ON public.unidades USING btree (sigla);
 "   DROP INDEX public.unidades_sigla;
       public            postgres    false    216            �
           2606    24796 !   areas fk_areas_reference_sucursal    FK CONSTRAINT     �   ALTER TABLE ONLY public.areas
    ADD CONSTRAINT fk_areas_reference_sucursal FOREIGN KEY (sucursal) REFERENCES public.sucursales(sucursal) ON UPDATE RESTRICT ON DELETE RESTRICT;
 K   ALTER TABLE ONLY public.areas DROP CONSTRAINT fk_areas_reference_sucursal;
       public          postgres    false    2804    214    196            �
           2606    24801 )   auditorias fk_auditori_reference_usuarios    FK CONSTRAINT     �   ALTER TABLE ONLY public.auditorias
    ADD CONSTRAINT fk_auditori_reference_usuarios FOREIGN KEY (usuario) REFERENCES public.usuarios(usuario) ON UPDATE RESTRICT ON DELETE RESTRICT;
 S   ALTER TABLE ONLY public.auditorias DROP CONSTRAINT fk_auditori_reference_usuarios;
       public          postgres    false    217    2810    198            �
           2606    24806 2   comprobantes_detalles fk_comproba_reference_gastos    FK CONSTRAINT     �   ALTER TABLE ONLY public.comprobantes_detalles
    ADD CONSTRAINT fk_comproba_reference_gastos FOREIGN KEY (compra) REFERENCES public.gastos(gasto) ON UPDATE RESTRICT ON DELETE RESTRICT;
 \   ALTER TABLE ONLY public.comprobantes_detalles DROP CONSTRAINT fk_comproba_reference_gastos;
       public          postgres    false    205    206    2789            �
           2606    24811 4   comprobantes_detalles fk_comproba_reference_producto    FK CONSTRAINT     �   ALTER TABLE ONLY public.comprobantes_detalles
    ADD CONSTRAINT fk_comproba_reference_producto FOREIGN KEY (producto) REFERENCES public.productos(producto) ON UPDATE RESTRICT ON DELETE RESTRICT;
 ^   ALTER TABLE ONLY public.comprobantes_detalles DROP CONSTRAINT fk_comproba_reference_producto;
       public          postgres    false    205    211    2798            �
           2606    24816 #   gastos fk_gastos_reference_clasific    FK CONSTRAINT     �   ALTER TABLE ONLY public.gastos
    ADD CONSTRAINT fk_gastos_reference_clasific FOREIGN KEY (clasificador) REFERENCES public.clasificador_gastos(clasificador) ON UPDATE RESTRICT ON DELETE RESTRICT;
 M   ALTER TABLE ONLY public.gastos DROP CONSTRAINT fk_gastos_reference_clasific;
       public          postgres    false    2782    206    202                        2606    24821 "   gastos fk_gastos_reference_monedas    FK CONSTRAINT     �   ALTER TABLE ONLY public.gastos
    ADD CONSTRAINT fk_gastos_reference_monedas FOREIGN KEY (moneda) REFERENCES public.monedas(moneda) ON UPDATE RESTRICT ON DELETE RESTRICT;
 L   ALTER TABLE ONLY public.gastos DROP CONSTRAINT fk_gastos_reference_monedas;
       public          postgres    false    2792    206    207                       2606    24826 #   gastos fk_gastos_reference_presupue    FK CONSTRAINT     �   ALTER TABLE ONLY public.gastos
    ADD CONSTRAINT fk_gastos_reference_presupue FOREIGN KEY (presupuesto_vta) REFERENCES public.presupuestos_ventas(presupuesto) ON UPDATE RESTRICT ON DELETE RESTRICT;
 M   ALTER TABLE ONLY public.gastos DROP CONSTRAINT fk_gastos_reference_presupue;
       public          postgres    false    2796    206    210                       2606    24831 #   gastos fk_gastos_reference_proveedo    FK CONSTRAINT     �   ALTER TABLE ONLY public.gastos
    ADD CONSTRAINT fk_gastos_reference_proveedo FOREIGN KEY (proveedor) REFERENCES public.proveedores(proveedor) ON UPDATE RESTRICT ON DELETE RESTRICT;
 M   ALTER TABLE ONLY public.gastos DROP CONSTRAINT fk_gastos_reference_proveedo;
       public          postgres    false    2801    212    206                       2606    24836 #   gastos fk_gastos_reference_sucursal    FK CONSTRAINT     �   ALTER TABLE ONLY public.gastos
    ADD CONSTRAINT fk_gastos_reference_sucursal FOREIGN KEY (sucursal) REFERENCES public.sucursales(sucursal) ON UPDATE RESTRICT ON DELETE RESTRICT;
 M   ALTER TABLE ONLY public.gastos DROP CONSTRAINT fk_gastos_reference_sucursal;
       public          postgres    false    214    206    2804                       2606    24841 2   presupuestos_ventas fk_presupue_reference_clientes    FK CONSTRAINT     �   ALTER TABLE ONLY public.presupuestos_ventas
    ADD CONSTRAINT fk_presupue_reference_clientes FOREIGN KEY (cliente) REFERENCES public.clientes(cliente) ON UPDATE RESTRICT ON DELETE RESTRICT;
 \   ALTER TABLE ONLY public.presupuestos_ventas DROP CONSTRAINT fk_presupue_reference_clientes;
       public          postgres    false    210    2785    204                       2606    24846 4   presupuestos_detalles fk_presupue_reference_presupue    FK CONSTRAINT     �   ALTER TABLE ONLY public.presupuestos_detalles
    ADD CONSTRAINT fk_presupue_reference_presupue FOREIGN KEY (presupuesto) REFERENCES public.presupuestos_ventas(presupuesto) ON UPDATE RESTRICT ON DELETE CASCADE;
 ^   ALTER TABLE ONLY public.presupuestos_detalles DROP CONSTRAINT fk_presupue_reference_presupue;
       public          postgres    false    210    209    2796                       2606    24851 4   presupuestos_detalles fk_presupue_reference_producto    FK CONSTRAINT     �   ALTER TABLE ONLY public.presupuestos_detalles
    ADD CONSTRAINT fk_presupue_reference_producto FOREIGN KEY (producto) REFERENCES public.productos(producto) ON UPDATE RESTRICT ON DELETE RESTRICT;
 ^   ALTER TABLE ONLY public.presupuestos_detalles DROP CONSTRAINT fk_presupue_reference_producto;
       public          postgres    false    2798    209    211                       2606    24856 2   presupuestos_ventas fk_presupue_reference_sucursal    FK CONSTRAINT     �   ALTER TABLE ONLY public.presupuestos_ventas
    ADD CONSTRAINT fk_presupue_reference_sucursal FOREIGN KEY (sucursal) REFERENCES public.sucursales(sucursal) ON UPDATE RESTRICT ON DELETE RESTRICT;
 \   ALTER TABLE ONLY public.presupuestos_ventas DROP CONSTRAINT fk_presupue_reference_sucursal;
       public          postgres    false    214    210    2804                       2606    24861 (   productos fk_producto_reference_clasific    FK CONSTRAINT     �   ALTER TABLE ONLY public.productos
    ADD CONSTRAINT fk_producto_reference_clasific FOREIGN KEY (clasificacion) REFERENCES public.clasificaciones(clasificacion) ON UPDATE RESTRICT ON DELETE RESTRICT;
 R   ALTER TABLE ONLY public.productos DROP CONSTRAINT fk_producto_reference_clasific;
       public          postgres    false    200    2779    211            	           2606    24866 (   productos fk_producto_reference_unidades    FK CONSTRAINT     �   ALTER TABLE ONLY public.productos
    ADD CONSTRAINT fk_producto_reference_unidades FOREIGN KEY (unidad) REFERENCES public.unidades(unidad) ON UPDATE RESTRICT ON DELETE RESTRICT;
 R   ALTER TABLE ONLY public.productos DROP CONSTRAINT fk_producto_reference_unidades;
       public          postgres    false    216    2807    211            �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �      x������ � �      �   �   x����N1Ek�W��g�L�M*D�hf=�h���Q��x�@�9��4�� �"��Ϭc�ZE灵R��.%���[��>L�zb���qn��"�w����~�I��)x���k$L�Y�KD�B��4r����������g}�,�>f-(.qu� jm�]��eS�c(���wb��\�˵�co4��7��n��/�W��t��v���cq     