<?php
	include_once("lib/lib.php");
	include_once("../models/Area.php");
	$url = "../../controllers/areas-eliminar-code.php";
	$area = new Area();	
	$rows = $area->listar();
				
	$str = '<a class="btn btn-info btn-sm" href="areas-agregar.php" data-toggle="tooltip" title="Nuevo">
		    <i class="fa fa-plus"></i></a>
			<table id="lista-crud" class="hover" style="width:100%">
			<thead>
				<th>Sucursal</th>
				<th>Nombre</th>
				<th>Área</th>
				<th>Acciones</th>
			</thead>
			<tbody>';
	
	if($rows){
		foreach($rows as $registro){
			$str .= "<tr>
					<td>" . $registro["nombre_sucursal"] . "</td>
					<td>" . $registro["nombre_area"] . "</td>
					<td>" . $registro["area"] . "</td>
					<td>
						<a class='btn btn-info btn-sm' href='areas-editar.php?clave=" . 
						$registro["area"] . "'
						data-toggle='tooltip' title='Modificar'>
						<i class='fa fa-pencil'></i></a>
						<a class='btn btn-dark btn-sm' onclick='eliminar(" . '"' . 
						$registro["area"] . '"' . ',"' .$url . '"' . ");'
						data-toggle='tooltip' title='Eliminar'>
						<i class='fa fa-minus-circle'></i></a>					
					</td>
				</tr>";
		}
	}
	
	$str .= '</tbody>
			<tfoot>
				<th>Sucursal</th>
				<th>Área</th>
				<th>Nombre</th>
				<th>Acciones</th>
			</tfoot>';
			
	echo $str;
	
	dataTable("lista-crud");
?>