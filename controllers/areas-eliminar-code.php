<?php
	$clave = $_POST["clave"];
	
	include_once("../models/Area.php");
	$area = new Area();
	
	if($area->eliminar($clave)){
		insertarAuditoria('AREAS','E','Eliminado: ' . $clave);
		$area = null;
		echo "<script> listarAreas(); </script>";
	}
?>