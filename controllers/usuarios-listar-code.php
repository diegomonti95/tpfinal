<?php
	include_once("lib/lib.php");
	include_once("../models/User.php");
	$url = "../../controllers/usuarios-eliminar-code.php";
	$user = new User();	
	$rows = $user->listar();
	
			
	$str = '<a class="btn btn-info btn-sm" href="usuarios-agregar.php" data-toggle="tooltip" title="Nuevo">
		    <i class="fa fa-plus"></i></a>
			<table id="lista-crud" class="hover" style="width:100%">
			<thead>
				<th>Usuario</th>
				<th>Nombre</th>
				<th>Estado</th>
				<th>Perfil</th>
				<th>Acciones</th>
			</thead>
			<tbody>';
	
	foreach($rows as $registro){
		$e = ($registro["estado"] == "H" ? "Habilitado" : "Inhabilitado");
		$p = $registro["perfil"];
		if($p == "A") $p = "Administrador";
		elseif($p == "U") $p = "Usuario";
		elseif($p == "C") $p = "Consulta";
		
		$str .= "<tr>
					<td>" . $registro["usuario"] . "</td>
					<td>" . $registro["nombres"] . "</td>
					<td>" . $e . "</td>
					<td>" . $p . "</td>
					<td>
						<a class='btn btn-info btn-sm' href='usuarios-editar.php?clave=" . 
						$registro["usuario"] . "'
						data-toggle='tooltip' title='Modificar'>
						<i class='fa fa-pencil'></i></a>
						<a class='btn btn-dark btn-sm' onclick='eliminar(" . '"' . 
						$registro["usuario"] . '"' . ',"' .$url . '"' . ");'
						data-toggle='tooltip' title='Eliminar'>
						<i class='fa fa-minus-circle'></i></a>					
					</td>
				</tr>";
	}
	$str .= '</tbody>
			<tfoot>
				<th>Usuario</th>
				<th>Nombre</th>
				<th>Estado</th>
				<th>Perfil</th>
				<th>Acciones</th>
			</tfoot>';
			
	echo $str;
	
	dataTable("lista-crud");
?>