<?php
	include_once("lib/lib.php");
	include_once("../models/Producto.php");
	$url = "../../controllers/productos-eliminar-code.php";
	$producto = new Producto();	
	$rows = $producto->listar();
	
	$str = '<a class="btn btn-info btn-sm" href="productos-agregar.php" data-toggle="tooltip" title="Nuevo">
		    <i class="fa fa-plus"></i></a>
			<table id="lista-crud" class="hover" style="width:100%">
			<thead>
				<th>Nombre</th>
				<th>Código</th>
				<th>Descripción</th>
				<th>Clasificación</th>
				<th>Unidad</th>
				<th>Habilitado</th>
				<th>ID</th>
				<th>Acciones</th>
			</thead>
			<tbody>';
	
	if($rows){
		foreach($rows as $registro){		
			$str .= "<tr>
					<td>" . $registro["nombre_producto"] . "</td>
					<td>" . $registro["codigo"] . "</td>
					<td>" . $registro["descripcion"] . "</td>
					<td>" . $registro["nombre_clasificacion"] . "</td>
					<td>" . $registro["nombre_unidad"] . "</td>
					<td>" . $registro["habilitado"] . "</td>
					<td>" . $registro["producto"] . "</td>
					<td>
						<a class='btn btn-info btn-sm' href='productos-editar.php?clave=" . 
						$registro["producto"] . "'
						data-toggle='tooltip' title='Modificar'>
						<i class='fa fa-pencil'></i></a>
						<a class='btn btn-dark btn-sm' onclick='eliminar(" . '"' . 
						$registro["producto"] . '"' . ',"' .$url . '"' . ");'
						data-toggle='tooltip' title='Eliminar'>
						<i class='fa fa-minus-circle'></i></a>					
					</td>
				</tr>";
		}
	}
	
	$str .= '</tbody>
			<tfoot>
				<th>Nombre</th>
				<th>Código</th>
				<th>Descripción</th>
				<th>Clasificación</th>
				<th>Unidad</th>
				<th>Habilitado</th>
				<th>ID</th>
			</tfoot>';
			
	echo $str;
	
	dataTable("lista-crud");
?>