<?php
	include_once("lib/lib.php");
	
	$clasificador 	= $_POST["clasificador"];
	$nombre 	= $_POST["nombre"];
		
	include_once("clasificador-gastos-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Clasificador.php");
	$cla = new Clasificador($clasificador,$nombre);
	
	if($cla->editar())
	{
		insertarAuditoria('CLASIFICADORES_GASTOS','M','Modificado: ' . $nombre);
		$cla = null;
		echo "<script> location.href='../../views/clasificador/clasificador-gastos-listar.php';</script>";
	}
?>