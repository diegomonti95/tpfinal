<?php
	$resultadoControl = true;
		
	if($nombre === ""){
		alertar("Error","Ingrese un nombre para el Producto.","nombre");
		$resultadoControl = false;
		return;
	}
	
	if($codigo === ""){
		alertar("Error","Ingrese un codigo para el Producto.","codigo");
		$resultadoControl = false;
		return;
	}
	
	if($descripcion === ""){
		alertar("Error","Ingrese una descripción para el Producto.","descripcion");
		$resultadoControl = false;
		return;
	}
	
	if($clasificacion === ""){
		alertar("Error","Ingrese una clasificación para el Producto.","clasificacion");
		$resultadoControl = false;
		return;
	}
	
	if($unidad === ""){
		alertar("Error","Ingrese una unidad para el Producto.","unidad");
		$resultadoControl = false;
		return;
	}
	
	if($impuesto === ""){
		alertar("Error","Ingrese un impuesto para el Producto.","impuesto");
		$resultadoControl = false;
		return;
	}
?>