<?php
	$clave = $_POST["clave"];
	
	include_once("../models/Clasificador.php");
	$cla = new Clasificador();
	
	if($cla->eliminar($clave)){
		insertarAuditoria('CLASIFICADORES_GASTOS','E','Eliminado: ' . $clave);
		$cla = null;
		echo "<script> listarClasificadores(); </script>";
	}
?>