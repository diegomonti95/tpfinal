<?php
	$resultadoControl = true;
		
	if($razon_social === ""){
		alertar("Error","Ingrese Razón Social.","razon_social");
		$resultadoControl = false;
		return;
	}
	
	if(strpos($documento,"-") == 0){
		if(validar_dato($documento,"integer") == false){
			alertar("Error","Ingrese documento correcto.","documento");
			$resultadoControl = false;
			return;
		}
	}else{
		if(validar_dato($documento,"ruc") == false){
			alertar("Error","Ingrese RUC correcto.","documento");
			$resultadoControl = false;
			return;
		}
	}
	
	if($direccion === ""){
		alertar("Error","Ingrese una dirección.","direccion");
		$resultadoControl = false;
		return;
	}
	
	if($pais === ""){
		alertar("Error","Ingrese un pais.","pais");
		$resultadoControl = false;
		return;
	
	}
	
	if($ciudad === ""){
		alertar("Error","Ingrese una ciudad.","ciudad");
		$resultadoControl = false;
		return;
	}
	
	if($localidad === ""){
		alertar("Error","Ingrese una localidad.","localidad");
		$resultadoControl = false;
		return;
	}
	
	if(validar_dato($telefono_principal,"telefono") == false){
		alertar("Error","Ingrese un teléfono correcto.","telefono_principal");
		$resultadoControl = false;
		return;
	}
	
	if($telefono_secundario != "" & validar_dato($telefono_secundario,"telefono") == false){
		alertar("Error","Ingrese un teléfono correcto.","telefono_secundario");
		$resultadoControl = false;
		return;
	}
	
	if(validar_dato($email,"email") == false){
		alertar("Error","Ingrese un email en correcto.","email");
		$resultadoControl = false;
		return;
	}
?>