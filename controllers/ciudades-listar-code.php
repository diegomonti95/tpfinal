<?php
	include_once("lib/lib.php");
	include_once("../models/Ciudad.php");
	$url = "../../controllers/ciudades-eliminar-code.php";
	$ciudad = new Ciudad();	
	$rows = $ciudad->listar();
				
	$str = '<a class="btn btn-info btn-sm" href="ciudades-agregar.php" data-toggle="tooltip" title="Nuevo">
		    <i class="fa fa-plus"></i></a>
			<table id="lista-crud" class="hover" style="width:100%">
			<thead>
				<th>País</th>
				<th>Nombre</th>
				<th>ID</th>
				<th>Acciones</th>
			</thead>
			<tbody>';
	
	if($rows){
		foreach($rows as $registro){
			$str .= "<tr>
					<td>" . $registro["nombre_pais"] . "</td>
					<td>" . $registro["nombre_ciudad"] . "</td>
					<td>" . $registro["ciudad"] . "</td>
					<td>
						<a class='btn btn-info btn-sm' href='ciudades-editar.php?clave=" . 
						$registro["ciudad"] . "'
						data-toggle='tooltip' title='Modificar'>
						<i class='fa fa-pencil'></i></a>
						<a class='btn btn-dark btn-sm' onclick='eliminar(" . '"' . 
						$registro["ciudad"] . '"' . ',"' .$url . '"' . ");'
						data-toggle='tooltip' title='Eliminar'>
						<i class='fa fa-minus-circle'></i></a>					
					</td>
				</tr>";
		}
	}
	
	$str .= '</tbody>
			<tfoot>
				<th>País</th>
				<th>Ciudad</th>
				<th>ID</th>
				<th>Acciones</th>
			</tfoot>';
			
	echo $str;
	
	dataTable("lista-crud");
?>