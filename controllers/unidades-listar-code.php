<?php
	include_once("lib/lib.php");
	include_once("../models/Unidad.php");
	$url = "../../controllers/unidades-eliminar-code.php";
	$unidad = new Unidad();	
	$rows = $unidad->listar();
				
	$str = '<a class="btn btn-info btn-sm" href="unidades-agregar.php" data-toggle="tooltip" title="Nuevo">
		    <i class="fa fa-plus"></i></a>
			<table id="lista-crud" class="hover" style="width:100%">
			<thead>
				<th>Nombre</th>
				<th>Sigla</th>
				<th>Unidad</th>
				<th>Acciones</th>
			</thead>
			<tbody>';
	
	if($rows){
		foreach($rows as $registro){
			$str .= "<tr>
					<td>" . $registro["nombre"] . "</td>
					<td>" . $registro["sigla"] . "</td>
					<td>" . $registro["unidad"] . "</td>
					<td>
						<a class='btn btn-info btn-sm' href='unidades-editar.php?clave=" . 
						$registro["unidad"] . "'
						data-toggle='tooltip' title='Modificar'>
						<i class='fa fa-pencil'></i></a>
						<a class='btn btn-dark btn-sm' onclick='eliminar(" . '"' . 
						$registro["unidad"] . '"' . ',"' .$url . '"' . ");'
						data-toggle='tooltip' title='Eliminar'>
						<i class='fa fa-minus-circle'></i></a>					
					</td>
				</tr>";
		}
	}
	
	$str .= '</tbody>
			<tfoot>
				<th>Nombre</th>
				<th>Sigla</th>
				<th>Unidad</th>
				<th>Acciones</th>
			</tfoot>';
			
	echo $str;
	
	dataTable("lista-crud");
?>