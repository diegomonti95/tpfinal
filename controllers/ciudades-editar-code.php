<?php
	include_once("lib/lib.php");
	
	$ciudad 		= $_POST["ciudad"];
	$pais 	= $_POST["pais"];
	$nombre 	= $_POST["nombre"];
	
	include_once("ciudades-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/ciudad.php");
	$ciudad = new Ciudad($ciudad,
	$pais,
	$nombre);
	
	if($ciudad->editar())
	{
		insertarAuditoria('CIUDADES','M','Modificado: ' . $nombre);
		$ciudad = null;
		echo "<script> location.href='../../views/ciudad/ciudades-listar.php';</script>";
	}
?>