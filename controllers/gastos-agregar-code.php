<?php
	include_once("lib/lib.php");
	
	$sucursal = 		$_POST["sucursal"];
	$proveedor = 		$_POST["proveedor"];
	$presupuesto_vta = 	$_POST["presupuesto_vta"];
	$moneda = 			$_POST["moneda"];
	$clasificador = 	$_POST["clasificador"];
	$nro_comprobante = 	$_POST["nro_comprobante"];
	$fecha = 			$_POST["fecha"];
	$condicion = 		$_POST["condicion"];
	$total_compra = 	$_POST["total_compra"];
	$total_impuesto =   $_POST["total_impuesto"];
	$estado =	        $_POST["estado"];
		
	include_once("gastos-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Gasto.php");
	$s = new Gasto("",$sucursal,$proveedor,$presupuesto_vta,$moneda,$clasificador,$nro_comprobante,$fecha,$condicion,$total_compra,$total_impuesto,$estado);
	
	if($s->agregar() == true){
		insertarAuditoria('GASTOS','I','Insertado: ' . $sucursal);
		$s=null;
		echo "<script> location.href='../../views/gasto/gastos-listar.php'; </script>";
	}
	
?>