// Funciones Javascript
function cerrar(obj){
	obj = "." + obj;
	$(obj).remove();
}

function acceder(){
	var data = {"user":$("#user").val(),
				"password":$("#password").val()
			   };
			   
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/acceso-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

// Borrar
function eliminar(clave,url){
	var confirmacion = confirm("⌂ Está a punto de eliminar un dato.\nDesea Continuar?");
	if(confirmacion){
		var data={"clave":clave};
		$.ajax({
			data:data,
			type:'post',
			url:url,
			success:function(response){
				$("#result-eliminar").html(response);
			}		
		});
	}
}

// Users
function listarUsers(){
	$.ajax({
		type:'post',
		url:'../../controllers/usuarios-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarUser(){
	var estado = ($("#estado:checked").val() == "on") ? "H" : "I";
	var data = {"usuario":$("#usuario").val(),
				"clave":$("#clave").val(),
				"nombres":$("#nombres").val(),
				"fecha_registro":$("#fecha_registro").val(),
				"perfil":$("#perfil").val(),
				"estado":estado
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/usuarios-agregar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function editarUser(){
	var estado = ($("#estado:checked").val() == "on") ? "H" : "I";
	var data = {"usuario":$("#usuario").val(),
				"clave":$("#clave").val(),
				"nombres":$("#nombres").val(),
				"fecha_registro":$("#fecha_registro").val(),
				"perfil":$("#perfil").val(),
				"estado":estado
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/usuarios-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

// Sucursales
function listarSucursales(){
	$.ajax({
		type:'post',
		url:'../../controllers/sucursales-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarSucursal(){
	var data = {"nombre":$("#nombre").val(),
				"ruc":$("#ruc").val(),
				"direccion":$("#direccion").val(),
				"ciudad":$("#ciudad").val(),
				"localidad":$("#localidad").val(),
				"telefono":$("#telefono").val(),
				"email":$("#email").val(),
				"pagina_web":$("#pagina_web").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/sucursales-agregar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function editarSucursal(){
	var data = {"sucursal":$("#sucursal").val(),
				"nombre":$("#nombre").val(),
				"ruc":$("#ruc").val(),
				"direccion":$("#direccion").val(),
				"ciudad":$("#ciudad").val(),
				"localidad":$("#localidad").val(),
				"telefono":$("#telefono").val(),
				"email":$("#email").val(),
				"pagina_web":$("#pagina_web").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/sucursales-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

// Areas
function listarAreas(){
	$.ajax({
		type:'post',
		url:'../../controllers/areas-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarArea(){
	var data = {"sucursal":$("#sucursal").val(),
				"nombre":$("#nombre").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/areas-agregar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function editarArea(){
	var data = {"area":$("#area").val(),
				"sucursal":$("#sucursal").val(),
				"nombre":$("#nombre").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/areas-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

// Monedas
function listarMonedas(){
	$.ajax({
		type:'post',
		url:'../../controllers/monedas-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarMoneda(){
	var data = {"nombre":$("#nombre").val(),
				"sigla":$("#sigla").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/monedas-agregar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function editarMoneda(){
	var data = {"moneda":$("#moneda").val(),
				"nombre":$("#nombre").val(),
				"sigla":$("#sigla").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/monedas-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}


////Clasificacion
function listarClasificaciones(){
	$.ajax({
		type:'post',
		url:'../../controllers/clasificaciones-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarClasificacion(){
	var data = {
				"nombre":$("#nombre").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/clasificaciones-agregar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function editarClasificacion(){
	var data = {"clasificacion":$("#clasificacion").val(),
				"nombre":$("#nombre").val()				
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/clasificaciones-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}
//Unidades
function listarUnidades(){
	$.ajax({
		type:'post',
		url:'../../controllers/unidades-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarUnidad(){
	var data = {"nombre":$("#nombre").val(),
				"sigla":$("#sigla").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/unidades-agregar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function editarUnidad(){
	var data = {"unidad":$("#unidad").val(),
				"nombre":$("#nombre").val(),
				"sigla":$("#sigla").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/unidades-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}
//Clasificador
function listarClasificadores(){
	$.ajax({
		type:'post',
		url:'../../controllers/clasificador-gastos-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarClasificador(){
	var data = {
				"nombre":$("#nombre").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/clasificador-gastos-agregar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function editarClasificador(){
	var data = {"clasificador":$("#clasificador").val(),
				"nombre":$("#nombre").val()				
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/clasificador-gastos-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

// Clientes
function listarClientes(){
	$.ajax({
		type:'post',
		url:'../../controllers/clientes-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarCliente(){
	var data = {
				"razon_social":$("#razon_social").val(),
				"documento":$("#documento").val(),
				"direccion":$("#direccion").val(),
				"pais":$("#pais").val(),
				"ciudad":$("#ciudad").val(),
				"localidad":$("#localidad").val(),
				"telefono_principal":$("#telefono_principal").val(),
				"telefono_secundario":$("#telefono_secundario").val(),
				"email":$("#email").val(),
				"personeria":$("#personeria").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/clientes-agregar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function editarCliente(){
	var data = {
				"cliente":$("#cliente").val(),
				"razon_social":$("#razon_social").val(),
				"documento":$("#documento").val(),
				"direccion":$("#direccion").val(),
				"pais":$("#pais").val(),
				"ciudad":$("#ciudad").val(),
				"localidad":$("#localidad").val(),
				"telefono_principal":$("#telefono_principal").val(),
				"telefono_secundario":$("#telefono_secundario").val(),
				"email":$("#email").val(),
				"personeria":$("#personeria").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/clientes-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

// Productos
function listarProductos(){
	$.ajax({
		type:'post',
		url:'../../controllers/productos-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarProducto(){
	var habilitado = ($("#habilitado:checked").val() == "on") ? "S" : "N";
	var data = {"producto":$("#producto").val(),
				"codigo":$("#codigo").val(),
				"nombre":$("#nombre").val(),	
				"descripcion":$("#descripcion").val(),
				"clasificacion":$("#clasificacion").val(),
				"unidad":$("#unidad").val(),
				"impuesto":$("#impuesto").val(),
				"habilitado":habilitado
			   };
			   
		$.ajax({
			data:data,
			type:'post',
			url:'../../controllers/productos-agregar-code.php',
			success:function(response){
				$("#result").html(response);
			}		
		});
}

function editarProducto(){
	var habilitado = ($("#habilitado:checked").val() == "on") ? "S" : "N";
	var data = {"producto":$("#producto").val(),
				"codigo":$("#codigo").val(),
				"nombre":$("#nombre").val(),
				"descripcion":$("#descripcion").val(),
				"clasificacion":$("#clasificacion").val(),
				"unidad":$("#unidad").val(),
				"impuesto":$("#impuesto").val(),
				"habilitado":habilitado};	   
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/productos-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

//Proveedores
function listarProveedores(){
	$.ajax({
		type:'post',
		url:'../../controllers/proveedores-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarProveedor(){
	var data = {
				"razon_social":$("#razon_social").val(),
				"documento":$("#documento").val(),
				"direccion":$("#direccion").val(),
				"pais":$("#pais").val(),
				"ciudad":$("#ciudad").val(),
				"localidad":$("#localidad").val(),
				"telefono":$("#telefono").val(),
				"email":$("#email").val(),
				"personeria":$("#personeria").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/proveedores-agregar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function editarProveedor(){
	var data = {
				"proveedor":$("#proveedor").val(),
				"razon_social":$("#razon_social").val(),
				"documento":$("#documento").val(),
				"direccion":$("#direccion").val(),
				"pais":$("#pais").val(),
				"ciudad":$("#ciudad").val(),
				"localidad":$("#localidad").val(),
				"telefono":$("#telefono").val(),
				"email":$("#email").val(),
				"personeria":$("#personeria").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/proveedores-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}


function filtrarLocalidad(){
	var data = {"ciudad":$("#ciudad").val()};
	
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/filtrar-localidad.php',
		success:function(response){
			$("#localidad").html(response);
		}		
	});
}

function filtrarCiudad(){
	var data = {"pais":$("#pais").val()};
		
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/filtrar-ciudad.php',
		success:function(response){
			$("#ciudad").html(response);
		}		
	});
}

// Paises
function listarPaises(){
	$.ajax({
		type:'post',
		url:'../../controllers/paises-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarPais(){
	var data = {"nombre":$("#nombre").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/paises-agregar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function editarPais(){
	var data = {"pais":$("#pais").val(),
				"nombre":$("#nombre").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/paises-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

//Localidades
function listarLocalidades(){
	$.ajax({
		type:'post',
		url:'../../controllers/localidades-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarLocalidad(){
	var data = {"ciudad":$("#ciudad").val(),
				"nombre":$("#nombre").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/localidades-agregar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function editarLocalidad(){
	var data = {"localidad":$("#localidad").val(),
				"ciudad":$("#ciudad").val(),
				"nombre":$("#nombre").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/localidades-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

// Ciudades
function listarCiudades(){
	$.ajax({
		type:'post',
		url:'../../controllers/ciudades-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarCiudad(){
	var data = {"pais":$("#pais").val(),
				"nombre":$("#nombre").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/ciudades-agregar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function editarCiudad(){
	var data = {"ciudad":$("#ciudad").val(),
				"pais":$("#pais").val(),
				"nombre":$("#nombre").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/ciudades-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function reporte1(){
	var data = {"presupuesto":$("#presupuesto").val(),
				"fecha_desde":$("#fecha_desde").val(),
				"fecha_hasta":$("#fecha_hasta").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'rpt-comparativo-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

// Gastos
function listarGastos(){
	$.ajax({
		type:'post',
		url:'../../controllers/gastos-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarGasto(){
	var data = {"gasto":$("#gasto").val(),
				"sucursal":$("#sucursal").val(),
				"proveedor":$("#proveedor").val(),
				"presupuesto_vta":$("#presupuesto_vta").val(),
				"moneda":$("#moneda").val(),
				"clasificador":$("#clasificador").val(),
				"nro_comprobante":$("#nro_comprobante").val(),
				"fecha":$("#fecha").val(),
				"condicion":$("#condicion").val(),
				"total_compra":$("#total_compra").val(),
				"total_impuesto":$("#total_impuesto").val(),
				"estado":$("#estado").val()	
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/gastos-agregar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function editarGasto(){
	var data = {"gasto":$("#gasto").val(),
				"sucursal":$("#sucursal").val(),
				"proveedor":$("#proveedor").val(),
				"presupuesto_vta":$("#presupuesto_vta").val(),
				"moneda":$("#moneda").val(),
				"clasificador":$("#clasificador").val(),
				"nro_comprobante":$("#nro_comprobante").val(),
				"fecha":$("#fecha").val(),
				"condicion":$("#condicion").val(),
				"total_compra":$("#total_compra").val(),
				"total_impuesto":$("#total_impuesto").val(),
				"estado":$("#estado").val()	
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/gastos-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

// Presupuestos
function listarPresupuestos(){
	$.ajax({
		type:'post',
		url:'../../controllers/presupuestos-listar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function agregarPresupuesto(){
	var data = {"sucursal":$("#sucursal").val(),
				"cliente":$("#cliente").val(),
				"nombre":$("#nombre").val(),
				"fecha_presupuesto":$("#fecha_presupuesto").val(),
				"monto_presupuesto":$("#monto_presupuesto").val(),
				"encargado":$("#encargado").val(),
				"condicion":$("#condicion").val(),
				"estado":$("#estado").val(),
				"observacion":$("#observacion").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/presupuestos-agregar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}

function editarPresupuesto(){
	var data = {"sucursal":$("#sucursal").val(),
				"cliente":$("#cliente").val(),
				"nombre":$("#nombre").val(),
				"fecha_presupuesto":$("#fecha_presupuesto").val(),
				"monto_presupuesto":$("#monto_presupuesto").val(),
				"encargado":$("#encargado").val(),
				"condicion":$("#condicion").val(),
				"estado":$("#estado").val(),
				"observacion":$("#observacion").val()
			   };
	$.ajax({
		data:data,
		type:'post',
		url:'../../controllers/presupuestos-editar-code.php',
		success:function(response){
			$("#result").html(response);
		}		
	});
}
