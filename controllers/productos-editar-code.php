<?php
	include_once("lib/lib.php");
	
	$producto 	= $_POST["producto"];
	$codigo 	= $_POST["codigo"];
	$nombre 	= $_POST["nombre"];
	$descripcion = $_POST["descripcion"];
	$clasificacion = $_POST["clasificacion"];
	$unidad 	= $_POST["unidad"];
	$impuesto 	= $_POST["impuesto"];
	$habilitado = $_POST["habilitado"];
	
	include_once("productos-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Producto.php");
	$s = new Producto($producto,
	$codigo,
	$nombre,
	$descripcion,
	$clasificacion,
	$unidad,
	$impuesto,
	$habilitado);
	
	if($s->editar() == true)
	{
		insertarAuditoria('PRODUCTOS','M','Modificado: ' . $nombre);
		$s = null;
		echo "<script> location.href='../../views/producto/productos-listar.php';</script>";
	}
?>