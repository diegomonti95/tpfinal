<?php
	include_once("lib/lib.php");
	include_once("../models/Clasificador.php");
	$url = "../../controllers/clasificador-gastos-eliminar-code.php";
	$clasificador = new Clasificador();	
	$rows = $clasificador->listar();
				
	$str = '<a class="btn btn-info btn-sm" href="clasificador-gastos-agregar.php" data-toggle="tooltip" title="Nuevo">
		    <i class="fa fa-plus"></i></a>
			<table id="lista-crud" class="hover" style="width:100%">
			<thead>
				<th>Nombre</th>
				<th>Clasificador</th>		
				<th>Acciones</th>
			</thead>
			<tbody>';
	
	if($rows){
		foreach($rows as $registro){
			$str .= "<tr>
					<td>" . $registro["nombre"] . "</td>
					<td>" . $registro["clasificador"] . "</td>
					
					<td>
						<a class='btn btn-info btn-sm' href='clasificador-gastos-editar.php?clave=" . 
						$registro["clasificador"] . "'
						data-toggle='tooltip' title='Modificar'>
						<i class='fa fa-pencil'></i></a>
						<a class='btn btn-dark btn-sm' onclick='eliminar(" . '"' . 
						$registro["clasificador"] . '"' . ',"' .$url . '"' . ");'
						data-toggle='tooltip' title='Eliminar'>
						<i class='fa fa-minus-circle'></i></a>					
					</td>
				</tr>";
		}
	}
	
	$str .= '</tbody>
			<tfoot>
				<th>Nombre</th>
				<th>Clasificador</th>
				<th>Acciones</th>
			</tfoot>';
			
	echo $str;
	
	dataTable("lista-crud");
?>