<?php
	$resultadoControl = true;
		
	if($sucursal === ""){
		alertar("Error","Ingrese una sucursal para el Presupeusto.","sucursal");
		$resultadoControl = false;
		return;
	}
	
	if($cliente === ""){
		alertar("Error","Ingrese una cliente para el Presupuesto.","cliente");
		$resultadoControl = false;
		return;
	}
	
	if($nombre === ""){
		alertar("Error","Ingrese un nombre para el Presupeusto.","nombre");
		$resultadoControl = false;
		return;
	}
	
	if($fecha_presupuesto === ""){
		alertar("Error","Ingrese una fecha para el Presupuesto.","fecha_presupuesto");
		$resultadoControl = false;
		return;
	}
	
	if($monto_presupuesto === ""){
		alertar("Error","Ingrese un monto para el Presupuesto.","monto_presupuesto");
		$resultadoControl = false;
		return;
	}
	
	if($encargado == ""){
		alertar("Error","Ingrese un encargado para el Presupeusto.","encargado");
		$resultadoControl = false;
		return;
	}
	
	if($condicion == ""){
		alertar("Error","Ingrese una condicion para el Presupeusto.","condicion");
		$resultadoControl = false;
		return;
	}
		
	if($estado == ""){
		alertar("Error","Ingrese un estado para el Presupeusto.","estado");
		$resultadoControl = false;
		return;
	}
?>