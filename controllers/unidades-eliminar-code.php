<?php
	$clave = $_POST["clave"];
	
	include_once("../models/Unidad.php");
	$uni = new Unidad();
	
	if($uni->eliminar($clave)){
		insertarAuditoria('UNIDADES','E','Eliminado: ' . $clave);
		$uni = null;
		echo "<script> listarUnidades(); </script>";
	}
?>