<?php
	include_once("lib/lib.php");
	include_once("../models/Proveedor.php");
	$url = "../../controllers/proveedores-eliminar-code.php";
	$proveedor = new Proveedor();	
	$rows = $proveedor->listar();
	
	$str = '<a class="btn btn-info btn-sm" href="proveedores-agregar.php" data-toggle="tooltip" title="Nuevo">
		    <i class="fa fa-plus"></i></a>
			<table id="lista-crud" class="hover" style="width:100%">
			<thead>	
				<th>Razón Social</th>
				<th>Documento</th>
				<th>Dirección</th>
				<th>País</th>
				<th>Ciudad</th>
				<th>Localidad</th>
				<th>Teléfono</th>
				<th>Email</th>
				<th>Personería</th>
				<th>ID</th>
				<th>Acciones</th>
			</thead>
			<tbody>';
	
	if($rows){
		foreach($rows as $registro){		
			$str .= "<tr>
					<td>" . $registro["razon_social"] . "</td>
					<td>" . $registro["documento"] . "</td>
					<td>" . $registro["direccion"] . "</td>
					<td>" . $registro["nombre_pais"] . "</td>
					<td>" . $registro["nombre_ciudad"] . "</td>
					<td>" . $registro["nombre_localidad"] . "</td>
					<td>" . $registro["telefono"] . "</td>
					<td>" . $registro["email"] . "</td>
					<td>" . $registro["personeria"] . "</td>
					<td>" . $registro["proveedor"] . "</td>
					<td>
						<a class='btn btn-info btn-sm' href='proveedores-editar.php?clave=" . 
						$registro["proveedor"] . "'
						data-toggle='tooltip' title='Modificar'>
						<i class='fa fa-pencil'></i></a>
						<a class='btn btn-dark btn-sm' onclick='eliminar(" . '"' . 
						$registro["proveedor"] . '"' . ',"' .$url . '"' . ");'
						data-toggle='tooltip' title='Eliminar'>
						<i class='fa fa-minus-circle'></i></a>					
					</td>
				</tr>";
		}
	}
	
	$str .= '</tbody>
			<tfoot>
				<th>Razón Social</th>
				<th>Documento</th>
				<th>Dirección</th>
				<th>País</th>
				<th>Ciudad</th>
				<th>Localidad</th>
				<th>Teléfono</th>
				<th>Email</th>
				<th>Personería</th>
				<th>ID</th>
				<th>Acciones</th>
			</tfoot>';
			
	echo $str;
	
	dataTable("lista-crud");
?>