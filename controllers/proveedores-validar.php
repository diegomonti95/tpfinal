<?php
	$resultadoControl = true;
		
	if($razon_social === ""){
		alertar("Error","Ingrese Razon Social.","razon_social");
		$resultadoControl = false;
		return;
	}
	
	if(validar_dato($documento,"documento") == false){
		alertar("Error","Ingrese documento.","documento");
		$resultadoControl = false;
		return;
	}
	
	if($direccion === ""){
		alertar("Error","Ingrese una dirección.","direccion");
		$resultadoControl = false;
		return;
	}
	
	if($ciudad === ""){
		alertar("Error","Ingrese una ciudad.","ciudad");
		$resultadoControl = false;
		return;
	}
	
	if($localidad === ""){
		alertar("Error","Ingrese localidad.","localidad");
		$resultadoControl = false;
		return;
	}
	
	if(validar_dato($telefono,"integer") == false){
		alertar("Error","Ingrese un telèfono en el formato establecido.","telefono");
		$resultadoControl = false;
		return;
	}
	
	if(validar_dato($email,"email") == false){
		alertar("Error","Ingrese un mail en el formato establecido.","email");
		$resultadoControl = false;
		return;
	}
?>