<?php
	$resultadoControl = true;
	
	if($usuario === ""){
		alertar("Error","Ingrese el Usuario.","usuario");
		$resultadoControl = false;
		return;
	}
	
	if($clave === "" || strlen($clave) < 8){
		alertar("Error","Ingrese una clave de al menos 8 caracteres.","clave");
		$resultadoControl = false;
		return;
	}
	
	if($nombres === ""){
		alertar("Error","Ingrese un nombre de Usuario.","nombres");
		$resultadoControl = false;
		return;
	}
?>