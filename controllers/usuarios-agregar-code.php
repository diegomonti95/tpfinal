<?php
	include_once("lib/lib.php");
	
	$usuario = $_POST["usuario"];
	$clave = $_POST["clave"];
	$nombres = $_POST["nombres"];
	$fecha_registro = $_POST["fecha_registro"];
	$perfil = $_POST["perfil"];
	$estado = $_POST["estado"];
	
	$fecha_registro = fecha_formato($fecha_registro,true,true);
		
	include_once("usuarios-validar.php");
	if(!$resultadoControl) return;
	
	$clave = sha1($clave);
	
	include_once("../models/User.php");
	$user = new User($usuario,
	$clave,
	$nombres,
	$fecha_registro,
	$perfil,
	$estado);
	
	if($user->agregar())
	{
		insertarAuditoria('USUARIOS','I','Insertado: ' . $usuario);
		$user = null;
		echo "<script> location.href='../../views/usuario/usuarios-listar.php';</script>";
	}
?>