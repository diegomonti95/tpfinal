<?php
	include_once("lib/lib.php");
	
	$nombre = $_POST["nombre"];
		
	include_once("clasificador-gastos-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Clasificador.php");
	$cla = new Clasificador("",$nombre);
	
	if($cla->agregar())
	{
		insertarAuditoria('CLASIFICADORES_GASTOS','I','Insertado: ' . $nombre);
		$cla = null;
		echo "<script> location.href='../../views/clasificador/clasificador-gastos-listar.php';</script>";
	}
?>