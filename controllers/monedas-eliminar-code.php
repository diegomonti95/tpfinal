<?php
	$clave = $_POST["clave"];
	
	include_once("../models/Moneda.php");
	$mon = new Moneda();
	
	if($mon->eliminar($clave)){
		insertarAuditoria('MONEDAS','E','Eliminado: ' . $clave);
		$mon = null;
		echo "<script> listarMonedas(); </script>";
	}
?>