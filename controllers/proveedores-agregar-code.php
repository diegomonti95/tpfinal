<?php
	include_once("lib/lib.php");
	$razon_social = $_POST["razon_social"];
	$documento = $_POST["documento"];
	$direccion = $_POST["direccion"];
	$pais = $_POST["pais"];
	$ciudad = $_POST["ciudad"];
	$localidad = $_POST["localidad"];
	$telefono = $_POST["telefono"];
	$email = $_POST["email"];
	$personeria = $_POST["personeria"];
		
	include_once("proveedores-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Proveedor.php");
	$s = new Proveedor("",$razon_social,$documento,$direccion,$pais,$ciudad,$localidad,$telefono,$email,$personeria);
	
	if($s->agregar() == true){
		insertarAuditoria('PROVEEDORES','I','Insertado: ' . $razon_social);
		$s=null;
		echo "<script> location.href='../../views/proveedor/proveedores-listar.php'; </script>";
	}
	
?>