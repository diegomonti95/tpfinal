<?php
	include_once("lib/lib.php");
	
	$moneda		= $_POST["moneda"];
	$nombre 	= $_POST["nombre"];
	$sigla 	= $_POST["sigla"];
		
	include_once("monedas-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Moneda.php");
	$mon = new Moneda($moneda,
	$nombre,
	$sigla);
	
	if($mon->editar())
	{
		insertarAuditoria('MONEDAS','M','Modificado: ' . $nombre);
		$mon = null;
		echo "<script> location.href='../../views/moneda/monedas-listar.php';</script>";
	}
?>