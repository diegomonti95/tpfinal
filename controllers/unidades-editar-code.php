<?php
	include_once("lib/lib.php");
	
	$unidad		= $_POST["unidad"];
	$nombre 	= $_POST["nombre"];
	$sigla 	= $_POST["sigla"];
		
	include_once("unidades-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Unidad.php");
	$uni = new Unidad($unidad,
	$nombre,
	$sigla);
	
	if($uni->editar())
	{
		insertarAuditoria('UNIDADES','I','Insertado: ' . $nombre);
		$uni = null;
		echo "<script> location.href='../../views/unidad/unidades-listar.php';</script>";
	}
?>