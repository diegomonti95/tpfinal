<?php
	include_once("lib/lib.php");
	include_once("../models/Presupuesto.php");
	$url = "../../controllers/presupuestos-eliminar-code.php";
	$presupuesto = new Presupuesto();	
	$rows = $presupuesto->listar();
	
	$str = '<a class="btn btn-info btn-sm" href="presupuestos-agregar.php" data-toggle="tooltip" title="Nuevo">
		    <i class="fa fa-plus"></i></a>
			<table id="lista-crud" class="hover" style="width:100%">
			<thead>
				<th>Sucursal</th>
				<th>Cliente</th>
				<th>Nombre</th>
				<th>Fecha</th>
				<th>Monto</th>
				<th>Encargado</th>
				<th>Condición</th>
				<th>Estado</th>
				<th>Observación</th>
				<th>Acciones</th>
			</thead>
			<tbody>';
	
	if($rows){
		foreach($rows as $registro){		
			$str .= "<tr>
					<td>" . $registro["nombre_sucursal"] . "</td>
					<td>" . $registro["nombre_cliente"] . "</td>
					<td>" . $registro["nombre_presupuesto"] . "</td>
					<td>" . $registro["fecha_presupuesto"] . "</td>
					<td>" . $registro["monto_presupuesto"] . "</td>
					<td>" . $registro["encargado"] . "</td>
					<td>" . $registro["condicion"] . "</td>
					<td>" . $registro["estado"] . "</td>
					<td>" . $registro["observacion"] . "</td>
					<td>" . $registro["presupuesto"] . "</td>
					<td>
						<a class='btn btn-info btn-sm' href='presupuestos-editar.php?clave=" . 
						$registro["presupuesto"] . "'
						data-toggle='tooltip' title='Modificar'>
						<i class='fa fa-pencil'></i></a>
						<a class='btn btn-dark btn-sm' onclick='eliminar(" . '"' . 
						$registro["presupuesto"] . '"' . ',"' .$url . '"' . ");'
						data-toggle='tooltip' title='Eliminar'>
						<i class='fa fa-minus-circle'></i></a>					
					</td>
				</tr>";
		}
	}
	
	$str .= '</tbody>
			<tfoot>
				<th>Sucursal</th>
				<th>Cliente</th>
				<th>Nombre</th>
				<th>Fecha</th>
				<th>Monto</th>
				<th>Encargado</th>
				<th>Condición</th>
				<th>Estado</th>
				<th>Observación</th>
				<th>Acciones</th>
			</tfoot>';
			
	echo $str;
	
	dataTable("lista-crud");
?>