<?php
	include_once("lib/lib.php");
	include_once("../models/User.php");
	$user = new User();
	
	$rows = $user->listar();
	
	$str = '<table id="lista-crud" class="hover" style="width:100%">
			<thead>
				<th>Usuario</th>
				<th>Nombre</th>
				<th>Estado</th>
				<th>Perfil</th>
			</thead>
			<tbody>';
	
	foreach($rows as $registro){
		$e = ($registro["estado"] == "H" ? "Habilitado" : "Inhabilitado");
		$p = $registro["perfil"];
		if($p == "A") $p = "Administrador";
		elseif($p == "U") $p = "Usuario";
		elseif($p == "C") $p = "Consulta";
		
		$str .= '<tr>
					<td>' . $registro["usuario"] . '</td>
					<td>' . $registro["nombres"] . '</td>
					<td>' . $e . '</td>
					<td>' . $p . '</td>
				</tr>';
	}
	
	$str .= '</tbody>
			<tfoot>
			</tfoot>';
			
	echo $str;
	
	dataTable("lista-crud");
?>