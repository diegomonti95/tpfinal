<?php
	include_once("lib/lib.php");
	
	$pais = $_POST["pais"];
	$nombre = $_POST["nombre"];
		
	include_once("ciudades-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Ciudad.php");
	$ciudad = new Ciudad("",$pais,$nombre);
	
	if($ciudad->agregar())
	{
		insertarAuditoria('CIUDADES','I','Insertado: ' . $nombre);
		$ciudad = null;
		echo "<script> location.href='../../views/ciudad/ciudades-listar.php';</script>";
	}
?>