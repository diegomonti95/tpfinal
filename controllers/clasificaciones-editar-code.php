<?php
	include_once("lib/lib.php");
	
	$clasificacion 	= $_POST["clasificacion"];
	$nombre 	= $_POST["nombre"];
		
	include_once("clasificaciones-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Clasificacion.php");
	$cla = new Clasificacion($clasificacion,
	$nombre);
	
	if($cla->editar())
	{
		insertarAuditoria('CLASIFICACIONES','M','Modificado: ' . $nombre);
		$cla = null;
		echo "<script> location.href='../../views/clasificacion/clasificaciones-listar.php';</script>";
	}
?>