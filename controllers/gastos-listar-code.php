<?php
	include_once("lib/lib.php");
	include_once("../models/Gasto.php");
	$url = "../../controllers/gastos-eliminar-code.php";
	$gasto = new Gasto();	
	$rows = $gasto->listar();
	
	$str = '<a class="btn btn-info btn-sm" href="gastos-agregar.php" data-toggle="tooltip" title="Nuevo">
		    <i class="fa fa-plus"></i></a>
			<table id="lista-crud" class="hover" style="width:100%">
			<thead>
				<th>Fecha</th>
				<th>Nº Comprobante</th>
				<th>Proveedor</th>
				<th>Moneda</th>
				<th>Condición</th>
				<th>Total</th>
				<th>ID</th>
				<th>Acciones</th>	
			</thead>
			<tbody>';
	
	if($rows){
		foreach($rows as $registro){		
			$str .= "<tr>
					<td>" . $registro["fecha"] . "</td>
					<td>" . $registro["nro_comprobante"] . "</td>
					<td>" . $registro["razon_social"] . "</td>
					<td>" . $registro["nombre_moneda"] . "</td>
					<td>" . $registro["condicion"] . "</td>
					<td>" . $registro["total_compra"] . "</td>
					<td>" . $registro["gasto"] . "</td>
					<td>
						<a class='btn btn-info btn-sm' href='gastos-editar.php?clave=" . 
						$registro["gasto"] . "'
						data-toggle='tooltip' title='Modificar'>
						<i class='fa fa-pencil'></i></a>
						<a class='btn btn-dark btn-sm' onclick='eliminar(" . '"' . 
						$registro["gasto"] . '"' . ',"' .$url . '"' . ");'
						data-toggle='tooltip' title='Eliminar'>
						<i class='fa fa-minus-circle'></i></a>					
					</td>
				</tr>";
		}
	}
	
	$str .= '</tbody>
			<tfoot>
				<th>Fecha</th>
				<th>Nº Comprobante</th>
				<th>Proveedor</th>
				<th>Moneda</th>
				<th>Condición</th>
				<th>Total</th>
				<th>ID</th>		
			</tfoot>';	
			
	echo $str;
	
	dataTable("lista-crud");
?>