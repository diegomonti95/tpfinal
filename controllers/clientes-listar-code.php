<?php
	include_once("lib/lib.php");
	include_once("../models/Cliente.php");
	$url = "../../controllers/clientes-eliminar-code.php";
	$cliente = new Cliente();	
	$rows = $cliente->listar();
	
	$str = '<a class="btn btn-info btn-sm" href="clientes-agregar.php" data-toggle="tooltip" title="Nuevo">
		    <i class="fa fa-plus"></i></a>
			<table id="lista-crud" class="hover" style="width:100%">
			<thead>
				<th>Razón Social</th>
				<th>Documento</th>
				<th>Dirección</th>
				<th>Localidad</th>
				<th>Ciudad</th>
				<th>País</th>
				<th>Teléfono Principal</th>
				<th>Email</th>
				<th>Personería</th>
				<th>ID</th>
				<th>Acciones</th>
			</thead>
			<tbody>';
	
	if($rows){
		foreach($rows as $registro){		
			$str .= "<tr>
					<td>" . $registro["razon_social"] . "</td>
					<td>" . $registro["documento"] . "</td>
					<td>" . $registro["direccion"] . "</td>
					<td>" . $registro["nombre_localidad"] . "</td>
					<td>" . $registro["nombre_ciudad"] . "</td>
					<td>" . $registro["nombre_pais"] . "</td>
					<td>" . $registro["telefono_principal"] . "</td>
					<td>" . $registro["email"] . "</td>
					<td>" . $registro["personeria"] . "</td>
					<td>" . $registro["cliente"] . "</td>
					<td>
						<a class='btn btn-info btn-sm' href='clientes-editar.php?clave=" . 
						$registro["cliente"] . "'
						data-toggle='tooltip' title='Modificar'>
						<i class='fa fa-pencil'></i></a>
						<a class='btn btn-dark btn-sm' onclick='eliminar(" . '"' . 
						$registro["cliente"] . '"' . ',"' .$url . '"' . ");'
						data-toggle='tooltip' title='Eliminar'>
						<i class='fa fa-minus-circle'></i></a>					
					</td>
				</tr>";
		}
	}
	
	$str .= '</tbody>
			<tfoot>
				<th>Razón Social</th>
				<th>Documento</th>
				<th>Dirección</th>
				<th>Localidad</th>
				<th>Ciudad</th>
				<th>País</th>
				<th>Teléfono Principal</th>
				<th>Email</th>
				<th>Personería</th>
				<th>ID</th>
				<th>Acciones</th>
			</tfoot>';
			
	echo $str;
	
	dataTable("lista-crud");
?>