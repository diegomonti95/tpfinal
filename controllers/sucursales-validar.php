<?php
	$resultadoControl = true;
		
	if($nombre === ""){
		alertar("Error","Ingrese un nombre para la Sucursal.","nombre");
		$resultadoControl = false;
		return;
	}
	
	if(validar_dato($ruc,"ruc") == false){
		alertar("Error","Ingrese un RUC en el formato establecido.","ruc");
		$resultadoControl = false;
		return;
	}
	
	if($direccion === ""){
		alertar("Error","Ingrese una dirección para la Sucursal.","direccion");
		$resultadoControl = false;
		return;
	}
	
	if($ciudad === ""){
		alertar("Error","Ingrese una ciudad para la Sucursal.","ciudad");
		$resultadoControl = false;
		return;
	}
	
	if($localidad === ""){
		alertar("Error","Ingrese una localidad para la Sucursal.","localidad");
		$resultadoControl = false;
		return;
	}
	
	if(validar_dato($telefono,"telefono") == false){
		alertar("Error","Ingrese un telèfono en el formato establecido.","telefono");
		$resultadoControl = false;
		return;
	}
	
	if(validar_dato($email,"email") == false){
		alertar("Error","Ingrese un mail en el formato establecido.","email");
		$resultadoControl = false;
		return;
	}
?>