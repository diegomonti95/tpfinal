<?php
	include_once("lib/lib.php");
	
	$codigo 		= $_POST["codigo"];
	$nombre 		= $_POST["nombre"];
	$descripcion 	= $_POST["descripcion"];
	$clasificacion 	= $_POST["clasificacion"];
	$unidad 		= $_POST["unidad"];
	$impuesto 		= $_POST["impuesto"];
	$habilitado 	= $_POST["habilitado"];
			
	include_once("productos-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Producto.php");
	$s = new Producto($producto,$codigo,$nombre,$descripcion,$clasificacion,$unidad,$impuesto,$habilitado);
	
	if($s->agregar() == true){
		insertarAuditoria('PRODUCTOS','I','Insertado: ' . $nombre);
		$s=null;
		echo "<script> location.href='../../views/producto/productos-listar.php'; </script>";
	}
	
?>