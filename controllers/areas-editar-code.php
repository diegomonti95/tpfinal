<?php
	include_once("lib/lib.php");
	
	$area 		= $_POST["area"];
	$sucursal 	= $_POST["sucursal"];
	$nombre 	= $_POST["nombre"];
	
	include_once("areas-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Area.php");
	$area = new Area($area,
	$sucursal,
	$nombre);
	
	if($area->editar())
	{
		insertarAuditoria('AREAS','M','Modificado: ' . $nombre);
		$area = null;
		echo "<script> location.href='../../views/area/areas-listar.php';</script>";
	}
?>