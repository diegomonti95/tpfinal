<?php
	include_once("lib/lib.php");
	include_once("../models/Localidad.php");
	$url = "../../controllers/localidades-eliminar-code.php";
	$localidad = new Localidad();	
	$rows = $localidad->listar();
				
	$str = '<a class="btn btn-info btn-sm" href="localidades-agregar.php" data-toggle="tooltip" title="Nuevo">
		    <i class="fa fa-plus"></i></a>
			<table id="lista-crud" class="hover" style="width:100%">
			<thead>
				<th>Nombre</th>
				<th>Ciudad</th>
				<th>ID</th>
				<th>Acciones</th>
			</thead>
			<tbody>';
	
	if($rows){
		foreach($rows as $registro){
			$str .= "<tr>
					<td>" . $registro["nombre_localidad"] . "</td>
					<td>" . $registro["nombre_ciudad"] . "</td>
					<td>" . $registro["localidad"] . "</td>
					<td>
						<a class='btn btn-info btn-sm' href='localidades-editar.php?clave=" . 
						$registro["localidad"] . "'
						data-toggle='tooltip' title='Modificar'>
						<i class='fa fa-pencil'></i></a>
						<a class='btn btn-dark btn-sm' onclick='eliminar(" . '"' . 
						$registro["localidad"] . '"' . ',"' .$url . '"' . ");'
						data-toggle='tooltip' title='Eliminar'>
						<i class='fa fa-minus-circle'></i></a>					
					</td>
				</tr>";
		}
	}
	
	$str .= '</tbody>
			<tfoot>
				<th>Nombre</th>
				<th>Ciudad</th>
				<th>ID</th>
				<th>Acciones</th>
			</tfoot>';
			
	echo $str;
	
	dataTable("lista-crud");
?>