<?php
	include_once("lib/lib.php");
	
	$nombre = $_POST["nombre"];
	
	include_once("paises-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Pais.php");
	$p = new Pais("",$nombre);
	
	if($p->agregar())
	{
		insertarAuditoria('PAISES','I','Insertado: ' . $nombre);
		$p = null;
		echo "<script> location.href='../../views/pais/paises-listar.php';</script>";
	}
?>