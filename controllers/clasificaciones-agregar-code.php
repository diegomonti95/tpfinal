<?php
	include_once("lib/lib.php");
	
	$nombre = $_POST["nombre"];
		
	include_once("clasificaciones-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Clasificacion.php");
	$cla = new Clasificacion("",$nombre);
	
	if($cla->agregar())
	{
		insertarAuditoria('CLASIFICACIONES','I','Insertado: ' . $nombre);
		$cla = null;
		echo "<script> location.href='../../views/clasificacion/clasificaciones-listar.php';</script>";
	}
?>