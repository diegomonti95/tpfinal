<?php
	include_once("lib/lib.php");
		
	$user 		= $_POST["user"];
	$password 	= $_POST["password"];
	
	if($user == ""){
		alertar("Aviso!","El campo Usuario requiere un valor.","user");
		return;
	}
	
	if($password == ""){
		alertar("Aviso!","El campo Clave requiere un valor.","password");
		return;
	}
	
	$password = sha1($password);
		
	include_once("../models/DataBase.php");
	$conn = new DataBase();
	
	$sentencia = "select nombres, perfil, estado 
				  from usuarios
				  where	usuario = '$user'
				  and clave = '$password'";
	$data = $conn->ejecutarConsulta($sentencia);
	
	if($data){
		if($data[0]["estado"] == "H"){
			session_start();
			$_SESSION["usuario"] = $user;
			$_SESSION["perfil"] = $data[0]["perfil"];
			
			echo "<script> location.href='../menu/menu.php';</script>";
		}
		else{
			alertar("Usuario Incorrecto","El usuario se encuentra Inactivo.","user");
		}
	}
	else
		alertar("Usuario Incorrecto","Verifique el usuario y/o la Clave ingresadas.","user");
	
	$conn = null;
?>