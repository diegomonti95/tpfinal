<?php
	include_once("lib/lib.php");
	
	$ciudad = $_POST["ciudad"];
	$nombre = $_POST["nombre"];
	
	include_once("localidades-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Localidad.php");
	$l = new Localidad("",$ciudad,$nombre);
	
	if($l->agregar())
	{
		insertarAuditoria('LOCALIDADES','I','Insertado: ' . $nombre);
		$l = null;
		echo "<script> location.href='../../views/localidad/localidades-listar.php';</script>";
	}
?>