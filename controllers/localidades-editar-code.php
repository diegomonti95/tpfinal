<?php
	include_once("lib/lib.php");
	
	$localidad		= $_POST["localidad"];
	$ciudad 	= $_POST["ciudad"];
	$nombre 	= $_POST["nombre"];
		
	include_once("localidades-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Localidad.php");
	$l = new Localidad($localidad,
	$ciudad,
	$nombre);
	
	if($l->editar())
	{
		insertarAuditoria('LOCALIDADES','M','Modificado: ' . $nombre);
		$l = null;
		echo "<script> location.href='../../views/localidad/localidades-listar.php';</script>";
	}
?>