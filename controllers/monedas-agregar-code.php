<?php
	include_once("lib/lib.php");
	
	$nombre = $_POST["nombre"];
	$sigla = $_POST["sigla"];
	
	include_once("monedas-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Moneda.php");
	$mon = new Moneda("",$nombre,$sigla);
	
	if($mon->agregar())
	{
		insertarAuditoria('MONEDAS','I','Insertado: ' . $nombre);
		$mon = null;
		echo "<script> location.href='../../views/moneda/monedas-listar.php';</script>";
	}
?>