<?php
	include_once("lib/lib.php");
	
	$sucursal = $_POST["sucursal"];
	$nombre = $_POST["nombre"];
		
	include_once("areas-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Area.php");
	$area = new Area("",$sucursal,$nombre);
	
	if($area->agregar())
	{
		insertarAuditoria('AREAS','I','Insertado: ' . $nombre);
		$area = null;
		echo "<script> location.href='../../views/area/areas-listar.php';</script>";
	}
?>