<?php
	include_once("lib/lib.php");
	
	$nombre = $_POST["nombre"];
	$ruc = $_POST["ruc"];
	$direccion = $_POST["direccion"];
	$ciudad = $_POST["ciudad"];
	$localidad = $_POST["localidad"];
	$telefono = $_POST["telefono"];
	$email = $_POST["email"];
	$pagina_web = $_POST["pagina_web"];
		
	include_once("sucursales-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Sucursal.php");
	$s = new Sucursal("",$nombre,$ruc,$direccion,$ciudad,$localidad,$telefono,$email,$pagina_web);
	
	if($s->agregar() == true){
		insertarAuditoria('SUCURSALES','I','Insertado: ' . $nombre);
		$s=null;
		echo "<script> location.href='../../views/sucursal/sucursales-listar.php'; </script>";
	}
	
?>