<?php
	include_once("lib/lib.php");
	include_once("../models/Moneda.php");
	$url = "../../controllers/monedas-eliminar-code.php";
	$moneda = new Moneda();	
	$rows = $moneda->listar();
				
	$str = '<a class="btn btn-info btn-sm" href="monedas-agregar.php" data-toggle="tooltip" title="Nuevo">
		    <i class="fa fa-plus"></i></a>
			<table id="lista-crud" class="hover" style="width:100%">
			<thead>
				<th>Nombre</th>
				<th>Sigla</th>
				<th>Moneda</th>
				<th>Acciones</th>
			</thead>
			<tbody>';
	
	if($rows){
		foreach($rows as $registro){
			$str .= "<tr>
					<td>" . $registro["nombre"] . "</td>
					<td>" . $registro["sigla"] . "</td>
					<td>" . $registro["moneda"] . "</td>
					<td>
						<a class='btn btn-info btn-sm' href='monedas-editar.php?clave=" . 
						$registro["moneda"] . "'
						data-toggle='tooltip' title='Modificar'>
						<i class='fa fa-pencil'></i></a>
						<a class='btn btn-dark btn-sm' onclick='eliminar(" . '"' . 
						$registro["moneda"] . '"' . ',"' .$url . '"' . ");'
						data-toggle='tooltip' title='Eliminar'>
						<i class='fa fa-minus-circle'></i></a>					
					</td>
				</tr>";
		}
	}
	
	$str .= '</tbody>
			<tfoot>
				<th>Nombre</th>
				<th>Sigla</th>
				<th>Moneda</th>
				<th>Acciones</th>
			</tfoot>';
			
	echo $str;
	
	dataTable("lista-crud");
?>