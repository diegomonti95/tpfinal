<?php
	include_once("lib/lib.php");
	
	$sucursal 			= $_POST["sucursal"];
	$cliente 			= $_POST["cliente"];
	$nombre 			= $_POST["nombre"];
	$fecha_presupuesto 	= $_POST["fecha_presupuesto"];
	$monto_presupuesto 	= $_POST["monto_presupuesto"];
	$encargado 			= $_POST["encargado"];
	$condicion 			= $_POST["condicion"];
	$estado 			= $_POST["estado"];
	$observacion	 	= $_POST["observacion"];
	
	include_once("presupuestos-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Presupuesto.php");
	$pr = new Presupuesto("",$sucursal,$cliente,$nombre,$fecha_presupuesto,$monto_presupuesto,$encargado,$condicion,$estado,$observacion);
	
	if($pr->agregar())
	{
		insertarAuditoria('PRESUPUESTOS','I','Insertado: ' . $nombre);
		$pr = null;
		echo "<script> location.href='../../views/presupuesto/presupuestos-listar.php';</script>";
	}
?>