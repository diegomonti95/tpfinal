<?php
	$clave = $_POST["clave"];
	
	include_once("../models/Clasificacion.php");
	$cla = new Clasificacion();
	
	if($cla->eliminar($clave)){
		insertarAuditoria('CLASIFICACIONES','E','Eliminado: ' . $clave);
		$cla = null;
		echo "<script> listarClasificaciones(); </script>";
	}
?>