<?php
	include_once("lib/lib.php");
	
	$cliente 			= $_POST["cliente"];
	$razon_social 		= $_POST["razon_social"];
	$documento 			= $_POST["documento"];
	$direccion 			= $_POST["direccion"];
	$pais			    = $_POST["pais"];
	$ciudad			    = $_POST["ciudad"];
	$localidad		    = $_POST["localidad"];
	$telefono_principal = $_POST["telefono_principal"];
	$telefono_secundario= $_POST["telefono_secundario"];
	$email 				= $_POST["email"];
	$personeria		    = $_POST["personeria"];
	
	include_once("clientes-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Cliente.php");
	$cli = new Cliente($cliente,
	$razon_social,
	$documento,
	$direccion,
	$pais,
	$ciudad,
	$localidad,
	$telefono_principal,
	$telefono_secundario,
	$email,
	$personeria);
	
	if($cli->editar() == true)
	{
		insertarAuditoria('CLIENTES','M','Modificado: ' . $razon_social);
		$cli = null;
		echo "<script> location.href='../../views/cliente/clientes-listar.php';</script>";
	}
?>