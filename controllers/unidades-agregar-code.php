<?php
	include_once("lib/lib.php");
	
	$nombre = $_POST["nombre"];
	$sigla = $_POST["sigla"];
	
	include_once("unidades-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Unidad.php");
	$uni = new Unidad("",$nombre,$sigla);
	
	if($uni->agregar())
	{
		insertarAuditoria('UNIDADES','I','Insertado: ' . $nombre);
		$uni = null;
		echo "<script> location.href='../../views/unidad/unidades-listar.php';</script>";
	}
?>