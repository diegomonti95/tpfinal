<?php
	include_once("lib/lib.php");
	include_once("../models/Sucursal.php");
	$url = "../../controllers/sucursales-eliminar-code.php";
	$sucursal = new Sucursal();	
	$rows = $sucursal->listar();
	
	$str = '<a class="btn btn-info btn-sm" href="sucursales-agregar.php" data-toggle="tooltip" title="Nuevo">
		    <i class="fa fa-plus"></i></a>
			<table id="lista-crud" class="hover" style="width:100%">
			<thead>
				<th>Nombre</th>
				<th>Dirección</th>
				<th>Teléfono</th>
				<th>Email</th>
				<th>ID</th>
				<th>Acciones</th>
			</thead>
			<tbody>';
	
	if($rows){
		foreach($rows as $registro){		
			$str .= "<tr>
					<td>" . $registro["nombre"] . "</td>
					<td>" . $registro["direccion"] . "</td>
					<td>" . $registro["telefono"] . "</td>
					<td>" . $registro["email"] . "</td>
					<td>" . $registro["sucursal"] . "</td>
					<td>
						<a class='btn btn-info btn-sm' href='sucursales-editar.php?clave=" . 
						$registro["sucursal"] . "'
						data-toggle='tooltip' title='Modificar'>
						<i class='fa fa-pencil'></i></a>
						<a class='btn btn-dark btn-sm' onclick='eliminar(" . '"' . 
						$registro["sucursal"] . '"' . ',"' .$url . '"' . ");'
						data-toggle='tooltip' title='Eliminar'>
						<i class='fa fa-minus-circle'></i></a>					
					</td>
				</tr>";
		}
	}
	
	$str .= '</tbody>
			<tfoot>
				<th>Nombre</th>
				<th>Dirección</th>
				<th>Teléfono</th>
				<th>Email</th>
				<th>ID</th>
				<th>Acciones</th>
			</tfoot>';
			
	echo $str;
	
	dataTable("lista-crud");
?>