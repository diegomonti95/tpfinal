<?php
	$clave = $_POST["clave"];
	
	include_once("../models/Localidad.php");
	$l = new Localidad();
	
	if($l->eliminar($clave)){
		insertarAuditoria('LOCALIDADES','E','Eliminado: ' . $clave);
		$l = null;
		echo "<script> listarLocalidades(); </script>";
	}
?>