<?php
	include_once("lib/lib.php");
	$clave = $_POST["clave"];
		
	include_once("../models/Presupuesto.php");
	$pr = new Presupuesto();
	
	if($pr->eliminar($clave)){
		insertarAuditoria('PRESUPUESTOS','E','Eliminado: ' . $clave);
		$pr = null;
		echo "<script> listarPresupuestos(); </script>";
	}
?>