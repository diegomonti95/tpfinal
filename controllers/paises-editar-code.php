<?php
	include_once("lib/lib.php");
	
	$pais		= $_POST["pais"];
	$nombre 	= $_POST["nombre"];
		
	include_once("paises-validar.php");
	if(!$resultadoControl) return;
	
	include_once("../models/Pais.php");
	$p = new Pais($pais,
	$nombre);
	
	if($p->editar())
	{
		insertarAuditoria('PAISES','M','Modificado: ' . $nombre);
		$p = null;
		echo "<script> location.href='../../views/pais/paises-listar.php';</script>";
	}
?>