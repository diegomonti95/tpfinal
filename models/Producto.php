<?php
	class Producto{
		protected $producto;
		protected $codigo;
		protected $nombre;
		protected $descripcion;
		protected $clasificacion;
		protected $unidad;
		protected $impuesto;
		protected $habilitado;
		protected $db;
		
		// Constructor de la Clase
		function __construct($producto="",$codigo="",$nombre="",$descripcion="",$clasificacion="",$unidad="",
		$impuesto="",$habilitado="",$path=""){
			$this->producto = $producto;
			$this->codigo = $codigo;
			$this->nombre = $nombre;
			$this->descripcion = $descripcion;
			$this->clasificacion = $clasificacion;
			$this->unidad = $unidad;
			$this->impuesto = $impuesto;
			$this->habilitado = $habilitado;
			
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($clave){
			$sentencia = "select * from productos where producto = '$clave'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->producto = $clave;
				$this->codigo = $registro[0]["codigo"];
				$this->nombre = $registro[0]["nombre"];
				$this->descripcion = $registro[0]["descripcion"];
				$this->clasificacion = $registro[0]["clasificacion"];
				$this->unidad = $registro[0]["unidad"];
				$this->impuesto = $registro[0]["impuesto"];
				$this->habilitado = $registro[0]["habilitado"];
			}
		}
		
		function listar(){
			$sentencia = "select 	p.codigo,
									p.nombre as nombre_producto,
									p.descripcion,
									c.nombre as nombre_clasificacion,
									u.nombre as nombre_unidad,
									p.producto,
									(case 
										when p.habilitado = 'S' then 'SI'
										when p.habilitado = 'N' then 'NO'
									end) as habilitado
							from 	productos p, clasificaciones c, unidades u
							where	p.clasificacion = c.clasificacion
							and		p.unidad = u.unidad
							order by p.nombre ";
			return $this->db->ejecutarConsulta($sentencia);
		}
		
		function agregar(){
			$sentencia = "insert into productos(codigo,nombre,descripcion,clasificacion,unidad,impuesto,habilitado)
			values(
			'$this->codigo',
			'$this->nombre',
			'$this->descripcion',
			'$this->clasificacion',
			'$this->unidad',
			'$this->impuesto',
			'$this->habilitado')";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update productos
			set nombre = '$this->nombre',
			codigo = '$this->codigo',
			descripcion = '$this->descripcion',
			clasificacion = '$this->clasificacion',
			unidad = '$this->unidad',
			impuesto = '$this->impuesto',
			habilitado = '$this->habilitado'
			where producto = '$this->producto'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function eliminar($clave){
			$sentencia = "delete from productos where producto = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
		function getProducto(){
			return $this->producto;
		}
		
		function getCodigo(){
			return $this->codigo;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
		function getDescripcion(){
			return $this->descripcion;
		}
		
		function getClasificacion(){
			return $this->clasificacion;
		}
		
		function getUnidad(){
			return $this->unidad;
		}
		
		function getImpuesto(){
			return $this->impuesto;
		}
		
		function getHabilitado(){
			return $this->habilitado;
		}
		
	
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>