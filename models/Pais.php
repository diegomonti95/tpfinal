<?php
	class Pais{
		protected $pais;
		protected $nombre;
		protected $db;
		
		// Constructor de la Clase
		function __construct($pais="",$nombre="",$path="")
		{
			$this->pais = $pais;
			$this->nombre = $nombre;
						
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($pais){
			$sentencia = "select * from paises where pais = '$pais'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->pais = $registro[0]["pais"];
				$this->nombre = $registro[0]["nombre"];
			}
		}
		
		function listar(){
			$sentencia = "select * from paises order by nombre";
			return $this->db->ejecutarConsulta($sentencia);
		}
		
		function agregar(){
			$sentencia = "insert into paises(nombre)
			values('$this->nombre')";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update paises set
			nombre = '$this->nombre'
			where pais = '$this->pais'";  
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function eliminar($clave){
			$sentencia = "delete from paises where pais = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
		function getPais(){
			return $this->pais;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
				
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>