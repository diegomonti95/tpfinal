<?php
	class Moneda{
		protected $moneda;
		protected $nombre;
		protected $sigla;
		protected $db;
		
		// Constructor de la Clase
		function __construct($moneda="",$nombre="",$sigla="",$path="")
		{
			$this->moneda = $moneda;
			$this->nombre = $nombre;
			$this->sigla = $sigla;
						
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($sigla){
			$sentencia = "select * from monedas where moneda = '$sigla'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->moneda = $registro[0]["moneda"];
				$this->nombre = $registro[0]["nombre"];
				$this->sigla = $registro[0]["sigla"];
			}
		}
		
		function listar(){
			$sentencia = "select * from monedas order by nombre";
			return $this->db->ejecutarConsulta($sentencia);
		}
		
		function agregar(){
			$sentencia = "insert into monedas(nombre,sigla)
			values('$this->nombre','$this->sigla')";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update monedas set
			nombre = '$this->nombre',
			sigla = '$this->sigla'
			where moneda = '$this->moneda'";  
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function eliminar($clave){
			$sentencia = "delete from monedas where moneda = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
		function getMoneda(){
			return $this->moneda;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
		function getSigla(){
			return $this->sigla;
		}
		
				
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>