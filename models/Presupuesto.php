<?php
	class Presupuesto{
		protected $presupuesto;
		protected $sucursal;
		protected $cliente;
		protected $nombre;
		protected $fecha_presupuesto;
		protected $monto_presupuesto;
		protected $encargado;
		protected $condicion;
		protected $estado;
		protected $observacion;
		protected $db;
		
		// Constructor de la Clase
		function __construct($presupuesto="",$sucursal="",$cliente="",$nombre="",$fecha_presupuesto="",$monto_presupuesto="",
		$encargado="",$condicion="",$estado="",$observacion="",$path=""){
			$this->presupuesto = $presupuesto;
			$this->sucursal = $sucursal;
			$this->cliente = $cliente;
			$this->nombre = $nombre;
			$this->fecha_presupuesto = $fecha_presupuesto;
			$this->monto_presupuesto = $monto_presupuesto;
			$this->encargado = $encargado ;
			$this->condicion = $condicion;
			$this->estado = $estado;
			$this->observacion = $observacion;
			
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($clave){
			$sentencia = "select * from presupuestos_ventas where presupuesto = '$clave'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->presupuesto = $clave;
				$this->sucursal = $registro[0]["sucursal"];
				$this->cliente = $registro[0]["cliente"];
				$this->nombre = $registro[0]["nombre"];
				$this->fecha_presupuesto = $registro[0]["fecha_presupuesto"];
				$this->monto_presupuesto = $registro[0]["monto_presupuesto"];
				$this->encargado = $registro[0]["encargado"];
				$this->condicion = $registro[0]["condicion"];
				$this->estado = $registro[0]["estado"];
				$this->observacion = $registro[0]["observacion"];
			}
		}
		
		function listar(){
		$sentencia = "select 	s.nombre as nombre_sucursal,
								cli.razon_social as nombre_cliente,
								pr.nombre as nombre_presupuesto,
								to_char(pr.fecha_presupuesto,'dd-mm-yyyy HH:MM') as fecha_presupuesto,
								to_char(pr.monto_presupuesto,'999G999G999.00') as monto_presupuesto,
								pr.encargado,
								(case 
									when pr.condicion = 'C' then 'CONTADO'
									when pr.condicion = 'R' then 'CREDITO'
								end) as condicion,
								(case 
									when pr.estado = 'A' then 'APROBADO'
									when pr.estado = 'P' then 'PENDIENTE'
									when pr.estado = 'R' then 'RECHAZADO'
								end) as estado,
								pr.observacion,
								pr.presupuesto
						from 	presupuestos_ventas pr, sucursales s, clientes cli
						where	pr.cliente = cli.cliente
						and		pr.sucursal = s.sucursal
						order by pr.fecha_presupuesto, nombre_cliente ";
		return $this->db->ejecutarConsulta($sentencia);
		}
		function agregar(){
			$sentencia = "insert into presupuestos_ventas(sucursal,cliente,nombre,fecha_presupuesto,monto_presupuesto,encargado,condicion,estado,observacion)
			values('$this->sucursal',
			'$this->cliente',
			'$this->nombre',
			'$this->fecha_presupuesto',
			'$this->monto_presupuesto',
			'$this->encargado',
			'$this->condicion',
			'$this->estado';
			'$this->observacion')";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update presupuestos_ventas
			set sucursal = '$this->sucursal',
			cliente = '$this->cliente',
			nombre = '$this->nombre',
			fecha_presupuesto = '$this->fecha_presupuesto',
			monto_presupuesto = '$this->monto_presupuesto',
			encargado = '$this->encargado',
			condicion = '$this->condicion',
			estado = '$this->estado',
			observacion = '$this->observacion'
			where presupuesto = '$this->presupuesto'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function eliminar($clave){
			$sentencia = "delete from presupuestos_ventas where presupuestos_ventas = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
		function getPresupuesto(){
			return $this->presupuesto;
		}
		
		function getSucursal(){
			return $this->sucursal;
		}
		
		function getCliente(){
			return $this->cliente;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
		function getFechaPresupuesto(){
			return $this->fecha_presupuesto;
		}
		
		function getMontoPresupuesto(){
			return $this->monto_presupuesto;
		}
		
		function getEncargado(){
			return $this->encargado;
		}
		
		function getCondicion(){
			return $this->condicion;
		}
		
		function getEstado(){
			return $this->estado;
		}
		
		function getObservacion(){
			return $this->observacion;
		}
	
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>