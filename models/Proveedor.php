<?php
	class Proveedor{
		protected $proveedor;
		protected $razon_social;
		protected $documento;
		protected $direccion;
		protected $pais;
		protected $ciudad;
		protected $localidad;
		protected $telefono;
		protected $email;
		protected $personeria;
		protected $db;
		
		// Constructor de la Clase
		function __construct($proveedor="",$razon_social="",$documento="",$direccion="",$pais="",$ciudad="",$localidad="",
		$telefono="",$email="",$personeria="",$path=""){
			$this->proveedor = $proveedor;
			$this->razon_social = $razon_social;
			$this->documento = $documento;
			$this->direccion = $direccion;
			$this->pais = $pais;
			$this->ciudad = $ciudad;
			$this->localidad = $localidad;
			$this->telefono = $telefono;
			$this->email = $email;
			$this->personeria = $personeria;
			
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($clave){
			$sentencia = "select * from proveedores where proveedor = '$clave'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->proveedor = $clave;
				$this->razon_social = $registro[0]["razon_social"];
				$this->documento = $registro[0]["documento"];
				$this->direccion = $registro[0]["direccion"];
				$this->pais = $registro[0]["pais"];
				$this->ciudad = $registro[0]["ciudad"];
				$this->localidad = $registro[0]["localidad"];
				$this->telefono = $registro[0]["telefono"];
				$this->email = $registro[0]["email"];
				$this->personeria = $registro[0]["personeria"];
			}
		}
		
		function listar(){
			$sentencia = "select	p.razon_social,
								p.documento,
								p.direccion,
								pa.nombre as nombre_pais,
								c.nombre as nombre_ciudad,
								l.nombre as nombre_localidad,
								p.telefono,
								p.email,
								(case
									when p.personeria = 'F' then 'Física'
									when p.personeria = 'J' then 'Jurídica'
									else 'Desconocida'
								end) as personeria,
								p.proveedor
						from 	proveedores as p, paises as pa, ciudades as c, localidades as l 
						where 	p.pais=pa.pais
						and		p.ciudad=c.ciudad
						and		p.localidad=l.localidad
						order by 1";
			return $this->db->ejecutarConsulta($sentencia);
		}
		
		function agregar(){
			$sentencia = "insert into proveedores(razon_social,documento,direccion,pais,ciudad,localidad,telefono,email,personeria)
			values('$this->razon_social',
			'$this->documento',
			'$this->direccion',
			'$this->pais',
			'$this->ciudad',
			'$this->localidad',
			'$this->telefono',
			'$this->email',
			'$this->personeria')";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update proveedores
			set razon_social = '$this->razon_social',
			documento = '$this->documento',
			direccion = '$this->direccion',
			pais = '$this->pais',
			ciudad = '$this->ciudad',
			localidad = '$this->localidad',
			telefono = '$this->telefono',
			email = '$this->email',
			personeria = '$this->personeria'
			where proveedor = '$this->proveedor'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function eliminar($clave){
			$sentencia = "delete from proveedores where proveedor = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
		function getProveedor(){
			return $this->proveedor;
		}
		
		function getRazonSocial(){
			return $this->razon_social;
		}
		
		function getDocumento(){
			return $this->documento;
		}
		
		function getDireccion(){
			return $this->direccion;
		}
		
		function getPais(){
			return $this->pais;
		}
		
		function getCiudad(){
			return $this->ciudad;
		}
		
		function getLocalidad(){
			return $this->localidad;
		}
		
		function getTelefono(){
			return $this->telefono;
		}
		
		function getEmail(){
			return $this->email;
		}
		
		function getPersoneria(){
			return $this->personeria;
		}
		
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>