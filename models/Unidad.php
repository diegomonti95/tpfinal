<?php
	class Unidad{
		protected $unidad;
		protected $nombre;
		protected $sigla;
		protected $db;
		
		// Constructor de la Clase
		function __construct($unidad="",$nombre="",$sigla="",$path="")
		{
			$this->unidad = $unidad;
			$this->nombre = $nombre;
			$this->sigla = $sigla;
						
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($sigla){
			$sentencia = "select * from unidades where unidad = '$sigla'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->unidad = $registro[0]["unidad"];
				$this->nombre = $registro[0]["nombre"];
				$this->sigla = $registro[0]["sigla"];
			}
		}
		
		function listar(){
			$sentencia = "select * from unidades order by nombre";
			return $this->db->ejecutarConsulta($sentencia);
		}
		
		function agregar(){
			$sentencia = "insert into unidades(nombre,sigla)
			values('$this->nombre','$this->sigla')";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update unidades set
			nombre = '$this->nombre',
			sigla = '$this->sigla'
			where unidad = '$this->unidad'";  
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function eliminar($clave){
			$sentencia = "delete from unidades where unidad = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
		function getUnidad(){
			return $this->unidad;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
		function getSigla(){
			return $this->sigla;
		}
		
				
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>