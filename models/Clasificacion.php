<?php
	class Clasificacion{
		protected $clasificacion;
		protected $nombre;
		protected $db;
		
		// Constructor de la Clase
		function __construct($clasificacion="",$nombre="",$path="")
		{
			$this->clasificacion = $clasificacion;
			$this->nombre = $nombre;
						
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($clave){
			$sentencia = "select * from clasificaciones where clasificacion = '$clave'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->clasificacion = $registro[0]["clasificacion"];
				$this->nombre = $registro[0]["nombre"];
			}
		}
		
		function listar(){
			$sentencia = "select * from clasificaciones order by nombre";
			return $this->db->ejecutarConsulta($sentencia);
		}
		
		function agregar(){
			$sentencia = "insert into clasificaciones(nombre) values('$this->nombre')";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update clasificaciones set
			nombre = '$this->nombre'
			where clasificacion = '$this->clasificacion'";  
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function eliminar($clave){
			$sentencia = "delete from clasificaciones where clasificacion = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
		function getClasificacion(){
			return $this->clasificacion;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
				
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>