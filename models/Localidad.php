<?php
	class Localidad{
		protected $localidad;
		protected $ciudad;
		protected $nombre;
		protected $db;
		
		// Constructor de la Clase
		function __construct($localidad="",$ciudad="",$nombre="",$path="")
		{
			$this->localidad = $localidad;
			$this->ciudad = $ciudad;
			$this->nombre = $nombre;
						
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($nombre){
			$sentencia = "select * from localidades where localidad = '$nombre'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->localidad = $registro[0]["localidad"];
				$this->ciudad = $registro[0]["ciudad"];
				$this->nombre = $registro[0]["nombre"];
			}
		}
		
		function listar(){
			$sentencia = "select 	l.nombre as nombre_localidad, 
									c.nombre as nombre_ciudad,
									l.localidad
							from	localidades as l, ciudades as c
							where	l.ciudad=c.ciudad
							order by 1,2";
			return $this->db->ejecutarConsulta($sentencia);
		}
		
		function agregar(){
			$sentencia = "insert into localidades(ciudad,nombre)
			values('$this->ciudad','$this->nombre')";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update localidades set
			ciudad = '$this->ciudad' ,
			nombre = '$this->nombre'
			where localidad = '$this->localidad'";  
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function eliminar($clave){
			$sentencia = "delete from localidades where localidad = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
		function getLocalidad(){
			return $this->localidad;
		}
		
		function getCiudad(){
			return $this->ciudad;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
				
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>