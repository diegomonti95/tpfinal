<?php
	class Ciudad{
		protected $ciudad;
		protected $pais;
		protected $nombre;
		protected $db;
		
		// Constructor de la Clase
		function __construct($ciudad="",$pais="",$nombre="",$path="")
		{
			$this->ciudad = $ciudad;
			$this->pais = $pais;
			$this->nombre = $nombre;
						
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($clave){
			$sentencia = "select * from ciudades where ciudad = '$clave'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->ciudad = $registro[0][0];
				$this->pais = $registro[0][1];
				$this->nombre = $registro[0][2];
			}
		}
		
		function listar(){
			$sentencia = "select ciudades.ciudad,
						paises.nombre as nombre_pais,
						ciudades.nombre as nombre_ciudad
						from ciudades, paises
						where ciudades.pais = paises.pais";
			return $this->db->ejecutarConsulta($sentencia);
		}
		
		function agregar(){
			$sentencia = "insert into ciudades(pais,nombre)
			values('$this->pais','$this->nombre')";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update ciudades set
			pais = '$this->pais',
			nombre = '$this->nombre'
			where ciudad = '$this->ciudad'";  
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function eliminar($clave){
			$sentencia = "delete from ciudades where ciudad = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
		function getCiudad(){
			return $this->ciudad;
		}
		
		function getPais(){
			return $this->pais;
		}
		
		function getNombre(){
			return $this->nombre;
		}		
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>