<?php
	if(file_exists("../controllers/lib/lib.php"))
		include_once("../controllers/lib/lib.php");
	
	class DataBase{
		private $conn;
		
		function __construct($path=""){
			if($path === "")
				$ini = "../models/params.ini";
			else
				$ini = $path;
				
			if(!file_exists($ini)){
				alertar("Error","No se encuentra " . $ini);
				return;
			}
		
			$valores = parse_ini_file($ini);
			$str = $valores["driver"] . ":host=" . $valores["host"] . ";port=" . $valores["port"] . ";dbname=" . $valores["db"];
		
			try{
				$this->conn = new PDO($str,$valores["user"],$valores["password"]);
				$this->conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			}
			catch(PDOException $e){
				alertar("Error",$e->getMessage());
				return;
			}
		}
		
		function ejecutarConsulta($sentencia,$es_consulta=true)
		{
			try
			{
				$s = $this->conn->prepare($sentencia);
				$s->setFetchMode(PDO::FETCH_BOTH);
				$result = $s->execute();
				if($es_consulta==true){
					$data = $s->fetchAll();
					if(count($data) > 0) return $data;
					else return false;
				}
				else
					return $result;
			}
			catch(PDOException $e){
				alertar("Error de Sql",$e->getMessage());
			}
		}
	}
?>