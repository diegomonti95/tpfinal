<?php
	class Sucursal{
		protected $sucursal;
		protected $nombre;
		protected $ruc;
		protected $direccion;
		protected $ciudad;
		protected $localidad;
		protected $telefono;
		protected $email;
		protected $pagina_web;
		protected $db;
		
		// Constructor de la Clase
		function __construct($sucursal="",$nombre="",$ruc="",$direccion="",$ciudad="",$localidad="",
		$telefono="",$email="",$pagina_web="",$path=""){
			$this->sucursal = $sucursal;
			$this->nombre = $nombre;
			$this->ruc = $ruc;
			$this->direccion = $direccion;
			$this->ciudad = $ciudad;
			$this->localidad = $localidad;
			$this->telefono = $telefono;
			$this->email = $email;
			$this->pagina_web = $pagina_web;
			
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($clave){
			$sentencia = "select * from sucursales where sucursal = '$clave'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->sucursal = $clave;
				$this->nombre = $registro[0]["nombre"];
				$this->ruc = $registro[0]["ruc"];
				$this->direccion = $registro[0]["direccion"];
				$this->ciudad = $registro[0]["ciudad"];
				$this->localidad = $registro[0]["localidad"];
				$this->telefono = $registro[0]["telefono"];
				$this->email = $registro[0]["email"];
				$this->pagina_web = $registro[0]["pagina_web"];
			}
		}
		
		function listar(){
			$sentencia = "select * from sucursales order by nombre";
			return $this->db->ejecutarConsulta($sentencia);
		}
		
		function agregar(){
			$sentencia = "insert into sucursales(nombre,ruc,direccion,ciudad,localidad,telefono,email,pagina_web)
			values('$this->nombre',
			'$this->ruc',
			'$this->direccion',
			'$this->ciudad',
			'$this->localidad',
			'$this->telefono',
			'$this->email',
			'$this->pagina_web')";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update sucursales
			set nombre = '$this->nombre',
			ruc = '$this->ruc',
			direccion = '$this->direccion',
			ciudad = '$this->ciudad',
			localidad = '$this->localidad',
			telefono = '$this->telefono',
			email = '$this->email',
			pagina_web = '$this->pagina_web'
			where sucursal = '$this->sucursal'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function eliminar($clave){
			$sentencia = "delete from sucursales where sucursal = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
		function getSucursal(){
			return $this->sucursal;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
		function getRuc(){
			return $this->ruc;
		}
		
		function getDireccion(){
			return $this->direccion;
		}
		
		function getCiudad(){
			return $this->ciudad;
		}
		
		function getLocalidad(){
			return $this->localidad;
		}
		
		function getTelefono(){
			return $this->telefono;
		}
		
		function getEmail(){
			return $this->email;
		}
		
		function getPaginaWeb(){
			return $this->pagina_web;
		}
		
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>