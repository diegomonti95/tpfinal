<?php
	class Clasificador{
		protected $clasificador;
		protected $nombre;
		protected $db;
		
		// Constructor de la Clase
		function __construct($clasificador="",$nombre="",$path="")
		{
			$this->clasificador = $clasificador;
			$this->nombre = $nombre;
						
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($clave){
			$sentencia = "select * from clasificadores_gastos where clasificador = '$clave'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->clasificador = $registro[0]["clasificador"];
				$this->nombre = $registro[0]["nombre"];
			}
		}
		
		function listar(){
			$sentencia = "select * from clasificadores_gastos order by nombre";
			return $this->db->ejecutarConsulta($sentencia);
		}
		
		function agregar(){
			$sentencia = "insert into clasificadores_gastos(nombre)
			values('$this->nombre')";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update clasificadores_gastos set	
			nombre = '$this->nombre'
			where clasificador = '$this->clasificador'";  
			return $this->db->ejecutarConsulta($sentencia,false);		
		}
		
		function eliminar($clave){
			$sentencia = "delete from clasificadores_gastos where clasificador = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
		function getClasificador(){
			return $this->clasificador;
		}
		
		function getNombre(){
			return $this->nombre;
		}
		
				
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>