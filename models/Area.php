<?php
	class Area{
		protected $area;
		protected $sucursal;
		protected $nombre;
		protected $db;
		
		// Constructor de la Clase
		function __construct($area="",$sucursal="",$nombre="",$path="")
		{
			$this->area = $area;
			$this->sucursal = $sucursal;
			$this->nombre = $nombre;
						
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($clave){
			$sentencia = "select * from areas where area = '$clave'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->area = $registro[0][0];
				$this->sucursal = $registro[0][1];
				$this->nombre = $registro[0][2];
			}
		}
		
		function listar(){
			$sentencia = "select areas.area,
						sucursales.nombre as nombre_sucursal,
						areas.nombre as nombre_area
						from areas, sucursales
						where areas.sucursal = sucursales.sucursal";
			return $this->db->ejecutarConsulta($sentencia);
		}
		
		function agregar(){
			$sentencia = "insert into areas(sucursal,nombre)
			values('$this->sucursal','$this->nombre')";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update areas set
			sucursal = '$this->sucursal',
			nombre = '$this->nombre'
			where area = '$this->area'";  
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function eliminar($clave){
			$sentencia = "delete from areas where area = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
		function getArea(){
			return $this->area;
		}
		
		function getSucursal(){
			return $this->sucursal;
		}
		
		function getNombre(){
			return $this->nombre;
		}		
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>