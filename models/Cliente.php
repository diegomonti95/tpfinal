<?php
	class Cliente{
		protected $cliente;
		protected $razon_social;
		protected $documento;
		protected $direccion;
		protected $pais;
		protected $ciudad;
		protected $localidad;
		protected $telefono_principal;
		protected $telefono_secundario;
		protected $email;
		protected $personeria;
		protected $db;
		
		// Constructor de la Clase
		function __construct($cliente="",$razon_social="",$documento="",$direccion="",$pais="",$ciudad="",$localidad="",
		$telefono_principal="",$telefono_secundario="",$email="",$personeria="",$path=""){
			$this->cliente = $cliente;
			$this->razon_social = $razon_social;
			$this->documento = $documento;
			$this->direccion = $direccion;
			$this->pais = $pais;
			$this->ciudad = $ciudad;
			$this->localidad = $localidad;
			$this->telefono_principal = $telefono_principal;
			$this->telefono_secundario = $telefono_secundario;
			$this->email = $email;
			$this->personeria = $personeria;
			
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($clave){
			$sentencia = "select * from clientes where cliente = '$clave'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->cliente = $clave;
				$this->razon_social = $registro[0]["razon_social"];
				$this->documento = $registro[0]["documento"];
				$this->direccion = $registro[0]["direccion"];
				$this->pais = $registro[0]["pais"];
				$this->ciudad = $registro[0]["ciudad"];
				$this->localidad = $registro[0]["localidad"];
				$this->telefono_principal = $registro[0]["telefono_principal"];
				$this->telefono_secundario = $registro[0]["telefono_secundario"];
				$this->email = $registro[0]["email"];
				$this->personeria = $registro[0]["personeria"];
			}
		}
		
		function listar(){
			$sentencia = "select	cli.razon_social,
									cli.documento,
									cli.direccion,
									p.nombre as nombre_pais,
									c.nombre as nombre_ciudad,
									l.nombre as nombre_localidad,
									cli.telefono_principal,
									cli.email,
									(case
										when cli.personeria = 'F' then 'Física'
										when cli.personeria = 'J' then 'Jurídica'
										else 'Desconocida'
									end) as personeria,
									cli.cliente
						from 		clientes as cli, paises as p, ciudades as c, localidades as l 
						where 		cli.pais=p.pais
						and			cli.ciudad=c.ciudad
						and			cli.localidad=l.localidad
						order by 1";
			return $this->db->ejecutarConsulta($sentencia);
		}
		
		function agregar(){
			$sentencia = "insert into clientes(razon_social,documento,direccion,pais,
			ciudad,localidad,telefono_principal,telefono_secundario,email,personeria)
			values('$this->razon_social',
			'$this->documento',
			'$this->direccion',
			'$this->pais',
			'$this->ciudad',
			'$this->localidad',
			'$this->telefono_principal',
			'$this->telefono_secundario',
			'$this->email',
			'$this->personeria')";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update clientes
			set razon_social = '$this->razon_social',
			documento = '$this->documento',
			direccion = '$this->direccion',
			pais = '$this->pais',
			ciudad = '$this->ciudad',
			localidad = '$this->localidad',
			telefono_principal = '$this->telefono_principal',
			telefono_secundario = '$this->telefono_secundario',
			email = '$this->email',
			personeria = '$this->personeria'
			where cliente = '$this->cliente'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function eliminar($clave){
			$sentencia = "delete from clientes where cliente = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
				
		function getCliente(){
			return $this->cliente;
		}
		
		function getRazonSocial(){
			return $this->razon_social;
		}
		
		function getDocumento(){
			return $this->documento;
		}
		
		function getDireccion(){
			return $this->direccion;
		}
		
		function getPais(){
			return $this->pais;
		}
		
		function getCiudad(){
			return $this->ciudad;
		}
		
		function getLocalidad(){
			return $this->localidad;
		}
		
		function getTelefonoPrincipal(){
			return $this->telefono_principal;
		}
		
		function getTelefonoSecundario(){
			return $this->telefono_secundario;
		}
		
		function getEmail(){
			return $this->email;
		}
		
		function getPersoneria(){
			return $this->personeria;
		}
		
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>