<?php
	class User{
		protected $usuario;
		protected $clave;
		protected $nombres;
		protected $fecha_registro;
		protected $perfil;
		protected $estado;
		protected $db;
		
		// Constructor de la Clase
		function __construct($usuario="",$clave="",$nombres="",$fecha_registro="",$perfil="",$estado="",$path=""){
			$this->usuario = $usuario;
			$this->clave = $clave;
			$this->nombres = $nombres;
			$this->fecha_registro = $fecha_registro;
			$this->perfil = $perfil;
			$this->estado = $estado;
			
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($clave){
			$sentencia = "select * from usuarios where usuario = '$clave'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->usuario = $registro[0][0];
				$this->clave = $registro[0][1];
				$this->nombres = $registro[0][2];
				$this->fecha_registro = $registro[0][3];
				$this->perfil = $registro[0][4];
				$this->estado = $registro[0][5];
			}
		}
		
		function listar(){
			$sentencia = "select * from usuarios order by usuario";
			return $this->db->ejecutarConsulta($sentencia);
		}
		
		function agregar(){
			$sentencia = "insert into usuarios 
			values('$this->usuario',
			'$this->clave',
			'$this->nombres',
			'$this->fecha_registro',
			'$this->perfil',
			'$this->estado')";
			
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update usuarios 
			set clave = '$this->clave',
			nombres = '$this->nombres',
			fecha_registro = '$this->fecha_registro',
			perfil = '$this->perfil',
			estado = '$this->estado'
			where usuario = '$this->usuario' ";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function eliminar($clave){
			$sentencia = "delete from usuarios where usuario = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
		function getUsuario(){
			return $this->usuario;
		}
		
		function getClave(){
			return $this->clave;
		}
		
		function getNombres(){
			return $this->nombres;
		}
		
		function getFechaRegistro(){
			return $this->fecha_registro;
		}
		
		function getPerfil(){
			return $this->perfil;
		}
		
		function getEstado(){
			return $this->estado;
		}
		
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>