<?php
	class Gasto{
		protected $gasto;
		protected $sucursal;
		protected $proveedor;
		protected $presupuesto_vta;
		protected $moneda;
		protected $clasificador;
		protected $nro_comprobante;
		protected $fecha;
		protected $condicion;
		protected $total_compra;
		protected $total_impuesto;
		protected $estado;
		protected $db;
		
		// Constructor de la Clase
		function __construct($gasto="",$sucursal="",$proveedor="",$presupuesto_vta="",$moneda="",$clasificador="",
		$nro_comprobante="",$fecha="",$condicion="",$total_compra="",$total_impuesto="",$estado="",$path=""){
			$this->gasto = $gasto;
			$this->sucursal = $sucursal;
			$this->proveedor = $proveedor;
			$this->presupuesto_vta = $presupuesto_vta;
			$this->moneda = $moneda;
			$this->clasificador = $clasificador;
			$this->nro_comprobante = $nro_comprobante;
			$this->fecha = $fecha;
			$this->condicion = $condicion;
			$this->total_compra = $total_compra;
			$this->total_impuesto = $total_impuesto;
			$this->estado = $estado;
			
			include_once("DataBase.php");
			$this->db = new DataBase($path);
		}
		
		function consultar($clave){
			$sentencia = "select * from gastos where gasto = '$clave'";
			$registro = $this->db->ejecutarConsulta($sentencia);
			if ($registro){
				$this->gasto = $clave;
				$this->sucursal = $registro[0]["sucursal"];
				$this->proveedor = $registro[0]["proveedor"];
				$this->presupuesto_vta = $registro[0]["presupuesto_vta"];
				$this->moneda = $registro[0]["moneda"];
				$this->clasificador = $registro[0]["clasificador"];
				$this->nro_comprobante = $registro[0]["nro_comprobante"];
				$this->fecha = $registro[0]["fecha"];
				$this->condicion = $registro[0]["condicion"];
				$this->total_compra = $registro[0]["total_compra"];
				$this->total_impuesto = $registro[0]["total_impuesto"];
				$this->estado = $registro[0]["estado"];
			}
		}
		
		function listar(){
			$sentencia = "select	to_char(gas.fecha,'dd-mm-yyyy HH:MM') as fecha,
									gas.nro_comprobante,
									pro.razon_social,
									mon.nombre as nombre_moneda,
									cla.nombre as nombre_clasificador,
									(case 
                                        when gas.condicion = 'C' then 'CONTADO'
                                        when gas.condicion = 'R' then 'CREDITO'
                                    end) as condicion,
									to_char(gas.total_compra,'999G999G999.99') as total_compra,
									gas.gasto
							from    gastos gas, proveedores pro, monedas mon, clasificadores_gastos cla
							where	gas.proveedor = pro.proveedor
							and 	gas.moneda = mon.moneda
							and		gas.clasificador = cla.clasificador 	
							order by 1,2";				
			return $this->db->ejecutarConsulta($sentencia);
		}	
		
		function agregar(){
			$sentencia = "insert into gastos(sucursal,proveedor,presupuesto_vta,moneda,clasificador,nro_comprobante,fecha,condicion,total_compra,total_impuesto,estado)
			values('$this->sucursal',
			'$this->proveedor',
			'$this->presupuesto_vta',
			'$this->moneda',
			'$this->clasificador',
			'$this->nro_comprobante',
			'$this->fecha',
			'$this->condicion'
			'$this->total_compra'
			'$this->total_impuesto'
			'$this->estado')";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function editar(){
			$sentencia = "update gastos
			set sucursal = '$this->sucursal',
			proveedor = '$this->proveedor',
			presupuesto_vta = '$this->presupuesto_vta',
			moneda = '$this->moneda',
			clasificador = '$this->clasificador',
			nro_comprobante = '$this->nro_comprobante',
			fecha = '$this->fecha',
			condicion = '$this->condicion',
			total_compra = '$this->total_compra',
			total_impuesto = '$this->total_impuesto',
			estado = '$this->estado'
			where gasto = '$this->gasto'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		function eliminar($clave){
			$sentencia = "delete from gastos where gasto = '$clave'";
			return $this->db->ejecutarConsulta($sentencia,false);
		}
		
		// Funciones Getters
		
		function getGasto(){
			return $this->gasto;
		}
		
		function getSucursal(){
			return $this->sucursal;
		}
		
		function getProveedor(){
			return $this->proveedor;
		}
		
		function getPresupuesto_vta(){
			return $this->presupuesto_vta;
		}
		
		function getMoneda(){
			return $this->moneda;
		}
		
		function getClasificador(){
			return $this->clasificador;
		}
		
		function getNro_comprobante(){
			return $this->nro_comprobante;
		}
		
		function getFecha(){
			return $this->fecha;
		}
		
		function getCondicion(){
			return $this->condicion;
		}
		
		function getTotal_compra(){
			return $this->total_compra;
		}
		
		function getTotal_impuesto(){
			return $this->total_impuesto;
		}
		
		function getEstado(){
			return $this->estado;
		}
		
		
		// Destructor de la clase
		function __destruct(){
			unset($this->db);
		}
	}
?>