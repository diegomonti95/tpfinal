<!DOCTYPE hmtl>
<html>
	<head>
		<meta charset="utf-8">
		<title>Menú Importadora</title>
		<script src="../../controllers/js/jquery.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/bootstrap.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/popper.min.js"></script>
		<script src="../../controllers/js/code.js"></script>
		<link href="../../controllers/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="../css/style.css" rel="stylesheet"/>
		<link href="../css/menu.css" rel="stylesheet"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- DataTable -->
		<link href="../../controllers/js/bootstrap/css/jquery.dataTables.min.css" rel="stylesheet">
		<script src="../../controllers/js/bootstrap/js/jquery.dataTables.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php 
					include_once("../../controllers/lib/lib.php"); 
					sesion_usuario();
					/* ---------------------------------------------- */
					$clave = $_GET["clave"];
					include_once("../../models/Cliente.php");
					$cli = new Cliente("","","","","","","","","","","","../../models/params.ini");
					$cli->consultar($clave);
				?>
			</div>
			<br>
			<div class="col-sm-10 col-sm-offset-1">
				<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('col-sm-10');">
					<span aria-hidden='true'>&times;</span>
				</button>
				<h3>Editar Cliente <hr></h3>
				<div id="result"></div>
				<form>
				<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Cliente:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control text-left" id="cliente" 
							placeholder="" disabled value="<?php echo $cli->getCliente(); ?>">
						</div>
				</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Razon Social:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control text-left" id="razon_social" 
							placeholder="Razon Social del Cliente" maxlength="50px" value="<?php echo $cli->getRazonSocial(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Documento:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control text-left" id="documento" 
							placeholder="Ej.: 643215-8" maxlength="50px" value="<?php echo $cli->getDocumento(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Dirección:</label>
						<div class="col-sm-6">
							<textarea class="form-control text-left" id="direccion" 
							placeholder="Dirección del Cliente" maxlength="150px"><?php echo $cli->getDireccion(); ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Pais:</label>
						<div class="col-sm-6">
							<?php lista_datos("pais","select pais, nombre from paises order by 2",$cli->getPais(),"filtrarCiudad();"); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Ciudad:</label>
						<div class="col-sm-6">
							<?php lista_datos("ciudad","select ciudad, nombre from ciudades order by 2",$cli->getCiudad(),"filtrarLocalidad();"); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Localidad:</label>
						<div class="col-sm-6">
							<?php lista_datos("localidad","select localidad, nombre from localidades order by 2",$cli->getLocalidad()); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Teléfono:</label>
						<div class="col-sm-5">
							<input type="text" class="form-control text-left" id="telefono_principal" 
							placeholder="Ej.: 098164556 / 021899653" maxlength="10px" value="<?php echo $cli->getTelefonoPrincipal(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Teléfono2:</label>
						<div class="col-sm-5">
							<input type="text" class="form-control text-left" id="telefono_secundario" 
							placeholder="Ej.: 098164556 / 021899653" maxlength="10px" value="<?php echo $cli->getTelefonoSecundario(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">E-Mail:</label>
						<div class="col-sm-6">
							<input type="email" class="form-control text-left" id="email" 
							placeholder="Ej: ejemplo@gmail.com" maxlength="50px"value="<?php echo $cli->getEmail(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Personería:</label>
						<div class="col-sm-4">
							<select class="form-control" id="personeria">
								<option value="J"<?php echo 'J' === $cli->getPersoneria() ? "selected='selected'" : ""; ?> >Jurídica</option>
								<option value="F"<?php echo 'F' === $cli->getPersoneria() ? "selected='selected'" : ""; ?> >Fisica</option>
							</select> 
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-5">
							<button type="button" class="btn btn-secondary" onclick="editarCliente();">Actualizar</button>
							<button type="button" class="btn btn-info" onclick="location.href='clientes-listar.php'" > ■ Lista</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script>
		$("#razon_social").focus();
	</script>
</html>