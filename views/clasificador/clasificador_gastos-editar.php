<!DOCTYPE hmtl>
<html>
	<head>
		<meta charset="utf-8">
		<title>Menú Importadora</title>
		<script src="../../controllers/js/jquery.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/bootstrap.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/popper.min.js"></script>
		<script src="../../controllers/js/code.js"></script>
		<link href="../../controllers/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="../css/style.css" rel="stylesheet"/>
		<link href="../css/menu.css" rel="stylesheet"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- DataTable -->
		<link href="../../controllers/js/bootstrap/css/jquery.dataTables.min.css" rel="stylesheet">
		<script src="../../controllers/js/bootstrap/js/jquery.dataTables.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php 
					include_once("../../controllers/lib/lib.php"); 
					sesion_usuario();
					/* ---------------------------------------------- */
					$clave = $_GET["clave"];
					include_once("../../models/Clasificador.php");
					$mon = new Clasificador("","","","../../models/params.ini");
					$mon->consultar($clave);
				?>
			</div>
			<br>
			<div class="col-sm-10 col-sm-offset-1">
				<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('col-sm-10');">
					<span aria-hidden='true'>&times;</span>
				</button>
				<h3>Editar Clasificador<hr></h3>
				<div id="result"></div>
				<form>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Clasificador:</label>
						<div class="col-sm-2">
							<input type="text" class="form-control text-left" id="clasificador" 
							placeholder="Identificador" maxlength="15px" 
							value="<?php echo $mon->getClasificador(); ?>" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Nombre:</label>
						<div class="col-sm-5">
							<input type="text" class="form-control text-left" id="nombre" 
							placeholder="Nombre del Clasificador" 
							maxlength="50px" value="<?php echo $mon->getNombre(); ?>">
						</div>
					</div>
		
					
					<div class="form-group row">
						<div class="col-sm-5">
							<button type="button" class="btn btn-secondary" onclick="editarClasificador();">Actualizar</button>
							<button type="button" class="btn btn-info" onclick="location.href='clasificador_gastos-listar.php'" > ■ Lista</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script>
		$("#nombre").focus();
	</script>
</html>