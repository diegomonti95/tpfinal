<!DOCTYPE hmtl>
<html>
	<head>
		<meta charset="utf-8">
		<title>Menú Importadora</title>
		<script src="../../controllers/js/jquery.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/bootstrap.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/popper.min.js"></script>
		<script src="../../controllers/js/code.js"></script>
		<link href="../../controllers/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="../css/style.css" rel="stylesheet"/>
		<link href="../css/menu.css" rel="stylesheet"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- DataTable -->
		<link href="../../controllers/js/bootstrap/css/jquery.dataTables.min.css" rel="stylesheet">
		<script src="../../controllers/js/bootstrap/js/jquery.dataTables.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php 
					include_once("../../controllers/lib/lib.php"); 
					sesion_usuario(); sesion_administrador();
				?>
			</div>
			<br>
			<div class="col-sm-10 col-sm-offset-1">
				<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('col-sm-10');">
					<span aria-hidden='true'>&times;</span>
				</button>
				<h3>Agregar Proveedor <hr></h3>
				<div id="result"></div>
				<form>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Razon Social:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control text-left" id="razon_social" placeholder="Razon Social del Proveedor" maxlength="50px">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Documento:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control text-left" id="documento" placeholder="Ej.: 643215-8" maxlength="50px">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Dirección:</label>
						<div class="col-sm-6">
							<textarea class="form-control text-left" id="direccion" placeholder="Dirección del Proveedor" maxlength="150px"></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Pais:</label>
						<div class="col-sm-6">
							<?php lista_datos("pais","select pais, nombre from paises order by 2",0,"filtrarCiudad();"); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Ciudad:</label>
						<div class="col-sm-6">
							<?php lista_datos("ciudad","select ciudad, nombre from ciudades order by 2",0,"filtrarLocalidad();"); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Localidad:</label>
						<div class="col-sm-6">
							<?php lista_datos("localidad","select localidad, nombre from localidades order by 2",0,""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Teléfono:</label>
						<div class="col-sm-5">
							<input type="text" class="form-control text-left" id="telefono" placeholder="Ej.: 01121527610" maxlength="10px">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">E-Mail:</label>
						<div class="col-sm-6">
							<input type="email" class="form-control text-left" id="email" placeholder="Ej: ejemplo@gmail.com" maxlength="50px">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Personería:</label>
						<div class="col-sm-4">
							<select class="form-control" id="personeria">
								<option value='J'>Jurídica</option>
								<option value='F' selected>Física</option>
							</select> 
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-5">
							<button type="button" class="btn btn-secondary" onclick="agregarProveedor();">Actualizar</button>
							<button type="button" class="btn btn-info" onclick="location.href='proveedores-listar.php'" > ■ Lista</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script>
		$("#razon_social").focus();
	</script>
</html>