<!DOCTYPE html>
<html>
	<head>
		<meta charset = "utf-8">
		<title>Importadora</title>
		<script src="../../controllers/js/jquery.min.js"></script>
		<script src="../../controllers/js/code.js"></script>
		<script src="../../controllers/js/bootstrap/js/bootstrap.min.js"></script>
		<!-- css -->
		<link href="../../controllers/js/bootstrap/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css"/>
		<link href="../css/style.css" rel="stylesheet"/>
	</head>
	<body>
		<div id="result"></div>
		<div class="wrapper fadeInDown">
			<div id="formContent">
				<div class="fadeIn first">
					<img src="../imgs/logo.png" id="icon" alt="User Icon" />
				</div>
				<form>
					<input type="text" id="user" name="user" class="fadeIn second" placeholder="Usuario">
					<input type="password" id="password" name="passowrd" class="fadeIn third" placeholder="Contraseña">
					<input type="button" class="fadeIn fourth" value="INGRESAR" onclick="acceder();">
				</form>
			</div>
		</div>
	</body>
	<script>
		$("#user").focus();
	</script>
</html>