<!DOCTYPE hmtl>
<html>
	<head>
		<meta charset="utf-8">
		<title>Menú Importadora</title>
		<script src="../../controllers/js/jquery.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/bootstrap.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/popper.min.js"></script>
		<script src="../../controllers/js/code.js"></script>
		<link href="../../controllers/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="../css/style.css" rel="stylesheet"/>
		<link href="../css/menu.css" rel="stylesheet"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- DataTable -->
		<link href="../../controllers/js/bootstrap/css/jquery.dataTables.min.css" rel="stylesheet">
		<script src="../../controllers/js/bootstrap/js/jquery.dataTables.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php 
					include_once("../../controllers/lib/lib.php"); 
					sesion_usuario(); sesion_administrador();
					/* ---------------------------------------------- */
					$clave = $_GET["clave"];
					include_once("../../models/Gasto.php");
					$gas = new Gasto("","","","","","","","","","","","","../../models/params.ini");
					$gas->consultar($clave);
				?>
			</div>
			<br>
			<div class="col-sm-10 col-sm-offset-1">
				<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('col-sm-10');">
					<span aria-hidden='true'>&times;</span>
				</button>
				<h3>Editar Gasto <hr></h3>
				<div id="result"></div>
				<form>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Gasto:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control text-left" id="gasto" 
							placeholder="" disabled value="<?php echo $gas->getSucursal(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Sucursal:</label>
						<div class="col-sm-4">
							<?php lista_datos("sucursal","select sucursal,nombre from sucursales order by 2",
							$area->getSucursal(),""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Proveedor:</label>
						<div class="col-sm-4">
							<?php lista_datos("proveedor","select proveedor,razon_social from proveedores order by 2",
							$gas->getProveedor(),""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Presupuesto de Venta:</label>
							<div class="col-sm-4">
							<?php lista_datos("presupuesto","select presupuesto,nombre from presupuestos_ventas order by 2",
							$gas->getPresupuesto(),""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Moneda:</label>
						<div class="col-sm-4">
							<?php lista_datos("moneda","select moneda,nombre from monedas order by 2",
							$gas->getMoneda(),""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Clasificador de Gastos:</label>
						<div class="col-sm-4">
							<?php lista_datos("clasificador","select clasificador,nombre from clasificadores_gastos order by 2",
							$gas->getClasificador(),""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Número de Comprobante:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control text-left" id="nro_comprobante" 
							placeholder="" maxlength="50px" value="<?php echo $gas->getNro_comprobante(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Fecha:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="fecha_registro" 
							placeholder="" value="<?php  echo fecha_formato($gas->getFecha(),true,false); ?>" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Condición:</label>
						<div class="col-sm-4">
							<select class="form-control" id="perfil">
								<option value="A"<?php echo 'R' === $gas->getPerfil() ? "selected='selected'" : ""; ?> >Administrador</option>
								<option value="C"<?php echo 'C' === $gas->getPerfil() ? "selected='selected'" : ""; ?> >Consulta</option>
							</select> 
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Compra Total:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control text-left" id="total_compra" 
							placeholder="" maxlength="50px" value="<?php echo $gas->getTotal_compra(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Impuesto Total:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control text-left" id="total_impuesto" 
							placeholder="" maxlength="50px" value="<?php echo $gas->getTotal_impuesto(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-2">Habilitado:</div>
						<div class="col-sm-5">
							<div class="form-check">
								<label class="form-check-label">
									<input class="form-check-input" type="checkbox" id="estado" 
									<?php chequeo($gas->getEstado()); ?> > Sí
								</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-5">
							<button type="button" class="btn btn-secondary" onclick="editarGasto();">Actualizar</button>
							<button type="button" class="btn btn-info" onclick="location.href='gastos-listar.php'" > ■ Lista</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script>
		$("#sucursal").focus();
	</script>
</html>