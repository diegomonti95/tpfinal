<!DOCTYPE hmtl>
<html>
	<head>
		<meta charset="utf-8">
		<title>Menú Importadora</title>
		<script src="../../controllers/js/jquery.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/bootstrap.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/popper.min.js"></script>
		<script src="../../controllers/js/code.js"></script>
		<link href="../../controllers/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="../css/style.css" rel="stylesheet"/>
		<link href="../css/menu.css" rel="stylesheet"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- DataTable -->
		<link href="../../controllers/js/bootstrap/css/jquery.dataTables.min.css" rel="stylesheet">
		<script src="../../controllers/js/bootstrap/js/jquery.dataTables.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php 
					include_once("../../controllers/lib/lib.php"); 
					sesion_usuario(); 
					/* ---------------------------------------------- */
					$clave = $_GET["clave"];
					include_once("../../models/Producto.php");
					$s = new Producto("","","","","","","","","../../models/params.ini");
					$s->consultar($clave);
				?>
			</div>
			<br>
			<div class="col-sm-10 col-sm-offset-1">
				<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('col-sm-10');">
					<span aria-hidden='true'>&times;</span>
				</button>
				<h3>Editar Producto <hr></h3>
				<div id="result"></div>
				<form>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Producto:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control text-left" id="producto" 
							placeholder="" disabled value="<?php echo $s->getProducto(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Código:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control text-left" id="codigo" 
							placeholder="Ej.: 643215" maxlength="50px" value="<?php echo $s->getCodigo(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Nombre:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control text-left" id="nombre" 
							placeholder="Nombre del producto" maxlength="50px" value="<?php echo $s->getNombre(); ?>">
						</div>
					</div>
					
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Descripción:</label>
						<div class="col-sm-6">
							<textarea class="form-control text-left" id="descripcion" 
							placeholder="Descripcción del producto" maxlength="150px"><?php echo $s->getDescripcion(); ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Clasificación:</label>
						<div class="col-sm-6">
							<?php lista_datos("clasificacion","select clasificacion,nombre from clasificaciones order by 2",$s->getClasificacion(),""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Unidad:</label>
						<div class="col-sm-6">
							<?php lista_datos("unidad","select unidad,nombre from unidades	 order by 2",$s->getUnidad(),""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Impuesto:</label>
						<div class="col-sm-4">
							<select class="form-control" id="impuesto">
								<option value="0"<?php echo '0' === $s->getImpuesto() ? "selected='selected'" : ""; ?> >Exento</option>
								<option value="1"<?php echo '1' === $s->getImpuesto() ? "selected='selected'" : ""; ?> >IVA 5%</option>
								<option value="2"<?php echo '2' === $s->getImpuesto() ? "selected='selected'" : ""; ?> >IVA 10%</option>
							</select> 
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-2">Habilitado:</div>
						<div class="col-sm-5">
							<div class="form-check">
								<label class="form-check-label">
									<input class="form-check-input" type="checkbox" id="habilitado" 
									<?php chequeo($s->getHabilitado(),"producto"); ?> > Sí
								</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-5">
							<button type="button" class="btn btn-secondary" onclick="editarProducto();">Actualizar</button>
							<button type="button" class="btn btn-info" onclick="location.href='productos-listar.php'" > ■ Lista</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script>
		$("#codigo").focus();
	</script>
</html>