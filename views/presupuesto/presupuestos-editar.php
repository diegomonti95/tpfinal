<!DOCTYPE hmtl>
<html>
	<head>
		<meta charset="utf-8">
		<title>Menú Importadora</title>
		<script src="../../controllers/js/jquery.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/bootstrap.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/popper.min.js"></script>
		<script src="../../controllers/js/code.js"></script>
		<link href="../../controllers/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="../css/style.css" rel="stylesheet"/>
		<link href="../css/menu.css" rel="stylesheet"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- DataTable -->
		<link href="../../controllers/js/bootstrap/css/jquery.dataTables.min.css" rel="stylesheet">
		<script src="../../controllers/js/bootstrap/js/jquery.dataTables.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php 
					include_once("../../controllers/lib/lib.php"); 
					sesion_usuario(); sesion_administrador();
					/* ---------------------------------------------- */
					$clave = $_GET["clave"];
					include_once("../../models/Presupuesto.php");
					$pr = new Presupuesto("","","","","","","","","","","../../models/params.ini");
					$pr->consultar($clave);
				?>
			</div>
			<br>
			<div class="col-sm-10 col-sm-offset-1">
				<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('col-sm-10');">
					<span aria-hidden='true'>&times;</span>
				</button>
				<h3>Editar Presupuesto de Ventas <hr></h3>
				<div id="result"></div>
				<form>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Presupuesto:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control text-left" id="presupuesto" 
							placeholder="" disabled value="<?php echo $pr->getPresupuesto(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Sucursal:</label>
						<div class="col-sm-6">
							<?php lista_datos("sucursal","select sucursal, nombre from sucursales order by 2",$pr->getSucursal()); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Cliente:</label>
						<div class="col-sm-6">
							<?php lista_datos("cliente","select cliente, nombre from clientes order by 2",$pr->getClientes()); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Nombre:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control text-left" id="nombre" 
							placeholder="" maxlength="50px" value="<?php echo $pr->getNombre(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Fecha del Presupuesto:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="fecha_presupuesto" 
							placeholder="" value="<?php  echo fecha_formato($pr->getFechaPresupuesto(),true,false); ?>" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Monto del Presupuesto:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control text-left" id="monto_presupuesto" 
							placeholder="" maxlength="50px" value="<?php echo $pr->getMontoPresupuesto(); ?>">
						</div>
					</div>	
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Encargado:</label>
						<div class="col-sm-6">
							<textarea class="form-control text-left" id="encargado" 
							placeholder="Julian Casablancas" maxlength="150px"><?php echo $pr->getEncargado(); ?></textarea>
						</div>-
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Condicion:</label>
						<div class="col-sm-4">
							<select class="form-control" id="condicion">
								<option value="R"<?php echo 'R' === $pr->getCondicion() ? "selected='selected'" : ""; ?> >Credito</option>
								<option value="C"<?php echo 'C' === $pr->getCondicion() ? "selected='selected'" : ""; ?> >Contado</option>
							</select> 
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Estado:</label>
						<div class="col-sm-4">
							<select class="form-control" id="estado">
								<option value="0"<?php echo 'A' === $pr->getEstado() ? "selected='selected'" : ""; ?> >Aprobado</option>
								<option value="1"<?php echo 'P' === $pr->getEstado() ? "selected='selected'" : ""; ?> >Pendiente</option>
								<option value="2"<?php echo 'R' === $pr->getEstado() ? "selected='selected'" : ""; ?> >Rechazado</option>
							</select> 
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Observación:</label>
						<div class="col-sm-6">
							<textarea class="form-control text-left" id="observacion" 
							placeholder="" maxlength="150px"><?php echo $pro->getObservacion(); ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-5">
							<button type="button" class="btn btn-secondary" onclick="editarPresupuesto();">Actualizar</button>
							<button type="button" class="btn btn-info" onclick="location.href='presupuestos-listar.php'" > ■ Lista</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script>
		$("#sucursal").focus();
	</script>
</html>