<?php
	// Librerías del Phpjasperxml
	include_once("../../controllers/lib/phpjasperxml/tcpdf/tcpdf.php");
	include_once("../../controllers/lib/phpjasperxml/PHPJasperXML.inc.php");
	include_once("../../controllers/lib/phpjasperxml/setting.php");
	include_once("../../controllers/lib/lib.php");

	$usuario		= $_POST["usuario"];
	$fecha_desde	= $_POST["fecha_desde"];
	$fecha_hasta	= $_POST["fecha_hasta"];
	$tabla 			= $_POST["tabla"];
	$accion			= $_POST["accion"];
	
	$sentencia = "select	au.auditoria,
							au.usuario,
							au.tabla,
							to_char(au.fecha,'dd/mm/yyyy hh:mm') as fecha,
							au.observacion,
							(case 
								when au.accion = 'A' then 'ACCESO'
								when au.accion = 'S' then 'SALIDA'
								when au.accion = 'I' then 'INSERCIÓN'
								when au.accion = 'E' then 'ELIMINACIÓN'
								when au.accion = 'M' then 'MODIFICACIÓN'
							end) as accion
					from 	auditorias as au
					where	1 = 1";

	// Filtros del SQL
	if($usuario != "") $sentencia .= " and au.usuario = '" . $usuario . "'";
	if($fecha_desde != "" && $fecha_desde != "__/__/____ __:__") $sentencia .= " and au.fecha >= '" . $fecha_desde . "'";
	if($fecha_hasta != "" && $fecha_hasta != "__/__/____ __:__") $sentencia .= " and au.fecha <= '" . $fecha_hasta . "'";
	if($tabla != "") $sentencia .= " and au.tabla = '" . $tabla . "'";
	if($accion != "") $sentencia .= " and au.accion = '" . $accion . "'";
			
	$sentencia .= " order by au.usuario, au.fecha";
			
	include_once("../../models/DataBase.php");
	$dataBase = new DataBase("../../models/params.ini");
			
	$row = $dataBase->ejecutarConsulta($sentencia);
	if(!$row){
		echo "<script> alert('No existen Datos. Verifique'); 
		location.href='rpt-auditorias-sistema.php'; </script>";
		return;
	}
				
	$PHPJasperXML = new PHPJasperXML();
	$PHPJasperXML->arrayParameter=array("parameter1"=>"1");
	$PHPJasperXML->load_xml_file("rpt-auditorias-sistema.jrxml");
	$PHPJasperXML->sql=$sentencia;
	$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db,"psql");
	$PHPJasperXML->outpage("I");
			
	unset($dataBase);
?>