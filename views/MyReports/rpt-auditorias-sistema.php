<!DOCTYPE hmtl>
<html>
	<head>
		<meta charset="utf-8">
		<title>Menú Importadora</title>
		<script src="../../controllers/js/jquery.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/bootstrap.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/popper.min.js"></script>
		<script src="../../controllers/js/code.js"></script>
		<link href="../../controllers/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="../css/style.css" rel="stylesheet"/>
		<link href="../css/menu.css" rel="stylesheet"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- datetimepicker -->
		<link rel="stylesheet" type="text/css" href="../../controllers/js/datetimepicker/jquery.datetimepicker.css">
		<script src="../../controllers/js/datetimepicker/build/jquery.datetimepicker.full.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php 
					include_once("../../controllers/lib/lib.php"); 
					sesion_usuario(); 
					sesion_administrador();
				?>
			</div>
			<br>
			<div class="col-sm-10 col-sm-offset-1">
				<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('col-sm-10');">
					<span aria-hidden='true'>&times;</span>
				</button>
				<h3>Reporte de Auditorías <hr></h3>
				<form method="post" action="rpt-auditorias-sistema-code.php">
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Usuario:</label>
						<div class="col-sm-6">
							<?php lista_datos("usuario","select usuario, usuario from usuarios order by 2",0,"",""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Fecha:</label>
						<div class="col-sm-2">
							<input type="datetime" class="form-control text-left" id="fecha_desde" name="fecha_desde"
							placeholder="dd/mm/aaaa hh:mm" maxlength="16">
						</div>
						<div class="col-sm-2">
							<input type="datetime" class="form-control text-left" id="fecha_hasta" name="fecha_hasta"
							placeholder="dd/mm/aaaa hh:mm" maxlength="16">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Acción:</label>
						<div class="col-sm-4">
							<select class="form-control" id="accion" name="accion">
								<option value=""></option>
								<option value="A">Acceso</option>
								<option value="S">Salida</option>
								<option value="I">Inserción</option>
								<option value="M">Modificación</option>
								<option value="E">Eliminación</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Tabla:</label>
						<div class="col-sm-6">
							<?php lista_datos("tabla","select distinct tabla, tabla from auditorias order by 2",0,"",""); ?>
						</div>
					</div>	
					<div class="form-group row">
						<div class="col-sm-5">
							<button type="submit" class="btn btn-secondary">Visualizar Reporte</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script>
		jQuery("#fecha_desde").datetimepicker({format:"d/m/Y H:i",mask:true});
		jQuery("#fecha_hasta").datetimepicker({format:"d/m/Y H:i",mask:true});
		$("#usuario").prop('selectedIndex', 0);
		$("#tabla").prop('selectedIndex', 0);
		$("#usuario").focus();
	</script>
</html>