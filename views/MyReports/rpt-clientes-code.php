<?php
	// Librerías del Phpjasperxml
	include_once("../../controllers/lib/phpjasperxml/tcpdf/tcpdf.php");
	include_once("../../controllers/lib/phpjasperxml/PHPJasperXML.inc.php");
	include_once("../../controllers/lib/phpjasperxml/setting.php");
	include_once("../../controllers/lib/lib.php");

	$cliente 			= $_POST["cliente"];
	$documento 			= $_POST["documento"];
	$pais 				= $_POST["pais"];
	$ciudad				= $_POST["ciudad"];
	$personeria 		= $_POST["personeria"];
	
	$sentencia =  "select	cli.razon_social,
							cli.documento,
							cli.direccion,
							p.nombre as nombre_pais,
							c.nombre as nombre_ciudad,
							l.nombre as nombre_localidad,
							cli.telefono_principal,
							cli.email,
							(case
								when cli.personeria = 'F' then 'Física'
								when cli.personeria = 'J' then 'Jurídica'
								else 'Desconocida'
							end) as personeria,
							cli.cliente
					from 	clientes as cli, paises as p, ciudades as c, localidades as l 
					where 	cli.pais=p.pais
					and		cli.ciudad=c.ciudad
					and		cli.localidad=l.localidad";

	// Filtros del SQL
	if($cliente != "") $sentencia .= " and cli.cliente = '" . $cliente . "'";
	if($documento != "") $sentencia .= " and cli.documento = '" . $documento . "'";
	if($pais != "") $sentencia .= " and cli.pais = '" . $pais . "'";
	if($ciudad != "") $sentencia .= " and cli.ciudad = '" . $ciudad . "'";
	if($personeria != "") $sentencia .= " and cli.personeria = '" . $personeria . "'";
			
	$sentencia .= " order by cli.razon_social";
	
	include_once("../../models/DataBase.php");
	$dataBase = new DataBase("../../models/params.ini");
			
	$row = $dataBase->ejecutarConsulta($sentencia);
	if(!$row){
		echo "<script> alert('No existen Datos. Verifique'); 
		location.href='rpt-clientes.php'; </script>";
		return;
	}
				
	$PHPJasperXML = new PHPJasperXML();
	$PHPJasperXML->arrayParameter=array("parameter1"=>"1");
	$PHPJasperXML->load_xml_file("rpt-clientes.jrxml");
	$PHPJasperXML->sql=$sentencia;
	$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db,"psql");
	$PHPJasperXML->outpage("I");
			
	unset($dataBase);
?>