<!DOCTYPE hmtl>
<html>
	<head>
		<meta charset="utf-8">
		<title>Menú Importadora</title>
		<script src="../../controllers/js/jquery.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/bootstrap.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/popper.min.js"></script>
		<script src="../../controllers/js/code.js"></script>
		<link href="../../controllers/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="../css/style.css" rel="stylesheet"/>
		<link href="../css/menu.css" rel="stylesheet"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- DataTable -->
		<link href="../../controllers/js/bootstrap/css/jquery.dataTables.min.css" rel="stylesheet">
		<script src="../../controllers/js/bootstrap/js/jquery.dataTables.min.js"></script>
		
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php 
					include_once("../../controllers/lib/lib.php"); 
					sesion_usuario(); sesion_administrador();  
				?>
			</div>
			<br>
			<div class="col-sm-10 col-sm-offset-1">
				<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('col-sm-10');">
					<span aria-hidden='true'>&times;</span>
				</button>
				<h3>Reporte de Proveedores <hr></h3>
				<form method="post" action="rpt-proveedores-code.php">
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Proveedor:</label>
						<div class="col-sm-2">
							<input type="text" class="form-control text-left" id="proveedor" name=proveedor  
							maxlength="5px">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Documento:</label>
						<div class="col-sm-4">
							<?php lista_datos("documento","select distinct documento, documento from proveedores order by 2",0,"",""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Pais:</label>
						<div class="col-sm-4">
							<?php lista_datos("pais","select pais, nombre from paises order by 2",0,"filtrarCiudad();"); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Ciudad:</label>
						<div class="col-sm-4">
							<?php lista_datos("ciudad","select ciudad, nombre from ciudades order by 2",0,""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Personería:</label>
						<div class="col-sm-4">
							<select class="form-control" id="personeria" name="personeria">
								<option value=''></option>
								<option value='J'>Jurídica</option>
								<option value='F' selected>Física</option>
							</select> 
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-5">
							<button type="submit" class="btn btn-secondary">Visualizar Reporte</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script>
		$("#pais").prop('selectedIndex', 0);
		$("#ciudad").prop('selectedIndex', 0);
		$("#personeria").prop('selectedIndex', 0);
		$("#proveedor").focus();
	</script>
</html>