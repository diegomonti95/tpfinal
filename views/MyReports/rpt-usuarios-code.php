<?php
	// Librerías del Phpjasperxml
	include_once("../../controllers/lib/phpjasperxml/tcpdf/tcpdf.php");
	include_once("../../controllers/lib/phpjasperxml/PHPJasperXML.inc.php");
	include_once("../../controllers/lib/phpjasperxml/setting.php");
	include_once("../../controllers/lib/lib.php");

	$usuario		= $_POST["usuario"];
	$perfil 		= $_POST["perfil"];
	$estado			= $_POST["estado"];
	
	$sentencia = "select	u.usuario,
							u.nombres,
							to_char(u.fecha_registro,'dd/mm/yyyy hh:mm') as fecha_registro,
							(case 
								when u.perfil = 'A' then 'ADMINISTRADOR'
								when u.perfil = 'U' then 'USUARIO'
								when u.perfil = 'C' then 'CONSULTA'
							end) as perfil,
							(case 
								when u.estado = 'H' then 'HABILITADO'
								when u.estado = 'I' then 'INHABILITADO'
							end) as estado
					from 	usuarios as u
					where	1 = 1";

	// Filtros del SQL
	if($usuario != "") $sentencia .= " and u.usuario = '" . $usuario . "'";
	if($perfil != "") $sentencia .= " and u.perfil = '" . $perfil . "'";
	if($estado != "") $sentencia .= " and u.estado = '" . $estado . "'";
			
	$sentencia .= " order by u.usuario, u.perfil, u.estado";
			
	include_once("../../models/DataBase.php");
	$dataBase = new DataBase("../../models/params.ini");
			
	$row = $dataBase->ejecutarConsulta($sentencia);
	if(!$row){
		echo "<script> alert('No existen Datos. Verifique'); 
		location.href='rpt-auditorias-sistema.php'; </script>";
		return;
	}
				
	$PHPJasperXML = new PHPJasperXML();
	$PHPJasperXML->arrayParameter=array("parameter1"=>"1");
	$PHPJasperXML->load_xml_file("rpt-usuarios.jrxml");
	$PHPJasperXML->sql=$sentencia;
	$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db,"psql");
	$PHPJasperXML->outpage("I");
			
	unset($dataBase);
?>