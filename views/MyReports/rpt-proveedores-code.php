<?php
	// Librerías del Phpjasperxml
	include_once("../../controllers/lib/phpjasperxml/tcpdf/tcpdf.php");
	include_once("../../controllers/lib/phpjasperxml/PHPJasperXML.inc.php");
	include_once("../../controllers/lib/phpjasperxml/setting.php");
	include_once("../../controllers/lib/lib.php");

	$proveedor 			= $_POST["proveedor"];
	$documento 			= $_POST["documento"];
	$pais 				= $_POST["pais"];
	$ciudad				= $_POST["ciudad"];
	$personeria 		= $_POST["personeria"];
	
	$sentencia =  "select	pro.razon_social,
							pro.documento,
							pro.direccion,
							p.nombre as nombre_pais,
							c.nombre as nombre_ciudad,
							l.nombre as nombre_localidad,
							pro.telefono,
							pro.email,
							(case
								when pro.personeria = 'F' then 'Física'
								when pro.personeria = 'J' then 'Jurídica'
								else 'Desconocida'
							end) as personeria,
							pro.proveedor
					from 	proveedores as pro, paises as p, ciudades as c, localidades as l 
					where 	pro.pais=p.pais
					and		pro.ciudad=c.ciudad
					and		pro.localidad=l.localidad";

	// Filtros del SQL
	if($proveedor != "") $sentencia .= " and pro.proveedor = '" . $proveedor . "'";
	if($documento != "") $sentencia .= " and pro.documento = '" . $documento . "'";
	if($pais != "") $sentencia .= " and pro.pais = '" . $pais . "'";
	if($ciudad != "") $sentencia .= " and pro.ciudad = '" . $ciudad . "'";
	if($personeria != "") $sentencia .= " and pro.personeria = '" . $personeria . "'";
			
	$sentencia .= " order by pro.razon_social";
	
	include_once("../../models/DataBase.php");
	$dataBase = new DataBase("../../models/params.ini");
			
	$row = $dataBase->ejecutarConsulta($sentencia);
	if(!$row){
		echo "<script> alert('No existen Datos. Verifique'); 
		location.href='rpt-proveedores.php'; </script>";
		return;
	}
				
	$PHPJasperXML = new PHPJasperXML();
	$PHPJasperXML->arrayParameter=array("parameter1"=>"1");
	$PHPJasperXML->load_xml_file("rpt-proveedores.jrxml");
	$PHPJasperXML->sql=$sentencia;
	$PHPJasperXML->transferDBtoArray($server,$user,$pass,$db,"psql");
	$PHPJasperXML->outpage("I");
			
	unset($dataBase);
?>