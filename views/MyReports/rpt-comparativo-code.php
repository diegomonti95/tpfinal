<?php
	include_once("../../models/DataBase.php");
		
	$presupuesto 	= $_POST["presupuesto"];
	$fecha_desde 	= $_POST["fecha_desde"];
	$fecha_hasta 		= $_POST["fecha_hasta"];
	
	$presupuestos = array();
	
	$sentencia = 	"select pe.presupuesto, 
					pe.nombre,
					pe.monto_presupuesto / (select sum(p.monto_presupuesto) from presupuestos_ventas p) * 100 as porcentaje
					from presupuestos_ventas pe
					where pe.estado <> 'R' ";
	if($presupuesto != "") $sentencia .= " and pe.presupuesto = " . $presupuesto . " ";
	if($fecha_desde != "" && $fecha_desde !=  "__/__/____ __:__") $sentencia .= " and pe.fecha_presupuesto >= '" . $fecha_desde . "' ";
	if($fecha_hasta != "" && $fecha_hasta !=  "__/__/____ __:__") $sentencia .= " and presupuestos.fecha_presupuesto <= '" . $fecha_hasta . "' ";
	$sentencia .= " order by porcentaje desc ";
	
	$db = new DataBase("../../models/params.ini");
	$result = $db->ejecutarConsulta($sentencia);
	if(!$result)
	{
		echo("Aviso de Reporte: No existen Datos..");
		return;
	}
	
	if( ($fecha_desde == "" || $fecha_desde == "__/__/____ __:__") &&
		($fecha_hasta == "" || $fecha_hasta == "__/__/____ __:__") ) 
		$titulo = "Tipos ";
	else
		$titulo = "Tipos. Desde: " . $fecha_desde . ". Hasta: " . $fecha_hasta;
		
	$rs= "<script src='../../controllers/js/highcharts/highcharts.js'></script>
		<script src='../../controllers/js/highcharts/modules/data.js'></script>
		<script src='../../controllers/js/highcharts/modules/drilldown.js'></script>
		<div id='container' style='min-width: 310px; height: 400px; margin: 0 auto'></div>
		<script type='text/javascript'>
		Highcharts.chart('container', {
			chart: {
				type: 'column'
			},
			title: {
				text: '" . $titulo . "'
			},
			subtitle: {
				text: 'Haga clic en las columnas para los rubros.'
			},
			xAxis: {
				type: 'category'
			},
			yAxis: {
				title: {
					text: 'Total de participación'
				}

			},
			legend: {
				enabled: false
			},
			plotOptions: {
				series: {
					borderWidth: 0,
					dataLabels: {
						enabled: true,
						format: '{point.y:.0f}'
					}
				}
			},
			tooltip: {
				headerFormat: '<span style=font-size:11px>{series.name}</span><br>',
				pointFormat: '<span style=color:{point.color}>{point.name}</span>: <b>{point.y:.0f}</b><br/>'
			},
			series: [{
				name: 'Brands',
				colorByPoint: true,
				data: [";
				
			
			foreach($result as $registro){
				$rs .= "{name: '" . $registro["nombre"] . "', y: " . $registro["porcentaje"] . ", drilldown: '" . $registro["nombre"] . "'},";
				array_push($presupuestos,$registro["presupuesto"]);
			}
			$rs .= "]}],
			drilldown: {
				series: [";
				for($i=0;$i<count($presupuestos);$i++)
				{
					$sentencia = "select presupuestos_ventas.nombre as nombre_presupuesto,
							clasificaciones.nombre as nombre_clasficacion,
							sum(gastos_detalles.subtotal) as total_clasificacion
							from gastos, gastos_detalles, presupuestos_ventas, productos, clasificaciones
							where gastos.gasto = gastos_detalles.gasto
							and	gastos.presupuesto_vta = presupuestos_ventas.presupuesto
							and	gastos_detalles.producto = productos.producto
							and	productos.clasificacion = clasificaciones.clasificacion
							and	presupuestos_ventas.presupuesto = '" . $presupuestos[$i] . "'
							group by 1,2
							order by total_clasificacion desc ";
							
					$result = $db->ejecutarConsulta($sentencia);
					
					$rs .= "{name: '" . $result[0]["nombre_presupuesto"] . "',id: '" . $result[0]["nombre_presupuesto"] . "',data:[";
					foreach($result as $registro){
						$rs .= "['" . $registro["nombre_clasficacion"] . "', " . 
						$registro["total_clasificacion"] . "],";
					}
					$rs .= "]},";
				}
				$rs .= "]
			}
		});
		</script>";
		
		echo $rs;
?>