<!DOCTYPE hmtl>
<html>
	<head>
		<meta charset="utf-8">
		<title>Menú Importadora</title>
		<script src="../../controllers/js/jquery.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/bootstrap.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/popper.min.js"></script>
		<script src="../../controllers/js/code.js"></script>
		<link href="../../controllers/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="../css/style.css" rel="stylesheet"/>
		<link href="../css/menu.css" rel="stylesheet"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- DataTable -->
		<link href="../../controllers/js/bootstrap/css/jquery.dataTables.min.css" rel="stylesheet">
		<script src="../../controllers/js/bootstrap/js/jquery.dataTables.min.js"></script>
		
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php 
					include_once("../../controllers/lib/lib.php"); 
					sesion_usuario(); 
					sesion_administrador();
				?>
			</div>
			<br>
			<div class="col-sm-10 col-sm-offset-1">
				<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('col-sm-10');">
					<span aria-hidden='true'>&times;</span>
				</button>
				<h3>Reporte de Usuarios <hr></h3>
				<form method="post" action="rpt-usuarios-code.php">
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Usuario:</label>
						<div class="col-sm-6">
							<?php lista_datos("usuario","select usuario, usuario from usuarios order by 2",0,"",""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Perfil:</label>
						<div class="col-sm-4">
							<select class="form-control" id="perfil" name="perfil">
								<option value=""></option>
								<option value="A">Administrador</option>
								<option value="U">Usuario</option>
								<option value="C">Consulta</option>
							</select>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Estado:</label>
						<div class="col-sm-4">
							<select class="form-control" id="estado" name="estado">
								<option value=""></option>
								<option value="H">Habilitado</option>
								<option value="I">Inhabiltado</option>
							</select>
						</div>
					</div>	
					<div class="form-group row">
						<div class="col-sm-5">
							<button type="submit" class="btn btn-secondary">Visualizar Reporte</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script>
		$("#usuario").prop('selectedIndex', 0);
		$("#perfil").prop('selectedIndex', 0);
		$("#estado").prop('selectedIndex', 0);
		$("#usuario").focus();
	</script>
</html>