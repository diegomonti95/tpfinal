<!DOCTYPE hmtl>
<html>
	<head>
		<meta charset="utf-8">
		<title>Menú Importadora</title>
		<script src="../../controllers/js/jquery.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/bootstrap.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/popper.min.js"></script>
		<script src="../../controllers/js/code.js"></script>
		<link href="../../controllers/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="../css/style.css" rel="stylesheet"/>
		<link href="../css/menu.css" rel="stylesheet"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- DataTable -->
		<link href="../../controllers/js/bootstrap/css/jquery.dataTables.min.css" rel="stylesheet">
		<script src="../../controllers/js/bootstrap/js/jquery.dataTables.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php 
					include_once("../../controllers/lib/lib.php"); 
					sesion_usuario(); sesion_administrador();
					/* ---------------------------------------------- */
					$clave = $_GET["clave"];
					include_once("../../models/Sucursal.php");
					$s = new Sucursal("","","","","","","","","","../../models/params.ini");
					$s->consultar($clave);
				?>
			</div>
			<br>
			<div class="col-sm-10 col-sm-offset-1">
				<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('col-sm-10');">
					<span aria-hidden='true'>&times;</span>
				</button>
				<h3>Editar Sucursal <hr></h3>
				<div id="result"></div>
				<form>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Sucursal:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control text-left" id="sucursal" 
							placeholder="" disabled value="<?php echo $s->getSucursal(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Nombre:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control text-left" id="nombre" 
							placeholder="Nombre de la sucursal" maxlength="50px" value="<?php echo $s->getNombre(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">RUC:</label>
						<div class="col-sm-6">
							<input type="text" class="form-control text-left" id="ruc" 
							placeholder="Ej.: 643215-8" maxlength="50px" value="<?php echo $s->getRuc(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Dirección:</label>
						<div class="col-sm-6">
							<textarea class="form-control text-left" id="direccion" 
							placeholder="Dirección de la sucursal" maxlength="150px"><?php echo $s->getDireccion(); ?></textarea>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Ciudad:</label>
						<div class="col-sm-6">
							<?php lista_datos("ciudad","select ciudad, nombre from ciudades order by 2",$s->getCiudad(),"filtrarLocalidad();"); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Localidad:</label>
						<div class="col-sm-6">
							<?php lista_datos("localidad","select localidad, nombre from localidades order by 2",$s->getLocalidad()); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Teléfono:</label>
						<div class="col-sm-5">
							<input type="text" class="form-control text-left" id="telefono" 
							placeholder="Ej.: 098164556 / 021899653" maxlength="10px" value="<?php echo $s->getTelefono(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">E-Mail:</label>
						<div class="col-sm-6">
							<input type="email" class="form-control text-left" id="email" 
							placeholder="Ej: ejemplo@gmail.com" maxlength="50px"value="<?php echo $s->getEmail(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Sitio Web:</label>
						<div class="col-sm-6">
							<input type="url" class="form-control text-left" id="pagina_web" 
							placeholder="Ej: www.ejemplo.com.py" maxlength="150px" value="<?php echo $s->getPaginaWeb(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-5">
							<button type="button" class="btn btn-secondary" onclick="editarSucursal();">Actualizar</button>
							<button type="button" class="btn btn-info" onclick="location.href='sucursales-listar.php'" > ■ Lista</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script>
		$("#nombre").focus();
	</script>
</html>