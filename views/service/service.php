<?php
	require_once("nusoap/lib/nusoap.php");
	
	$srv = new soap_server();
	
	function fn_datos_gastos($presupuesto,$fecha_desde,$fecha_hasta,
							$proveedor,$producto,$clasificador){
		
		include_once("../../models/DataBase.php");
		$db = new DataBase("../../models/params.ini");
				
		$sentencia = "select sucursales.nombre as nombre_sucursal,
							gastos.presupuesto_vta,
							gastos.nro_comprobante,
							clientes.razon_social as nombre_cliente,
							to_char(gastos.fecha,'dd-mm-yyyy HH:II') as fecha_gasto,
							proveedores.razon_social as nombre_proveedor,
							proveedores.documento,
							productos.nombre as nombre_producto,
							to_char(gastos_detalles.subtotal,'999G999G999,99') as total,
							monedas.nombre as nombre_moneda
				from		gastos, gastos_detalles, sucursales, proveedores, presupuestos_ventas, 
							clientes, productos, monedas
				where		gastos.sucursal = sucursales.sucursal
				and			gastos.gasto = gastos_detalles.gasto
				and			gastos.proveedor = proveedores.proveedor
				and			gastos_detalles.producto = productos.producto
				and			gastos.presupuesto_vta = presupuestos_ventas.presupuesto
				and			presupuestos_ventas.cliente = clientes.cliente
				and			gastos.moneda = monedas.moneda ";
		
		if($presupuesto > 0) $sentencia .= " and gastos.presupuesto_vta = '" . $presupuesto . "'";
		if($fecha_desde != "" && $fecha_desde != "__/__/____ __:__") 
			$sentencia .= " and gastos.fecha >='" . $fecha_desde . "'";
		if($fecha_hasta != "" && $fecha_hasta != "__/__/____ __:__") 
			$sentencia .= " and gastos.fecha <='" . $fecha_hasta . "'";
		if($proveedor > 0) $sentencia .= " and gastos.proveedor = '" . $proveedor . "'";
		if($producto > 0) $sentencia .= " and gastos_detalles.producto = '" . $producto . "'";
		if($clasificador > 0) $sentencia .= " and gastos.clasificador = '" . $clasificador . "'";
		$sentencia .= " order by gastos.fecha, gastos.presupuesto_vta ";
		
		$row = $db->ejecutarConsulta($sentencia);
		
		$salida="";
		if($row){
			foreach($row as $datos){
				$salida .=  $datos["nombre_sucursal"] . "#" . $datos["presupuesto_vta"] . "#" . 
				$datos["nro_comprobante"] . "#" . $datos["nombre_cliente"] . "#" .
				$datos["fecha_gasto"] . "#" . $datos["nombre_proveedor"] . "#" .
				$datos["documento"] . "#" . $datos["nombre_producto"] . "#" .
				$datos["total"] . "#" . $datos["nombre_moneda"] . "|";
			}
		}else
			$salida = "NO SE ENCUENTRAN DATOS..";
		
		$salida = substr($salida, 0, -1);
		return $salida;
	}
	
	$ns = "http://localhost/importadora/views/service/service.php";
	$srv->configurewsdl('ApplicationServices',$ns);
	$srv->wsdl->schematargetnamespace="$ns";
	
	$srv->register("fn_datos_gastos",array('presupuesto'=>'xsd:int', 
	'fecha_desde'=>'xsd:String','fecha_hasta'=>'xsd:String','proveedor'=>'xsd:int',
	'producto'=>'xsd:int','clasificador'=>'xsd:int'),
	array('return'=>'xsd:String'),$ns);
	
	if(isset($HTTP_RAW_POST_DATA)){
		$input = $HTTP_RAW_POST_DATA;
	}
	else{
		$input = implode("rn",file('php://input'));
	}
	$srv->service($input);
	exit;
?>