<?php
	require_once("client.php");
		
	$presupuesto= $_POST["presupuesto"];
	$fecha_desde= $_POST["fecha_desde"];
	$fecha_hasta= $_POST["fecha_hasta"];
	$proveedor	= $_POST["proveedor"];
	$producto	= $_POST["producto"];
	$clasificador= $_POST["clasificador"];
	
	$datos = invocarServicio($presupuesto, $fecha_desde, $fecha_hasta, $proveedor, $producto, $clasificador);
	// echo invocarServicio($presupuesto, $fecha_desde, $fecha_hasta, $proveedor, $producto, $clasificador);
	// Separa por filas los datos
	$datos = explode("|",$datos);
	
	$salida="<table class='table table-striped'>
		<thead>
			<tr>
				<th scope='col'>Sucursal</th>
				<th scope='col'>Presupuesto</th>
				<th scope='col'>Nº Comprobante</th>
				<th scope='col'>Cliente</th>
				<th scope='col'>Fecha</th>
				<th scope='col'>Proveedor</th>
				<th scope='col'>Documento</th>
				<th scope='col'>Producto</th>
				<th scope='col'>Subtotal</th>
				<th scope='col'>Moneda</th>
			</tr>
		</thead>
		<tbody>";
		
	foreach($datos as $row){
		$cols = explode("#",$row);
		
		$salida .= "<tr>
					<td>" . $cols[0] . "</td>
					<td>" . $cols[1] . "</td>
					<td>" . $cols[2] . "</td>
					<td>" . $cols[3] . "</td>
					<td>" . $cols[4] . "</td>
					<td>" . $cols[5] . "</td>
					<td>" . $cols[6] . "</td>
					<td>" . $cols[7] . "</td>
					<td>" . $cols[8] . "</td>
					<td>" . $cols[9] . "</td>
				</tr>";
	}
	
	$salida .= "</tbody>
			</table>";
	
	echo $salida;
?>