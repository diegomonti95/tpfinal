<!DOCTYPE hmtl>
<html>
	<head>
		<meta charset="utf-8">
		<title>Menú Importadora</title>
		<script src="../../controllers/js/jquery.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/bootstrap.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/popper.min.js"></script>
		<script src="../../controllers/js/code.js"></script>
		<link href="../../controllers/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="../css/style.css" rel="stylesheet"/>
		<link href="../css/menu.css" rel="stylesheet"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- datetimepicker -->
		<link rel="stylesheet" type="text/css" href="../../controllers/js/datetimepicker/jquery.datetimepicker.css">
		<script src="../../controllers/js/datetimepicker/build/jquery.datetimepicker.full.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php 
					include_once("../../controllers/lib/lib.php"); 
					sesion_usuario(); 
				?>
			</div>
			<br>
			<div class="col-sm-10 col-sm-offset-1">
				<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('col-sm-10');">
					<span aria-hidden='true'>&times;</span>
				</button>
				<h3>Webservice<hr></h3>
				<p>Se desarrollará y consumirá un servicio web que permita presentar los datos de frascos a adquirir por parte AsuEXpress, los proveedores y precios de compra de los productos utilizados en cada presupuesto de ventas.
				De este modo, si consultamos una caja de Frascos de medida 120 ML color Ámbar, deberá aparecer este dato en todos los presupuestos de la empresa en que se haya utilizado, junto con los datos del proveedor y el costo de adquisición para referencia de futuros presupuestos.
				</p>
				<form method="post" action="rpt-auditorias-sistema-code.php">
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Presupuesto:</label>
						<div class="col-sm-2">
							<?php lista_datos("presupuesto","select presupuesto, presupuesto from presupuestos_ventas order by 2",
							0,"",""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Fecha:</label>
						<div class="col-sm-2">
							<input type="datetime" class="form-control text-left" id="fecha_desde" name="fecha_desde"
							placeholder="dd/mm/aaaa hh:mm" maxlength="16">
						</div>
						<div class="col-sm-2">
							<input type="datetime" class="form-control text-left" id="fecha_hasta" name="fecha_hasta"
							placeholder="dd/mm/aaaa hh:mm" maxlength="16">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Proveedor:</label>
						<div class="col-sm-6">
							<?php lista_datos("proveedor","select proveedor, razon_social from proveedores order by 2",
							0,"",""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Producto / Servicio:</label>
						<div class="col-sm-6">
							<?php lista_datos("producto","select producto, producto from productos order by 2",
							0,"",""); ?>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Clasificador:</label>
						<div class="col-sm-6">
							<?php lista_datos("clasificador","select clasificador_gasto, nombre from clasificadores_gastos order by 2",
							0,"",""); ?>
						</div>
					</div>					
					<div class="form-group row">
						<div class="col-sm-5">
							<button type="button" class="btn btn-secondary" onclick="llamarWebservice();">Consumir Webservice</button>
						</div>
					</div>
					<div id="result"></id>
				</form>
			</div>
		</div>
	</body>
	<script>
		function llamarWebservice(){
			var data = {
						"presupuesto":$("#presupuesto").val(),
						"fecha_desde":$("#fecha_desde").val(),
						"fecha_hasta":$("#fecha_hasta").val(),
						"proveedor":$("#proveedor").val(),
						"producto":$("#producto").val(),
						"clasificador":$("#clasificador").val()
					   };
					   
			$.ajax({
				data:data,
				type:'post',
				url:'webservice-code.php',
				success:function(response){
					$("#result").html(response);
				}		
			});
		}
		
		jQuery("#fecha_desde").datetimepicker({format:"d/m/Y H:i",mask:true});
		jQuery("#fecha_hasta").datetimepicker({format:"d/m/Y H:i",mask:true});
		$("#presupuesto").focus();
	</script>
</html>