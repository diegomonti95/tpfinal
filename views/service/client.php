<?php
	require_once("nusoap/lib/nusoap.php");

	function invocarServicio($presupuesto,
		$fecha_desde,
		$fecha_hasta,
		$proveedor,
		$producto,
		$clasificador)
	{
		
		$wsdl = "http://localhost/importadora/views/service/service.php?wsdl";
		
		$client = new nusoap_client($wsdl,"wsdl");
		$params = array("presupuesto"=>$presupuesto,
		"fecha_desde"=>$fecha_desde,
		"fecha_hasta"=>$fecha_hasta,
		"proveedor"=>$proveedor,
		"producto"=>$producto,
		"clasificador"=>$clasificador);
		
		$response = $client->call("fn_datos_gastos",$params);
		
		return $response;
	}	
?>