<?php
	function lista_datos($name,$sql,$fk=0,$evento=''){
		include_once("../../models/DataBase.php");
		$db = new DataBase("../../models/params.ini");
	
		echo "<select class='form-control' id='$name' name='$name' onchange='$evento'>
		<option value=''></option>";

		$res = $db->ejecutarConsulta($sql);
				
		if($res){
			foreach($res as $row)
			{
				$selected = "";
				if($row[0] == $fk) 
					$selected = "selected";
				else 
					$selected = "";

				echo "<option value=" . $row[0] . " $selected>" . $row[1] . "</option>";
			}
		}
		else
			echo "<option value=''>No hay Datos</option>";
		
		echo "</select>";
		
		$db = null;
	}
	
	function validar_dato($dato,$tipo,$formato_fecha='Y-m-d H:i')
	{
		if($tipo == "float"){
			if(filter_var($dato, FILTER_VALIDATE_FLOAT) === false)
				return false;
		}
		else if($tipo == "integer"){
			if(filter_var($dato, FILTER_VALIDATE_INT) === false)
				return false;
		}
		else if($tipo == "string"){
			if(!preg_match('/^[a-zA-Z ñÑáéíóúüç]*$/', $dato))
				return false;
		}
		else if($tipo == "date"){
			$d = DateTime::createFromFormat($formato_fecha, $dato);
			return $d && $d->format($formato_fecha) == $dato;
		}
		else if($tipo == "telefono"){
			if(!preg_match("/^([0-9]){10}|([0-9]){9}$/", $dato))
				return false;
		}
		else if($tipo == "email")
		{
			if(filter_var($dato, FILTER_VALIDATE_EMAIL) === false)
				return false;
		}
		else if($tipo == "ruc")
		{
			if(!preg_match('/^([0-9]{7})|([0-9]{8})($-[0-9]{1})$/', $dato))
				return false;
		}
		else if($tipo == "timbrado")
		{
			if(!preg_match('/^[0-9]{8}$/', $dato))
				return false;
		}
		
		return true;
	}
	
	function alertar($header="Aviso",$msn,$foco=""){
		echo '<div class="alert alert-danger alert-dismissible">
				<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
				<strong>' . $header . ':</strong>&nbsp;&nbsp;' . $msn . '</div>';
		
		if($foco != "")
			echo "<script> $('#" . $foco . "').focus(); </script>";
	}
	
	function menu(){
		@session_start();
		$perfil = $_SESSION["perfil"];
		if( $perfil == "A"){
			echo '<div class="col-sm-3">
                    <div class="nav-side-menu">
                        <div class="brand">Menú</div>
                        <i class="fa fa-bars" data-toggle="collapse" data-target="#menu-content"></i>
                        <div class="menu-list">
                            <ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#sistema" class="collapsed">
                                    <i class="fa fa-home"></i> Sistema <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="sistema">
                                    <li><a href="../usuario/usuarios-listar.php">Usuario</a></li>
                                </ul>
                            </ul>
							<ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#general" class="collapsed">
                                    <i class="fa fa-align-justify"></i> General <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="general">
                                    <li><a href="../sucursal/sucursales-listar.php">Sucursales</a></li>
                                    <li><a href="../area/areas-listar.php">Áreas</a></li>
                                    <li><a href="../moneda/monedas-listar.php">Monedas</a></li>
                                </ul>
                            </ul>
							<ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#productos" class="collapsed">
                                    <i class="fa fa-th-large"></i> Productos <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="productos">
                                    <li><a href="../clasificacion/clasificaciones-listar.php">Clasificaciones</a></li>
                                    <li><a href="../unidad/unidades-listar.php">Unidades</a></li>
                                    <li><a href="#">Productos</a></li>
                                </ul>
                            </ul>
							<ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#proveedores" class="collapsed">
                                    <i class="fa fa-book"></i> Proveedores <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="proveedores">
                                    <li><a href="#">Proveedores</a></li>
                                    <li><a href="#">Gastos</a></li>
                                    <li><a href="#">Clasificadores de Gastos</a></li>
                                </ul>
                            </ul>
							<ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#clientes" class="collapsed">
                                    <i class="fa fa-user"></i> Clientes <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="clientes">
                                    <li><a href="#">Presupuestos</a></li>
                                    <li><a href="#">Clientes</a></li>
                                </ul>
                            </ul>
							<ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#reportes" class="collapsed">
                                    <i class="fa fa-print"></i> Reportes <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="reportes">
                                    <li><a href="#">Usuarios</a></li>
                                    <li><a href="#">Auditorías del Sistema</a></li>
                                    <li><a href="#">Clientes</a></li>
                                    <li><a href="#">Presupuestos</a></li>
                                    <li><a href="#">Proveedores</a></li>
                                    <li><a href="#">Gastos</a></li>
                                    <li><a href="#">Comparativo de Gastos (Gráfico)</a></li>
                                    <li><a href="#">Comparativo de Productos (Gráfico)</a></li>
                                </ul>
                            </ul>
							<ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#webservice" class="collapsed">
                                    <i class="fa fa-spinner"></i> WebService <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="webservice">
                                    <li><a href="#">XML</a></li>
                                    <li><a href="#">Servicio</a></li>
                                </ul>
                            </ul>
							<ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#sesion" class="collapsed">
                                    <i class="fa fa-sign-out"></i> Sesión <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="sesion">
                                    <li><a href="../../controllers/salir.php">Cerrar</a></li>
                                </ul>
                            </ul>
                        </div>
                    </div>
                </div>';
		}
		elseif($perfil == "U"){
			echo '<div class="col-sm-3">
                    <div class="nav-side-menu">
                        <div class="brand">Menú</div>
                        <i class="fa fa-bars" data-toggle="collapse" data-target="#menu-content"></i>
                        <div class="menu-list">
                            <ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#general" class="collapsed">
                                    <i class="fa fa-align-justify"></i> General <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="general">
                                    <li><a href="../moneda/monedas-listar.php">Monedas</a></li>
                                </ul>
                            </ul>
							<ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#productos" class="collapsed">
                                    <i class="fa fa-th-large"></i> Productos <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="productos">
                                    <li><a href="../clasificacion/clasificaciones-listar.php">Clasificaciones</a></li>
                                    <li><a href="../unidad/unidades-listar.php">Unidades</a></li>
                                    <li><a href="#">Productos</a></li>
                                </ul>
                            </ul>
							<ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#proveedores" class="collapsed">
                                    <i class="fa fa-book"></i> Proveedores <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="proveedores">
                                    <li><a href="#">Proveedores</a></li>
                                    <li><a href="#">Gastos</a></li>
                                    <li><a href="#">Clasificadores de Gastos</a></li>
                                </ul>
                            </ul>
							<ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#clientes" class="collapsed">
                                    <i class="fa fa-user"></i> Clientes <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="clientes">
                                    <li><a href="#">Presupuestos</a></li>
                                    <li><a href="#">Clientes</a></li>
                                </ul>
                            </ul>
							<ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#reportes" class="collapsed">
                                    <i class="fa fa-print"></i> Reportes <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="reportes">
                                    <li><a href="#">Clientes</a></li>
                                    <li><a href="#">Presupuestos</a></li>
                                    <li><a href="#">Proveedores</a></li>
                                    <li><a href="#">Gastos</a></li>
                                    <li><a href="#">Comparativo de Gastos (Gráfico)</a></li>
                                    <li><a href="#">Comparativo de Productos (Gráfico)</a></li>
                                </ul>
                            </ul>
							<ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#sesion" class="collapsed">
                                    <i class="fa fa-sign-out"></i> Sesión <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="sesion">
                                    <li><a href="../../controllers/salir.php">Cerrar</a></li>
                                </ul>
                            </ul>
                        </div>
                    </div>
                </div>';
		}
		elseif($perfil == "C"){
			echo '<div class="col-sm-3">
                    <div class="nav-side-menu">
                        <div class="brand">Menú</div>
                        <i class="fa fa-bars" data-toggle="collapse" data-target="#menu-content"></i>
                        <div class="menu-list">
                            <ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#webservice" class="collapsed">
                                    <i class="fa fa-spinner"></i> WebService <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="webservice">
                                    <li><a href="#">XML</a></li>
                                    <li><a href="#">Servicio</a></li>
                                </ul>
                            </ul>
							<ul id="menu-content" class="menu-content collapse out">
                                <li data-toggle="collapse" data-target="#sesion" class="collapsed">
                                    <i class="fa fa-sign-out"></i> Sesión <span class="arrow"></span>
                                </li>
                                <ul class="sub-menu collapse" id="sesion">
                                    <li><a href="../../controllers/salir.php">Cerrar</a></li>
                                </ul>
                            </ul>
                            </ul>
                        </div>
                    </div>
                </div>';
		}
	}
	
	function dataTable($id){
		echo '<script>
			var table = $("#' . $id . '").DataTable({
				language: {
					"decimal": "",
					"emptyTable": "No hay información",
					"info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
					"infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
					"infoFiltered": "(Filtrado de _MAX_ total entradas)",
					"infoPostFix": "",
					"thousands": ",",
					"lengthMenu": "Mostrar _MENU_ Entradas",
					"loadingRecords": "Cargando...",
					"processing": "Procesando...",
					"search": "Buscar:",
					"zeroRecords": "Sin resultados encontrados",
					"paginate": {
						"first": "Primero",
						"last": "Ultimo",
						"next": "Siguiente",
						"previous": "Anterior"
					}
				}
			});
		</script>';
	
	}
	
	function fecha_formato($fecha,$hora=true,$invertir=false){
		
		if($invertir == true){
			if($hora == false){
				$salida = substr($fecha,6,4) . "/" . substr($fecha,3,2) . 
				"/" . substr($fecha,0,2);
			}
			else{
				$salida = substr($fecha,6,4) . "/" . substr($fecha,3,2) . 
				"/" . substr($fecha,0,2) . " " . substr($fecha,11,5);
			}
		}
		else{
			/*if($hora == false){
				$salida = substr($fecha,8,2) . "/" . substr($fecha,4,2) . 
				"/" . substr($fecha,0,4);
			}
			else{
				$salida = substr($fecha,8,2) . "/" . substr($fecha,4,2) . 
				"/" . substr($fecha,0,4) . " " . substr($fecha,11,5);
			}*/
		}
		
		return $salida;
	}
	
	function sesion_usuario(){
		@session_start();
		if(!$_SESSION){
			alertar("Error de Acceso","Usted intenta acceder incorrectamente al Sistema.");
				echo "<script>
					/*setTimeout(function(){
						location.href='../acceso/acceso.php';
					},3000,'JavaScript');*/
					alert('No Inició correctamente el Sistema');
					location.href='../acceso/acceso.php';
				  </script>";
		}
		else
			menu();
	}
	
	function sesion_administrador(){
		@session_start();
		if(strcmp($_SESSION["perfil"],"A") != 0){
			alertar("Error de Acceso","Usted intenta acceder incorrectamente al Sistema.");
			echo "<script>
					alert('No tiene permisos para esta opción');
					location.href='../acceso/acceso.php';
				  </script>";
		}
	}
	
	function chequeo($estado){
		$ch = ($estado == "H" ? "checked" : "");
		echo $ch;
	}
?>