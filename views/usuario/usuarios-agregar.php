<!DOCTYPE hmtl>
<html>
	<head>
		<meta charset="utf-8">
		<title>Menú Importadora</title>
		<script src="../../controllers/js/jquery.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/bootstrap.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/popper.min.js"></script>
		<script src="../../controllers/js/code.js"></script>
		<link href="../../controllers/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="../css/style.css" rel="stylesheet"/>
		<link href="../css/menu.css" rel="stylesheet"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- DataTable -->
		<link href="../../controllers/js/bootstrap/css/jquery.dataTables.min.css" rel="stylesheet">
		<script src="../../controllers/js/bootstrap/js/jquery.dataTables.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php 
					include_once("../../controllers/lib/lib.php"); 
					sesion_usuario(); sesion_administrador();
					date_default_timezone_set("America/Asuncion");
				?>
			</div>
			<br>
			<div class="col-sm-10 col-sm-offset-1">
				<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('col-sm-10');">
					<span aria-hidden='true'>&times;</span>
				</button>
				<h3>Agregar Usuario <hr></h3>
				<div id="result"></div>
				<form>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Usuario:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control text-left" id="usuario" placeholder="Identificador" maxlength="15px">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Clave:</label>
						<div class="col-sm-5">
							<input type="password" class="form-control text-left" id="clave" placeholder="Clave del Usuario" maxlength="40px">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Nombres:</label>
						<div class="col-sm-5">
							<input type="text" class="form-control text-left" id="nombres" placeholder="Nombre del Usuario" maxlength="50px">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Fecha Registro:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="fecha_registro" 
							placeholder="" value="<?php echo date("d/m/Y H:m"); ?>" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Nivel Administración:</label>
						<div class="col-sm-4">
							<select class="form-control" id="perfil">
								<option value='A'>Administrador</option>
								<option value='C'>Consulta</option>
								<option value='U' selected>Usuario</option>
							</select> 
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-2">Habilitado:</div>
						<div class="col-sm-5">
							<div class="form-check">
								<label class="form-check-label">
									<input class="form-check-input" type="checkbox" id="estado" checked>Sí
								</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-5">
							<button type="button" class="btn btn-secondary" onclick="agregarUser();">Actualizar</button>
							<button type="button" class="btn btn-info" onclick="location.href='usuarios-listar.php'" > ■ Lista</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script>
		$("#usuario").focus();
	</script>
</html>