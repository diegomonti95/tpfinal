<!DOCTYPE hmtl>
<html>
	<head>
		<meta charset="utf-8">
		<title>Menú Importadora</title>
		<script src="../../controllers/js/jquery.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/bootstrap.min.js"></script>
		<script src="../../controllers/js/bootstrap/js/popper.min.js"></script>
		<script src="../../controllers/js/code.js"></script>
		<link href="../../controllers/js/bootstrap/css/bootstrap.min.css" rel="stylesheet"/>
		<link href="../css/style.css" rel="stylesheet"/>
		<link href="../css/menu.css" rel="stylesheet"/>
		<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
		<!-- DataTable -->
		<link href="../../controllers/js/bootstrap/css/jquery.dataTables.min.css" rel="stylesheet">
		<script src="../../controllers/js/bootstrap/js/jquery.dataTables.min.js"></script>
	</head>
	<body>
		<div class="container-fluid">
			<div class="row">
				<?php 
					include_once("../../controllers/lib/lib.php"); 
					sesion_usuario(); sesion_administrador();
					date_default_timezone_set("America/Asuncion");
					/* ---------------------------------------------- */
					$clave = $_GET["clave"];
					include_once("../../models/User.php");
					$user = new User("","","","","","","../../models/params.ini");
					$user->consultar($clave);
				?>
			</div>
			<br>
			<div class="col-sm-10 col-sm-offset-1">
				<button type='button' class='close' data-dismiss='alert' aria-label='Close' onclick="cerrar('col-sm-10');">
					<span aria-hidden='true'>&times;</span>
				</button>
				<h3>Editar Usuario <hr></h3>
				<div id="result"></div>
				<form>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Usuario:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control text-left" id="usuario" 
							placeholder="Identificador" maxlength="15px" value="<?php echo $user->getUsuario(); ?>" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Clave:</label>
						<div class="col-sm-5">
							<input type="password" class="form-control text-left" id="clave" placeholder="Clave del Usuario" 
							maxlength="40px" value="<?php echo $user->getClave(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Nombres:</label>
						<div class="col-sm-5">
							<input type="text" class="form-control text-left" id="nombres" placeholder="Nombre del Usuario" 
							maxlength="50px" value="<?php echo $user->getNombres(); ?>">
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Fecha Registro:</label>
						<div class="col-sm-4">
							<input type="text" class="form-control" id="fecha_registro" 
							placeholder="" value="<?php  echo fecha_formato($user->getFechaRegistro(),true,false); ?>" readonly>
						</div>
					</div>
					<div class="form-group row">
						<label for="" class="col-sm-2 col-form-label">Nivel Administración:</label>
						<div class="col-sm-4">
							<select class="form-control" id="perfil">
								<option value="A"<?php echo 'A' === $user->getPerfil() ? "selected='selected'" : ""; ?> >Administrador</option>
								<option value="C"<?php echo 'C' === $user->getPerfil() ? "selected='selected'" : ""; ?> >Consulta</option>
								<option value="U"<?php echo 'C' === $user->getPerfil() ? "selected='selected'" : ""; ?> >Usuario</option>
							</select> 
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-2">Habilitado:</div>
						<div class="col-sm-5">
							<div class="form-check">
								<label class="form-check-label">
									<input class="form-check-input" type="checkbox" id="estado" 
									<?php chequeo($user->getEstado()); ?> > Sí
								</label>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-sm-5">
							<button type="button" class="btn btn-secondary" onclick="editarUser();">Actualizar</button>
							<button type="button" class="btn btn-info" onclick="location.href='usuarios-listar.php'" > ■ Lista</button>
						</div>
					</div>
				</form>
			</div>
		</div>
	</body>
	<script>
		$("#clave").focus();
	</script>
</html>